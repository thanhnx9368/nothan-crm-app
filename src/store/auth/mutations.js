import JwtService from "@/services/Jwt"

import { PURGE_AUTH } from '@/types/mutations.type'

export const mutations = {
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.token = {};
    state.errors = {};
    JwtService.destroyToken();
  }
}
