export const CHECK_AUTH = "checkAuth";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_USER = "updateUser";
export const FETCH_USER_BY_TEAM = "fetchUserByTeam";

// ACTION CONST USER
export const USER_CURRENT = "currentUser";
export const USER_BY_ID = "getUserById";
export const USER_GET_LIST_BY_TEAM = "userGetListByTeam";
export const USER_CHECK_ADD_ON_EXPIRE = "userCheckAddOnExpire";
export const GET_LIST_PERMISSION = "getListPermission";
export const GET_LIST_STRUCTURE = "getListStructure";
export const GET_PERMISSION_BY_CURRENT_USER = "getPermissionByCurrentUser";
export const GET_LIST_PERMISSION_BY_STRUCTURE = "getListPermissionByStructure";
export const SET_PERMISSION_FOR_STRUCTURE = "setPermissionForStructure";
export const GET_APP_INFO = "getAppInfo";
export const SAVE_DEVICE_TOKEN_NOTI = "saveDeviceTokenNoti";
export const DELETE_DEVICE_TOKEN_NOTI = "deleteDeviceTokenNoti";

// ACTION CONST ORGANIZATION
export const ORGANIZATION_GET_ALL = "getAllOrganization";
export const ORGANIZATION_CURRENT = "currentOrganization";

// ACTION CONST CUSTOMER
export const CUSTOMER_GET_ALL = "customerGetAll";
export const GET_COUNT_CUSTOMER_BY_FILTER = "getCountCustomerByFilter";

export const CUSTOMER_GET_ATTRIBUTES = "customerGetAttributes";
export const CUSTOMER_GET_USER_ATTRIBUTES = "customerGetUserAttributes";
export const CUSTOMER_IMPORT_CHECK_ATTRIBUTES_UNIQUE = "customerImportCheckAttributesUnique";

export const CUSTOMER_SHOW_DETAIL = "customerShowDetail";
export const CUSTOMER_SHOW = "customerShow";
export const CUSTOMER_IMPORT = "customerImport";
export const CUSTOMER_GET_CUSTOMER_TYPE = "customerGetCustomerType";
export const CUSTOMER_UPDATE = "customerUpdate";
export const CUSTOMER_CREATE = "customerCreate";
export const CUSTOMER_CANCEL_UPDATE = "customerCancelUpdate";
export const CUSTOMER_ADD_NEW_CHECK_EXIST = "customerAddNewCheckExist";
export const CUSTOMER_SEND_MAIL_ASSIGNERS = "customerSendMailAssigners";
export const CUSTOMER_UPDATE_ASSIGNERS = "customerUpdateAssigners";
export const CUSTOMER_REMOVE_ASSIGNERS = "customerRemoveAssigners";
export const CUSTOMER_GET_ALL_ASSIGNER_IN_TEAM = "getAllAssignerInTeam";
export const CUSTOMER_GET_FILTERS = "customerGetFilters";
export const CUSTOMER_GET_FILTER_ATTRIBUTES = "customerGetFilterAttributes";
export const CUSTOMER_FILTER_CREATE = "customerFilterCreate";
export const CUSTOMER_FILTER_UPDATE = "customerFilterUpdate";
export const CUSTOMER_FILTER_DELETE = "customerFilterDelete";
export const CUSTOMER_GET_DATA_FOR_EXCEL = "customerGetDataForExcel";
export const CUSTOMER_GET_ATTRIBUTE_OPTION = "customerGetAttributeOption";
export const CUSTOMER_SAVE_ATTRIBUTE_OPTION = "customerSaveAttributeOption";
export const DOWNLOAD_EXCEL_DEFAULT = "dowloadFileExcelDefault";
export const DOWNLOAD_EXCEL_FILE = 'downloadFileExcel';
export const DOWNLOAD_EXCEL_FILE_CACHE = 'downloadFileExcelCache';

export const CUSTOMER_SEND_MAIL_ADD_MULTIPLE_ASSIGNERS = "customerSendMailAddMultipleAssigners";
export const CUSTOMER_SEND_MAIL_DELETE_MULTIPLE_ASSIGNERS = "customerSendMailDeleteMultipleAssigners";

export const CUSTOMER_GET_ATTRIBUTE_DATA_TYPE = "customerGetAttributeDataType";
export const CUSTOMER_CREATE_ATTRIBUTE = "customerCreateAttribute";
export const CUSTOMER_ADD_ATTRIBUTE_CHECK_EXIST = "customerAddAttributeCheckExist";
export const CUSTOMER_DELETE_ATTRIBUTE = "customerDeleteAttribute";
export const CUSTOMER_UPDATE_ATTRIBUTE = "customerUpdateAttribute";
export const CUSTOMER_UPDATE_LEVEL = "customerUpdateLevel";

export const CUSTOMER_GET_CONTACT = "customerListContact";
export const CUSTOMER_LIST_CONTACT = "customerListContact";
export const CUSTOMER_DELECT_CONTACT = "customerDeleteContact";
export const CUSTOMER_UPDATE_CONTACT = "customerUpdateContact";
export const CUSTOMER_ADD_CONTACT = "customerAddContact";
export const CUSTOMER_GET_ALL_CUSTOMER_CONTACT_BY_TEAM = "customerGetAllCustomerContactByTeam";
export const CUSTOMER_LIST_TRASH = "customerListTrash";
export const CUSTOMER_TRASH = "customerTrash";
export const CUSTOMER_RESTORE = "customerRestore";
export const CUSTOMER_CHECK_HAS_DEAL_BEFORE_DELETE = "checkCustomerHasDealBeforeDelete";
export const GET_CONFIG_CUSTOMER_ATTRIBUTE_TABLE = "getConfigCustomerAttributeTable";
export const CHANGE_TABLE_ATTRIBUTE_CUSTOMER = "changeTableAttributeCustomer";
export const GET_CONFIG_CUSTOMER = "getConfigCustomer";

export const CHECK_BEFORE_SEND_EMAIL_CUSTOMERS = "checkBeforeSendEmailCustomers";
export const SEND_EMAIL_CUSTOMERS = "sendEmailCustomers";

export const CHECK_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS = "checkBeforeUpdateAttributeCustomers";
export const UPDATE_ATTRIBUTE_CUSTOMERS = "updateAttributeCustomers";
export const CHECK_VALIDATE_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS = "checkValidateBeforeUpdateAttributeCustomers";

export const GET_TOTAL_CUSTOMERS_PHONE_NUMBER = "getTotalCustomersPhoneNumber";
export const VALIDATE_SMS_SEND_CUSTOMERS = "validateSmsSendCustomers";
export const SEND_SMS_CUSTOMERS = "sendSmsCustomers";

//REPORTS
export const CUSTOMER_GET_TOP_HARD_REPORT = "customerGetTopHardReport";
export const CUSTOMER_GET_INFO_HARD_CHART = "customerGetInfoHardChart";

export const CUSTOMER_IMPORT_CHECK_FILE_EXCEL = "customerImportCheckFileExcel";
export const DEAL_IMPORT_CHECK_FILE_EXCEL = "dealImportCheckFileExcel";

//FILE
export const FILE_CREATE_AVATAR = "fileCreateAvatar";
export const FILE_CREATE = "fileCreate";
export const FILE_CREATE_MULTIPLE = "createMultiple";
export const FILE_UPLOAD_NOTE = "fileUploadNote";
export const FILE_DOWNLOAD = "downloadFile";
export const FILE_DETAIL = "fileDetail";
export const FILE_BY_OBJECT = "fileByObject";

//NOTE
export const NOTE_CREATE = "noteCreate";
export const NOTE_GET_ALL_BY_OBJECT_ID = "noteGetAllByOjectId";

// ACTION CONST PRODUCT
export const PRODUCT_GET_ALL = "productGetAll";
export const PRODUCT_GET_ALL_WITHOUT_PAGINATE = "productGetAllWithoutPaginate";
export const PRODUCT_CREATE = "productCreate";
export const PRODUCT_UPDATE = "productUpdate";
export const PRODUCT_DETAIL = "productDetail";
export const PRODUCT_SHOW = "productShow";
export const PRODUCT_ADD_NEW_CHECK_EXIST = "productAddNewCheckExist";
export const PRODUCT_GET_ATTRIBUTES = "productGetAttributes";
export const PRODUCT_GET_USER_ATTRIBUTES = "productGetUserAttributes";
export const PRODUCT_GET_ALL_BY_TEAM = "productGetAllByTeam";
export const PRODUCT_LIST_TRASH = "productListTrash";
export const PRODUCT_TRASH = "productTrash";
export const PRODUCT_RESTORE = "productRestore";
export const PRODUCT_CHECK_HAS_DEAL_BEFORE_DELETE = "checkProductHasDealBeforeDelete";

export const GET_LIST_FILE_CONVERT_TABLE = "getListFileConvertTable";
export const LIST_FILE_IMPORT = "listFileImport";
export const DOWNLOAD_FILE_IMPORT = "downloadFileImport";
export const CHECK_FILE_CONVERT = "checkFileConvert";

// DEAL
export const DEAL_GET_ATTRIBUTES = "dealGetAttributes";
export const DEAL_GET_USER_ATTRIBUTES = "dealGetUserAttributes";
export const DEAL_GET_ALL = "dealGetAll";
export const DEAL_GET_DETAIL = "dealGetDetail";
export const DEAL_GET_TOTAL_PRICE = "dealGetTotalPrice";
export const DEAL_CREATE = "dealCreate";
export const DEAL_CREATE_ADD_STATUS = "dealCreateAddStatus";
export const DEAL_PRINT_TO_EXCEL = "dealPrintToExcel";
export const DEAL_UPDATE = "dealUpdate";
export const DEAL_GET_ALL_PAYMENT_HISTORY = "dealGetAllPaymentHistory";
export const DEAL_CREATE_PAYMENT = "dealCreatePayment";
export const CUSTOMER_DETAIL_GET_ALL_DEAL = "customerDetailGetAllDeal";
export const DEAL_GET_ALL_PRODUCT = "dealGetAllProduct";
export const DEAL_UPDATE_DELIVERY_STATUS = "dealUpdateDeliveryStatus";
export const GET_LIST_CUSTOMER_BY_PRODUCT_USED = "getListCustomerByProductUsed";
export const GET_LIST_DEAL_FILTERS = "getListDealFilters";
export const GET_DEAL_FILTER_ATTRIBUTES = "getDealFilterAttributes";
export const DEAL_FILTER_CREATE = "dealFilterCreate";
export const DEAL_FILTER_UPDATE = "dealFilterUpdate";
export const DEAL_FILTER_DELETE = "dealFilterDelete";
export const EXPORT_LIST_DEAL = "exportListDeal";
export const DEAL_IMPORT = "dealImport";

export const DEAL_GET_ATTRIBUTE_DATA_TYPE = "dealGetAttributeDataType";
export const DEAL_CREATE_ATTRIBUTE = "dealCreateAttribute";
export const DEAL_ADD_ATTRIBUTE_CHECK_EXIST = "dealAddAttributeCheckExist";
export const DEAL_DELETE_ATTRIBUTE = "dealDeleteAttribute";
export const DEAL_UPDATE_ATTRIBUTE = "dealUpdateAttribute";
export const DEAL_SORT_ATTRIBUTES = "dealSortAttributes";
export const DEAL_CHECK_HAS_PRODUCT_BEFORE_DELETE = "checkDealHasProductBeforeDelete";
export const DEAL_CHECK_PAYMENT_STATUS_SUCCESS = "checkPaymentStatusSuccessOfDeal";

export const DEAL_GET_CUSTOMER_ATTRIBUTE = "dealGetCustomerAttribute";
export const DEAL_GET_PRODUCT_ATTRIBUTE = "dealGetProductAttribute";
export const DEAL_GET_DEAL_ATTRIBUTE = "dealGetDealAttribute";
export const DEAL_EXPORT_ATTRIBUTE = "dealExportAttribute";
export const DEAL_EXPORT_DEFAULT_ATTRIBUTE = "dealExportDefaultAttribute";
export const DEAL_DEFAULT_ATTRIBUTE = "dealDefaultAttribute";
export const DEAL_VALIDATE_IMPORT_DEAL = "dealValidateImportDeal";
export const DEAL_IMPORT_DEAL = "dealImportDeal";
export const DEAL_GET_RESULT_FILE = "dealGetResultFile";

//REPORTS
export const DEAL_GET_TOP_HARD_REPORT = "dealGetTopHardReport";
export const DEAL_GET_INFO_HARD_CHART = "dealGetInfoHardChart";

//EDUCATION
export const CLASS_GET_ALL = "classGetAll";
export const CLASS_CREATE = "classCreate";
export const ClASS_DETAIL = "getClassDetail";
export const CLASS_CREATE_CHECK_EXIST = "classCreateCheckExist";
export const CLASS_UPDATE = "classUpdate";
export const CLASS_DELETE = "classDelete";
export const CLASS_UPDATE_STATUS = "classUpdateStatus";
export const CLASS_LIST_LESSON_GET_ALL = "listLessonClassGetAll";
export const CLASS_LESSON_UPDATE = "classLessonUpdate";
export const CHECK_IN_LESSON = "checkInLesson";
export const CHECK_HISTORY_CHECK_IN_LESSON = "checkHistoryCheckInLesson";
export const CLASS_JOIN_STUDENTS = "joinStudentsToClass";
export const GET_ALL_STUDENT_IN_THE_LESSON = "getAllStudentInTheLesson";
export const GET_ALL_STUDENT_OF_CLASS = "getAllStudentOfClass";
export const CHECK_HOMEWORK_LESSON = "checkHomeworkLesson";
export const CHECK_HISTORY_CHECK_HOMEWORK_LESSON = "checkHistoryCheckHomeworkLesson";
export const SAVE_HOMEWORK_NOTE_LESSON = "saveHomeworkNoteLesson";
export const NOTE_TEACHER_COMMENT_LESSON = "noteTeacherCommentLesson";
export const GET_MEMBER_IN_CLASS_OF_DEAL = "getMemberInClassOfDeal";
export const REMOVE_STUDENT_CLASS = "removeStudentClass";
export const RESERVE_STUDENT_CLASS = "reserveStudentClass";
export const CHANGE_CLASS = "changeClass";
export const GET_CLASS_BY_PRODUCT_ID = "getClassByProductId";
export const GET_LEARNING_OF_STUDENTS = "getLearningOfStudents";
export const GET_LIST_CLASS_OF_CUSTOMER_DETAIL = "getListClassOfCustomerDetail";
export const EXPORT_LIST_MEMBER_CLASS = "exportListMemberClass";
export const EXPORT_LEARNING_SITUATION = "exportLearningSituation";
export const EXPORT_LIST_MEMBER_WAITING_CLASS = "exportListMemberWaitingClass";
export const GET_ALL_STUDENT_EDUCATION = "getAllStudentEducation";
export const EXPORT_LIST_STUDENT_EDUCATION = "exportListStudentEducation";
export const STUDENT_GET_ATTRIBUTES = "studentGetAttributes";

export const STUDENT_GET_FILTERS = "studentGetFilters";
export const STUDENT_GET_FILTER_ATTRIBUTES = "studentGetFilterAttributes";
export const STUDENT_FILTER_CREATE = "studentFilterCreate";
export const STUDENT_FILTER_UPDATE = "studentFilterUpdate";
export const STUDENT_FILTER_DELETE = "studentFilterDelete";
export const CREATE_ACCOUNT_EDUCATION = "createAccountEducation";
export const BLOCK_ACCOUNT_EDUCATION = "blockAccountEducation";
export const ACTIVE_ACCOUNT_EDUCATION = "ActiveAccountEducation";
export const GET_ACCOUNT_EDU_ONLINE_STATUS = "getAccountEduOnlineStatus";

//REPORTS
export const GET_TOP_HARD_REPORT = "getTopHardReport";
export const GET_CUSTOMER_FLEXIBLE_REPORT = "getCustomerFlexibleReport";
export const GET_CUSTOMER_CHARGE_PERSON_REPORT = "getCustomerChargePersonReport";
export const EXPORT_CUSTOMER_FLEXIBLE_REPORT = "exportCustomerFlexibleReport";
export const EXPORT_CUSTOMER_CHARGE_PERSON_REPORT = "exportCustomerChargePersonReport";
export const SAVE_CUSTOMER_CONFIG_REPORT_FOR_USER = "saveConfigCustomerAttributeReportForUser";
export const GET_CUSTOMER_CONFIG_REPORT_FOR_USER = "getConfigCustomerAttributeReportForUser";

export const GET_DEAL_FLEXIBLE_REPORT = "getDealFlexibleReport";
export const GET_DEAL_CHARGE_PERSON_REPORT = "getDealChargePersonReport";
export const EXPORT_DEAL_FLEXIBLE_REPORT = "exportDealFlexibleReport";
export const EXPORT_DEAL_CHARGE_PERSON_REPORT = "exportDealChargePersonReport";
export const SAVE_DEAL_CONFIG_REPORT_FOR_USER = "saveConfigDealAttributeReportForUser";
export const GET_DEAL_CONFIG_REPORT_FOR_USER = "getConfigDealAttributeReportForUser";


//CUSTOMER CARE
export const EMAIL_TEMPLATE_LIST = "emailTemplateList";
export const EMAIL_TEMPLATE_ADD = "emailTemplateAdd";
export const EMAIL_TEMPLATE_DETAILS = "emailTemplateDetails";
export const EMAIL_TEMPLATE_UPDATE = "emailTemplateUpdate";

export const EMAIL_SCHEDULER_LIST = "emailSchedulerList";
export const EMAIL_SCHEDULER_ADD = "emailSchedulerAdd";
export const EMAIL_SCHEDULER_DETAIL = "emailSchedulerDetails";

export const ASK_UPLOAD_FILE = 'askUploadFile';
