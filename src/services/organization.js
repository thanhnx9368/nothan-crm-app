import * as config from "@/types/config";
import BaseApi from './BaseApi'
const apiBaseURL = config.UMS_BASE_URL;
const apiUrl = apiBaseURL + "teams";

const organizationServiceAPI = {
  getAll() {
    return BaseApi.get(apiUrl);
  },
  getCurrentTeam(teamId) {
    return BaseApi.get(`${apiUrl}/${teamId}`);
  },
};
export default organizationServiceAPI;
