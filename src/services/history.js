import BaseApi from './BaseApi'

const apiUrl = 'http://staging.hms.s.saf.com.vn/api/v1/histories';

export default {
  getAll() {
    return BaseApi.get(apiUrl);
  },
  getAllByObjectId(object_id) {
    return BaseApi.get(apiUrl + '?object_id=' + object_id);
  },
  show(id) {
    return BaseApi.get(apiUrl + '/' + id);
  },
  create(params) {
    return BaseApi.post(apiUrl, params);
  },
  import(file) {
    return BaseApi.post(apiUrl + '/import', file);
  },
  update(id, params) {
    return BaseApi.put(apiUrl + '/' + id, params);
  },
  multipleUpdate(params) {
    return BaseApi.put(apiUrl + '/multi', params);
  },
  delete(id) {
    return BaseApi.delete(apiUrl + '/' + id);
  },
  restore(id) {
    return BaseApi.put(apiUrl + '/restore/' + id);
  },
  multipleRestore(params) {
    return BaseApi.put(apiUrl + '/restore/multi', params);
  },
  allRestore() {
    return BaseApi.put(apiUrl + '/restore/all');
  },
  getAttributes(userId) {
    return BaseApi.get(apiUrl + '/attributes/' + userId);
  }
}
