/* eslint-disable no-undef */
import axios from 'axios'
import router from '@/router'
import jwt from './Jwt'
import {
  ID_BASE_URL
} from '@/const/config'
import {
  PERMISSION_STATEMENT_CUSTOMER,
  PERMISSION_STATEMENT_SALE
} from '@/const/permission'

const STATUS_CODE_AUTHEN_FAIL = 401
const STATUS_CODE_FORBIDDEN = 403
const STATUS_CODE_NOTFOUND = 404
const STATUS_CODE_METHOD_NOT_ALLOW = 405

axios.interceptors.request.use(config => {
  const team_id = router.history.current.params['team_id']
  config.headers['common'] = {
    ...config.headers['common'],
    'Authorization': `Bearer ${jwt.getToken()}`,
    'Content-Type': 'application/json',
  }
  config.headers = {
    ...config.headers,
  }
  config.params = {
    ...config.params,
    team_id
  }
  return config
})

axios.interceptors.response.use((response) => {
  if (response.data.status_code === STATUS_CODE_NOTFOUND || response.status === STATUS_CODE_NOTFOUND) {
    return window.location.href = '/404'
  }

  return response
}, (error) => {
  if (error.response.status === STATUS_CODE_AUTHEN_FAIL) {
    const returnUrl = `${ID_BASE_URL}/login?return_url=${window.location.href}`
    return window.location.href = returnUrl
  }


  if (error.response.status === STATUS_CODE_FORBIDDEN) {
    const errors = error.response.data.errors
    if (errors && errors.permission) {
      const permission = {}

      if (errors.permission && errors.permission.length > 0) {
        errors.permission.forEach((permissionItem) => {
          if (!permission[permissionItem]) {
            permission[permissionItem] = permissionItem
          }
        })
      }

      if (errors.is_redirect) {
        // return window.location.href = '/403'
        $('#PageForbidden').show()
        $('#app').hide()
        return
      } else {
        if (permission[PERMISSION_STATEMENT_CUSTOMER] || permission[PERMISSION_STATEMENT_SALE]) {
          // $('#popupUserCurrentNotPermissionRedirect403').modal('show')
          $('#PageForbidden').show()
          $('#app').hide()
          return
        } else {
          $('#popupUserCurrentNotPermission').modal('show')
          return
        }
      }
    } else {
      // return window.location.href = '/403'
      $('#PageForbidden').show()
      $('#app').hide()
      return
    }
  }

  if (error.response.status === STATUS_CODE_NOTFOUND) {
    return window.location.href = '/404'
  }

  if (error.response.status === 422) {
    return Promise.resolve(error.response)
  }

  if (error.response.status === STATUS_CODE_METHOD_NOT_ALLOW) {
    return Promise.resolve(error.response)
  }

  return Promise.resolve(error)
})
