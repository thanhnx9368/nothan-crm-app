import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.FMS_BASE_URL;
const filesUrl = baseUrl + 'files';
const folderUrl = baseUrl + 'folders';
const typeUrl = baseUrl + 'types';
const filterUrl = baseUrl + 'filtes';

export default {
  getAll(paramArgs) {
    return BaseApi.getSerialize(filesUrl + '?' + { params: paramArgs });
  },
  show(id) {
    return BaseApi.get(`${filesUrl}/${id}`);
  },
  download(id) {
    return BaseApi.get(`${filesUrl}/download/${id}`);
  },
  getAttributes() {
    return BaseApi.get(`${filesUrl}/attributes`);
  },
  getAttributesByUser(user_id) {
    return BaseApi.get(`${filesUrl}/attributes/${user_id}`);
  },
  create(params) {
    return BaseApi.post(filesUrl, params);
  },
  createMultiple(params) {
    return BaseApi.post(filesUrl + '/upload', params);
  },
  restore(id) {
    return BaseApi.post(filesUrl + '/restore/', id);
  },
  multipleRestore(params) {
    return BaseApi.post(filesUrl + '/restore', params);
  },
  unlink(params) {
    return BaseApi.post(filesUrl + '/unlink/' + params.id, params);
  },
  update(id, params) {
    return BaseApi.put(filesUrl + '/' + id, params);
  },
  multipleUpdate(params) {
    return BaseApi.put(filesUrl, params);
  },
  delete(id) {
    return BaseApi.delete(filesUrl + '/' + id);
  },
  multipleDelete(ids) {
    return BaseApi.delete(filesUrl, { params: ids });
  },
  forceDelete(id) {
    return BaseApi.delete(filesUrl + '/force-delete/' + id);
  },
  forceMultipleDelete(ids) {
    return BaseApi.delete(filesUrl + '/force-delete', { params: ids });
  },
  byObject(params) {
    return BaseApi.getSerialize(filesUrl + '/by-object', { params });
  },

  /*
  * Folders
  * */
  getAllFolders() {
    return BaseApi.get(`${folderUrl}`);
  },

  createFolders(params) {
    return BaseApi.post(folderUrl, params);
  },

  deleteFolders(id) {
    return BaseApi.delete(folderUrl + '/' + id);
  },

  showFolders(id) {
    return BaseApi.get(`${folderUrl}/${id}`);
  },

  updateFolders(id, params) {
    return BaseApi.put(folderUrl + '/' + id, params);
  },

  multipleDeleteFolders(ids) {
    return BaseApi.delete(folderUrl, { params: ids });
  },

  forceDeleteFolders(id) {
    return BaseApi.delete(folderUrl + '/force-delete/' + id);
  },

  restoreFolders(id) {
    return BaseApi.post(folderUrl + '/restore/', id);
  },

  /*
  * Types
  * */
  getAllTypes() {
    return BaseApi.get(typeUrl);
  },

  createTypes(params) {
    return BaseApi.post(typeUrl, params);
  },

  deleteTypes(id) {
    return BaseApi.delete(typeUrl + '/' + id);
  },

  showTypes(id) {
    return BaseApi.get(`${typeUrl}/${id}`);
  },

  updateTypes(id, params) {
    return BaseApi.put(typeUrl + '/' + id, params);
  },

  multipleDeleteTypes(ids) {
    return BaseApi.delete(typeUrl, { params: ids });
  },

  forceDeleteTypes(id) {
    return BaseApi.delete(typeUrl + '/force-delete/' + id);
  },

  restoreTypes(id) {
    return BaseApi.post(typeUrl + '/restore/', id);
  },

  /*
  * Filter
  * */
  getAllFilters() {
    return BaseApi.get(filterUrl);
  },

  createFilters(params) {
    return BaseApi.post(filterUrl, params);
  },

  deleteFilters(id) {
    return BaseApi.delete(filterUrl + '/' + id);
  },

  showFilters(id) {
    return BaseApi.get(`${filterUrl}/${id}`);
  },

  updateFilters(id, params) {
    return BaseApi.put(filterUrl + '/' + id, params);
  },

  multipleDeleteFilters(ids) {
    return BaseApi.delete(filterUrl, { params: ids });
  },

  forceDeleteFilters(id) {
    return BaseApi.delete(filterUrl + '/force-delete/' + id);
  },

  restoreFilters(id) {
    return BaseApi.post(filterUrl + '/restore/', id);
  }
}
