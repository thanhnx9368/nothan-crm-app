/* eslint-disable no-undef */
import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.CMS_BASE_URL;
const baseMailUrl = config.MAIL_BASE_URL;

export default {
  emailTemplateList(params) {
    return BaseApi.getSerialize(baseUrl + 'email-templates', {
      params
    })
  },

  emailTemplateAdd(body) {
    return BaseApi.post(baseUrl + 'email-templates', body)
  },

  emailTemplateDetails(id) {
    return BaseApi.get(baseUrl + 'email-templates/' + id)
  },

  emailTemplateUpdate(params) {
    return BaseApi.put(baseUrl + 'email-templates/' + params.id, params)
  },

  emailSchedulerList(params) {
    return BaseApi.getSerialize(baseUrl + 'email-schedules', {
      params
    })
  },

  emailSchedulerAdd(body) {
    return BaseApi.post(baseUrl + 'email-schedules', body)
  },


  emailSchedulerUpdate(id, body) {
    return BaseApi.put(`${baseUrl}email-schedules/${id}`, body)
  },

  emailSchedulerDetails(id) {
    return BaseApi.get(baseUrl + 'email-schedules/' + id)
  },

  getEmailsByIds(params) {
    return BaseApi.get(baseUrl + 'email-templates/email-info-by-ids', {
      params
    })
  },

  emailTemplateEmailFrame(params) {
    return BaseApi.get(baseMailUrl + 'mail-template/email-layout-by-id', {
      params
    })
  }
}