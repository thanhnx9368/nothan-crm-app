import Vue from 'vue'
import Router from 'vue-router'

import CustomerRouter from './modules/customer/router'
import DealRouter from './modules/deal/router'
import ProductRouter from './modules/product/router'
import ReportRouter from './modules/report/router'
import SettingRouter from './modules/setting/router'
import CustomerCareRouter from './modules/customer-care/router'
import MyJobRouter from './modules/my-job/router'

import utils from '@/utils/utils'
import Main from "./views/main/Main.vue"
import Maintenance from './views/maintenance/Maintenance.vue'
import PageForbidden from './views/errors/403/403.vue'
import PageNotFound from './views/errors/404/404.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'Root'
  }, {
    path: '/team/:team_id',
    component: Main,
    children: [{
        path: '',
        redirect: '/team/:team_id/customers',
      },
      ...utils.prefixRouter('/team/:team_id', DealRouter),
      ...utils.prefixRouter('/team/:team_id', ProductRouter),
      ...utils.prefixRouter('/team/:team_id', ReportRouter),
      ...utils.prefixRouter('/team/:team_id', SettingRouter),
      ...utils.prefixRouter('/team/:team_id', CustomerCareRouter),
      ...utils.prefixRouter('/team/:team_id', MyJobRouter),
      ...utils.prefixRouter('/team/:team_id', CustomerRouter),
    ]
  }, {
    path: '/maintenance',
    component: Maintenance
  }, {
    path: '/403',
    meta: {
      title: 'PageForbidden'
    },
    component: PageForbidden
  }, {
    path: '/404',
    meta: {
      title: 'PageNotFound'
    },
    component: PageNotFound
  }, {
    path: '*',
    redirect: '404'
  }]
})
export default router;