import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/utils/forms'
import './directives'
import './filters'
import './mixins'
import './libraries'

import BaseApi from '@/services/BaseApi'
import {
  ID_BASE_URL
} from '@/types/config.js'

window.addEventListener('popstate', () => {
  // eslint-disable-next-line no-undef
  $('.modal-backdrop').hide();
});

BaseApi.setup()

Vue.config.productionTip = false

const EventBus = new Vue();

Object.defineProperties(Vue.prototype, {
  $bus: {
    get: function () {
      return EventBus
    }
  }
});

let isAuthenticated = store.getters.isAuthenticated;
if (!isAuthenticated) {
  const returnUrl = `${ID_BASE_URL}/login?return_url=${window.location.href}`
  window.location.href = returnUrl
}

router.beforeEach((to, from, next) => {
  if (!isAuthenticated) {
    const returnUrl = `${ID_BASE_URL}/login?return_url=${window.location.href}`
    return window.location.href = returnUrl
  }

  if (to && to.name === 'Root') {
    return window.location.href = ID_BASE_URL + '/choose-organization';
  }

  if (!store.getters.currentTeam.name) {
    document.title = to.meta.title ? to.meta.title + ' | Nỏ Thần Web App' : 'Nỏ Thần Web App';
  }
  next();
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')