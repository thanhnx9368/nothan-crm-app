/* eslint-disable no-useless-escape */
import moment from 'moment'

export default {
  objectValues(object) {
    return Object.values(object)
  },

  sortBy(properties, targetArray) {
    targetArray.sort(function (i, j) {
      return properties.map(function (prop) {
        return i[prop] - j[prop]
      }).find(function (result) {
        return result
      })
    })
  },

  uid() {
    return Math.random()
      .toString(36)
      .slice(-5)
  },

  objectKeys(object) {
    return Object.keys(object)
  },

  continuousNumberArray(number) {
    if (!Number.isInteger(number)) {
      return
    }
    return [...Array(number)].map((_, i) => ++i)
  },

  download(blob, fileName) {
    const url = window.URL.createObjectURL(blob)
    const a = document.createElement('a')
    a.style.display = 'none'
    a.href = url
    a.download = fileName
    document.body.appendChild(a)
    a.click()
    window.URL.revokeObjectURL(url)
    document.body.removeChild(a)
  },

  changeAlias(alias) {
    let str = alias
    str = str.toLowerCase()
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str.replace(/đ/g, 'd')
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, ' ')
    str = str.replace(/ + /g, ' ')
    str = str.trim()
    return str
  },

  isEmptyObj(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false
      }
    }
    return true
  },

  checkObjData(obj) {
    if (obj instanceof Object === true) {
      return true
    }
    return false
  },

  getAttribute(data, attributeId) {
    let attribute = ''
    if (!this.isEmptyObj(data)) {
      const attributes = data.attributes
      if (attributes.length > 0) {
        attributes.forEach((item) => {
          if (item.id === attributeId) {
            attribute = item
          }
        })
      }
    }

    return attribute ? attribute : ''
  },

  getAttributeByAttrCode(data, attributeCode) {
    let attribute = ''
    if (!this.isEmptyObj(data)) {
      const attributes = data.attributes
      if (attributes.length > 0) {
        attributes.forEach((item) => {
          if (item.attribute_code === attributeCode) {
            attribute = item
          }
        })
      }
    }
    return attribute ? attribute : ''
  },

  getAttributeByAttributeCode(attributes, attributeCode) {
    let attribute = ''
    if (attributes.length > 0) {
      attributes.forEach((item) => {
        if (item.attribute_code === attributeCode) {
          attribute = item
        }
      })
    }
    return attribute ? attribute : ''
  },

  getAttributeByAttrCodeAndVariableName(data, attributeCode, variableName = 'id') {
    let attribute = ''
    if (!this.isEmptyObj(data)) {
      const attributes = data.attributes
      if (attributes.length > 0) {
        attributes.forEach((item) => {
          if (item.attribute_code === attributeCode) {
            attribute = item
          }
        })
      }
    }

    return attribute ? attribute[variableName] : ''
  },

  getAttributeContactName(data, attributeCode, variableName = 'id', contacts) {
    let attribute = ''
    if (!this.isEmptyObj(data)) {
      const attributes = data.attributes
      if (attributes.length > 0) {
        attributes.forEach((item) => {
          if (item.attribute_code === attributeCode) {
            attribute = item
          }
        })
      }
    }
    const attributeValue = attribute ? attribute[variableName] : ''
    let contactName = attributeValue ? attributeValue : 'Chưa cập nhật'
    if (attributeValue) {
      contactName = 'Chưa cập nhật'
      if (contacts && contacts.length) {
        contacts.forEach((item) => {
          if (item.id === attributeValue) {
            contactName = item.full_name
          }
        })
      }
    }
    return contactName
  },

  getAttributeOption(attribute, optionId, variable = 'id') {
    let option = ''
    if (!this.isEmptyObj(attribute)) {
      const options = attribute.attribute_options
      if (options.length > 0) {
        options.forEach((item) => {
          if (item.id === optionId) {
            option = item
          }
        })
      }
    }

    return option ? option[variable] : ''
  },

  getAttributeOptions(attribute, optionId, variable = 'id') {
    let option = ''
    if (!this.isEmptyObj(attribute)) {
      const options = attribute.attribute_options
      if (options.length > 0) {
        options.forEach((item) => {
          if (item.id === optionId) {
            option = item
          }
        })
      }
    }

    return option ? option[variable] : ''
  },

  getAttributesOptions(attribute, optionIds) {
    const selectedOptions = []
    if (!this.isEmptyObj(attribute)) {
      const options = attribute.attribute_options
      if (options && options.length > 0) {
        options.forEach((item) => {
          if (optionIds && optionIds.length > 0) {
            optionIds.forEach((opId) => {
              if (item.id === opId) {
                selectedOptions.push(item)
              }
            })
          }
        })
      }
    }
    return selectedOptions
  },

  getAttributeCodesByAllAttributes(attributes, attributeCodes) {
    const list = []

    if (attributes.length > 0) {
      if (attributes.length > 0 && attributeCodes.length > 0) {
        attributes.forEach((item) => {
          attributeCodes.forEach((attributeCode) => {
            if (item.attribute_code === attributeCode) {
              list.push(item)
            }
          })
        })

      }
    }
    return list
  },

  convertTimetoYMD(dateTime, getTime = true) {
    const dateTimeParse = moment(dateTime, 'DD/MM/YYYY hh:mm:ss').valueOf()
    if (Number.isNaN(dateTimeParse)) {
      return
    }
    if (dateTimeParse) {
      const newDateTime = new Date(dateTimeParse)
      const getDate = ('0' + newDateTime.getDate()).slice(-2)
      const getMonth = ('0' + (newDateTime.getMonth() + 1)).slice(-2)
      const getFullYear = newDateTime.getFullYear()
      const getHours = ('0' + newDateTime.getHours()).slice(-2)
      const getMinutes = ('0' + newDateTime.getMinutes()).slice(-2)
      const getSeconds = ('0' + newDateTime.getSeconds()).slice(-2)
      if (getTime) {
        return getFullYear + '-' + getMonth + '-' + getDate + ' ' + getHours + ':' + getMinutes + ':' + getSeconds
      }
      return getFullYear + '-' + getMonth + '-' + getDate
    }
    return dateTime
  },

  formatDateTimeForClient(dateTime, getTime = true) {
    const dateTimeParse = moment(dateTime, 'YYYY-MM-DD hh:mm:ss').valueOf()
    if (Number.isNaN(dateTimeParse)) {
      return
    }
    if (dateTimeParse) {
      const newDateTime = new Date(dateTimeParse)
      const getDate = ('0' + newDateTime.getDate()).slice(-2)
      const getMonth = ('0' + (newDateTime.getMonth() + 1)).slice(-2)
      const getFullYear = newDateTime.getFullYear()
      if (getTime) {
        return getDate + '/' + getMonth + '/' + getFullYear
      }
    }
    return
  },

  formatDateTimeDefault(dateTime, getTime = false) {
    const dateTimeParse = moment(dateTime, 'YYYY-MM-DD hh:mm:ss').valueOf()
    if (Number.isNaN(dateTimeParse)) {
      return
    }
    if (dateTimeParse) {
      const newDateTime = new Date(dateTimeParse)
      const getDate = ('0' + newDateTime.getDate()).slice(-2)
      const getMonth = ('0' + (newDateTime.getMonth() + 1)).slice(-2)
      const getFullYear = newDateTime.getFullYear()
      const getHours = ('0' + newDateTime.getHours()).slice(-2)
      const getMinutes = ('0' + newDateTime.getMinutes()).slice(-2)
      const getSeconds = ('0' + newDateTime.getSeconds()).slice(-2)

      if (getTime) {
        return getHours + ':' + getMinutes + ':' + getSeconds + ' - ' + getDate + '/' + getMonth + '/' + getFullYear
      }
      return getHours + ':' + getMinutes + ' - ' + getDate + '/' + getMonth + '/' + getFullYear
    }

    return
  },

  formatDateTime(time, dateTime = false) {
    const dateTimeParse = moment(time, 'YYYY-MM-DD hh:mm:ss').valueOf()
    if (Number.isNaN(dateTimeParse)) {
      return
    }
    if (dateTimeParse) {
      const newDateTime = new Date(dateTimeParse)
      const getDate = ('0' + newDateTime.getDate()).slice(-2)
      const getMonth = ('0' + (newDateTime.getMonth() + 1)).slice(-2)
      const getFullYear = newDateTime.getFullYear()
      const getHours = ('0' + newDateTime.getHours()).slice(-2)
      const getMinutes = ('0' + newDateTime.getMinutes()).slice(-2)
      const getSeconds = ('0' + newDateTime.getSeconds()).slice(-2)
      if (dateTime) {
        return getFullYear + '-' + getMonth + '-' + getDate + ' ' + getHours + ':' + getMinutes + ':' + getSeconds
      }

      return getFullYear + '-' + getMonth + '-' + getDate
    }
    return
  },

  formatTime(time, second = false) {
    if (time) {
      const dateTimeParse = moment(time, 'YYYY-MM-DD hh:mm:ss')
      if (!dateTimeParse.isValid()) {
        return
      }
      const getHours = ('0' + dateTimeParse.hour()).slice(-2)
      const getMinutes = ('0' + dateTimeParse.minutes()).slice(-2)
      const getSeconds = ('0' + dateTimeParse.seconds()).slice(-2)

      if (second) {
        return getHours + ':' + getMinutes + ':' + getSeconds
      }

      return getHours + ':' + getMinutes
    }
    return
  },

  formatTimeInDB(time) {
    if (time && typeof time !== 'undefined') {
      const newValue = time.substring(0, time.lastIndexOf(':'))
      return newValue
    }
  },

  formatSizeUnits(bytes, precision) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
      return '-'
    }
    if (typeof precision === 'undefined') {
      precision = 0
    }
    const units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB']
    const result = Math.floor(Math.log(bytes) / Math.log(1024))
    return (bytes / Math.pow(1024, Math.floor(result))).toFixed(precision) + ' ' + units[result]
  },

  getChargePersonValue(changeUsers, userIds) {
    const listUsers = []
    // tslint:disable-next-line: max-line-length
    if (typeof changeUsers !== 'undefined' && typeof userIds !== 'undefined' && changeUsers && changeUsers.length > 0 && userIds && userIds.length > 0) {
      changeUsers.forEach((item) => {
        userIds.forEach((userId) => {
          if (item.id === userId) {
            listUsers.push(item)
          }
        })
      })
      return listUsers
    } else {
      return ''
    }
  },

  textTruncate(text, stop = 20, clamp = '...') {
    if (typeof (text) !== 'string') {
      return
    }
    var index = text.indexOf(' ', stop);
    if (index == -1) return text;
    let newText = text ? text.substring(0, index) : '';
    return newText + ' ' + clamp;
  },

  checkShowHideTextReadmore(text, stop = 20) {
    var index = text.indexOf(' ', stop);
    if (index !== -1) return true;
    return false;
  },

  formatPrice(value) {
    const val = (value / 1).toFixed(0).replace('.', ',')
    if (value) {
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    }
    return 0
  },

  convertLanguages(text) {
    let str = String(text)

    if (str.length > 0) {
      str = str.trim()
      str = str.toLowerCase()
      str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
      str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
      str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
      str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
      str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
      str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
      str = str.replace(/đ/g, 'd')
      str = str.replace(/ + /g, ' ')
      str = str.trim()

      return str
    } else {
      return ''
    }
  },

  compareArray(arr1, arr2) {
    if (!arr1 || !arr2) {
      return false
    }
    let count = 0
    arr1.forEach((e1, id1) => {
      arr2.forEach((e2, id2) => {
        if (typeof arr1 === 'object' && typeof arr2 === 'object') {
          if (id1 === id2 && JSON.stringify(e1) === JSON.stringify(e2)) {
            count++
          } else {
            return
          }
        } else {
          return false
        }
      })
    })
    if (arr1.length > arr2.length) {
      if (arr1.length === count) {
        return true
      } else {
        return false
      }
    } else {
      if (arr2.length === count) {
        return true
      } else {
        return false
      }
    }
  },

  compareTwoArrayIds(arr1, arr2) {
    if (!arr1 || !arr2) {
      return false
    }
    let count = 0

    if (arr1.length < arr2.length) {
      return false
    }

    arr1.forEach((e1) => {
      if (arr2.indexOf(e1) !== -1) {
        count++
      }
    })

    if (arr2.length === count) {
      return true
    } else {
      return false
    }
  },

  prefixRouter: (prefix, routes) =>
    routes.map((route) => {
      route.path = prefix + route.path;
      return route;
    }),

  checkIsEmail(data) {
    const regex = new RegExp(this.regexEmail())
    return regex.test(data)
  },

  checkIsUrl(data) {
    const regex = new RegExp(/(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/)
    return regex.test(data)
  },

  regexPhone() {
    return /^0[0-9]{3}[\s]{1}[0-9]{3}[\s]{1}[0-9]{3,5}$/
  },

  replacePhone(data) {
    return data.replace(/(\d{4})(\d{3})(\d{3,5})/, '$1 $2 $3')
  },

  regexEmail() {
    return /^[a-z0-9]+[._-]?[a-z0-9]*@[a-z0-9]+[-]?[a-z0-9]+(\.[s]+)*(\.[a-z]{2,3}){1,2}$/i
  },

  getCustomerInfo(info, configs) {
    const attCodes = info.map(t => t.attribute_code)
    const currentConfigs = configs.filter(t => attCodes.indexOf(t.attribute_code) !== -1)
    const currentConfigCodes = currentConfigs.map(t => t.attribute_code)
    info.map(item => {
      const index = currentConfigCodes.indexOf(item.attribute_code)
      if (index === -1) {
        return
      }
      item.sort_order = currentConfigs[index].sort_order || item.sort_order
    })
    info.sort((a, b) => (a.sort_order > b.sort_order) ? 1 : ((b.sort_order > a.sort_order) ? -1 : 0))
    return info
  },

  convertNumberDecimal(value = "") {
    let newValue = String(value);
    if (newValue.length > 0) {
      newValue = parseFloat(newValue.replace(",", "."));
    }
    return newValue;
  },

  formatPhone(phone) {
    if (!phone) {
      return
    }
    return phone.replace(/(\d{4})(\d{3})(\d{3,5})/, '$1 $2 $3')
  }
}