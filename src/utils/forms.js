import Vue from 'vue'

import InputText from '@/components/forms/input/Input.vue'
Vue.component('s-input-text', InputText)

import InputTextDetailsUpdate from '@/components/forms/input-details-update/InputDetailsUpdate.vue'
Vue.component('s-input-text-details-update', InputTextDetailsUpdate)

import InputPhone from '@/components/forms/input-phone/InputPhone.vue'
Vue.component('s-input-phone', InputPhone)

import InputEmail from '@/components/forms/input-email/InputEmail.vue'
Vue.component('s-input-email', InputEmail)

import InputDate from '@/components/forms/input-date/InputDate.vue'
Vue.component('s-input-date', InputDate)

import InputDateTime from '@/components/forms/input-date-time/InputDateTime.vue'
Vue.component('s-input-date-time', InputDateTime)

import InputTime from '@/components/forms/input-time/InputTime.vue'
Vue.component('s-input-time', InputTime)

import InputNumberCustom from '@/components/forms/input-number-custom/InputNumberCustom.vue'
Vue.component('s-input-number-custom', InputNumberCustom)

import InputNumber from '@/components/forms/input-number/InputNumber.vue'
Vue.component('s-input-number', InputNumber)

import InputPrice from '@/components/forms/input-price/InputPrice.vue'
Vue.component('s-input-price', InputPrice)

import Textarea from '@/components/forms/textarea/Textarea.vue'
Vue.component('s-textarea', Textarea)

import Select from '@/components/forms/select/Select.vue'
Vue.component('s-select', Select)

import SelectSearch from '@/components/forms/select-search/SelectSearch.vue'
Vue.component('s-select-search', SelectSearch)

import SelectMultiple from '@/components/forms/select-multiple/SelectMultiple.vue'
Vue.component('s-select-multiple', SelectMultiple)

import CustomSelect from '@/components/forms/select-custom/SelectCustom.vue'
Vue.component('s-custom-select', CustomSelect)

import Tags from '@/components/forms/select-tag/Tags.vue'
Vue.component('s-tags', Tags)

import CheckboxMultiple from '@/components/forms/checkbox/CheckboxMultiple.vue'
Vue.component('s-checkbox-multiple', CheckboxMultiple)

import RadioMultiple from '@/components/forms/radio/RadioMultiple.vue'
Vue.component('s-radio-multiple', RadioMultiple)
