import XLSX from 'xlsx'
import utils from './utils'

export default {
  exportToExcel(data = [], attributeData = [], listUser = [], key = 'customer') {
    if (data && data.length > 0 && attributeData && attributeData.length > 0) {
      const dataAfterMapping = this.mappingDataForExcel(data, attributeData, listUser, key)
      const wscols = [
        { wpx: 100 }, // 1 Mã khách hàng
        { wpx: 180 }, // 2 Tên khách hàng
        { wpx: 100 }, // 3 Số điệ thoại
        { wpx: 180 }, // 4 Email
        { wpx: 250 }, // 5 Địa chỉ
        { wpx: 200 }, // 6 Loaị khách hàng
        { wpx: 50 }, // 7 Giới tính
        { wpx: 150 }, // 8 Tổng doanh số
        { wpx: 150 }, // 9 Số sản phẩm đã mua
        { wpx: 100 }, // 10 Số sản phẩm đã sử dụng
        { wpx: 150 }, // 11 Tổng tiền đã thanh toán
        { wpx: 150 }, // 12 Người phụ trách
        { wpx: 150 }, // 13 Cấp độ khách hàng
        { wpx: 100 }, // 14 Nhóm khách hàng
        { wpx: 100 }, // 15 Nguồn khách hàng
        { wpx: 150 }, // 16 Số CMND/Mã số thuế
        { wpx: 180 }, // 17 Ngày sinh/Ngày thành lập
        { wpx: 100 }, // 18 Facebook
        { wpx: 300 }, // 19 Ghi chú
        { wpx: 150 }, // 20 Tạo bởi
        { wpx: 150 }, // 21 Tạo lúc
        { wpx: 150 }, // 21 Lần chỉnh sửa cuối
        { wpx: 150 }, // 22 Khu vực
        { wpx: 150 }, // 23 Ngành nghề
        { wpx: 150 }, // 24 Trình độ
        { wpx: 150 }, // 25 Chức vụ
        { wpx: 150 }, // 26 Tên công ty
        { wpx: 150 }, // 27 Tên trường học
        { wpx: 150 }, // 28 Học lực
      ]
      const ws = XLSX.utils.aoa_to_sheet(dataAfterMapping)
      ws['!cols'] = wscols
      const wb = XLSX.utils.book_new()
      const title = this.getTitleByKey(key)
      XLSX.utils.book_append_sheet(wb, ws, title)
      /* generate file and send to client */
      const date = new Date()

      const getDate = ('0' + date.getDate()).slice(-2)
      const getMonth = ('0' + date.getMonth() + 1).slice(-2)
      const getFullYear = date.getFullYear()

      const prefixFile = this.getFileNameByKey(key)
      const fileName = prefixFile + getDate + getMonth + getFullYear + '.xlsx'

      XLSX.writeFile(wb, fileName)
    }
  },

  mappingDataForExcel(data, attributeData, listUser, key) {
    let result = []
    if (key === 'customer') {
      result = this.mappingDataCustomerForExcel(data, attributeData, listUser)
    }
    return result
  },
  mappingDataCustomerForExcel(data, attributeData, listUser) {
    const result = []
    const firstRows = []
    attributeData.forEach((attribute) => {
      if (attribute.attribute_code === 'avatar') {
        return
      }
      firstRows.push(attribute.attribute_name)
    })
    result.push(firstRows)
    const customerTypes = attributeData[0].customer_type
    data.forEach((customer) => {
      const dataExcel = []

      attributeData.forEach((attribute) => {
        if (attribute.attribute_code === 'avatar') {
          return
        }
        let resultValue = ''
        if (attribute.attribute_code === 'charge_person') {
          let attrValue = ''
          if (customer.assigners && customer.assigners.length > 0) {
            customer.assigners.forEach((assignerId) => {
              const user = listUser.find((choosen) => {
                return choosen.id === assignerId
              })

              if (user) {
                attrValue += user.full_name + ', '
              }
            })
            const value = attrValue.trim()
            resultValue = value ? value.substring(0, value.length - 1) : ''
          }
        } else if (attribute.attribute_code === 'type_customer') {
          if (customerTypes !== 'undefined') {
            const attrValue = customerTypes.find((type) => {
              return customer.customer_type_id === type.id
            })
            resultValue = attrValue.customer_type_name
          } else {
            resultValue = ''
          }
        } else if (attribute.attribute_code === 'user_id') {
          let attrValue = ''
          const user = listUser.find((choosen) => {
            return choosen.id === customer.user_id
          })

          attrValue = user && user.full_name ? user.full_name : ''

          resultValue = attrValue.trim()

        } else if (attribute.attribute_code === 'created_at') {
          resultValue = customer.created_at
        } else if (attribute.attribute_code === 'updated_at') {
          resultValue = customer.updated_at
        } else {
          if (typeof customer.attributes !== 'undefined') {
            let valueAttribute = customer.attributes.find((item) => {
              return item.attribute_id === attribute.id
            })
            if (valueAttribute && typeof valueAttribute !== 'undefined' && valueAttribute !== '') {
              valueAttribute = valueAttribute.value
              if (attribute.attribute_options && attribute.attribute_options.length > 0) {
                const value = attribute.attribute_options.find((itemOptions) => {
                  return itemOptions.id === valueAttribute
                })
                if (typeof value !== 'undefined') {
                  valueAttribute = value.option_value
                } else {
                  valueAttribute = ''
                }
              }
              if (attribute.attribute_code === 'birthday_set_to_date') {
                valueAttribute = utils.formatDateTime(valueAttribute)
              }
              resultValue = valueAttribute
            } else {
              resultValue = ''
            }
          } else {
            resultValue = ''
          }
        }
        dataExcel.push(resultValue)
      })
      result.push(dataExcel)
    })
    return result
  },

  getFileNameByKey(key) {
    let result = ''
    if (key === 'customer') {
      result = 'danh-sach-khach-hang-'
    }
    return result
  },

  getTitleByKey(key) {
    let result = ''
    if (key === 'customer') {
      result = 'Khách hàng'
    }
    return result
  },
}
