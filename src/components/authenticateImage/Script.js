/* eslint-disable no-undef */
import BaseApi from "@/services/BaseApi"
import jwt from "@/services/Jwt"

export default {
  props: {
    image_id: {
      type: String,
    },

    image_url: {
      type: String,
      required: true,
    },
  },
  watch: {
    image_url: {
      handler(after) {
        if (after) {
          this.setAuthenticationImage(after);
        }
      },
      deep: true
    }
  },
  mounted() {
    if (this.image_url) {
      this.setAuthenticationImage(this.image_url);
    }
  },
  data() {
    return {
      isLoader: false,
      imageUrl: ''
    };
  },
  methods: {
    setAuthenticationImage(image_url) {
      this.imageUrl = image_url + "&token=" + jwt.getToken();

      // BaseApi.getByBlob(image_url).then((res) => {
      //   if (res.data) {
      //     this.isLoader = false;
      //     const url = window.URL.createObjectURL(new Blob([res.data]));
      //     let img = document.createElement("img");
      //     img.setAttribute('src', url);
      //     document.getElementById(this.image_id).appendChild(img);
      //   }
      // })
    },
    downloadImg() {
      BaseApi.getByBlob(this.image_url).then((res) => {
        if (res.data) {
          const url = window.URL.createObjectURL(new Blob([res.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", this.file_name);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      })
    }
  }
};