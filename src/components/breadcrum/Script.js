import { mapGetters } from "vuex";

import * as config from "@/types/config";

export default {
  data() {
    return {
      idpUrl: config.ID_BASE_URL
    }
  },
  computed: {
    ...mapGetters(["currentTeam"]),
    breadcrumbList() {
      let vm = this;
      let breads = vm.$route.meta.breadcrumb;
      let breadCrumbs = [];
      if (breads && breads.length > 0) {
        breads.forEach(function (item) {
          let arrow = { name: "arrow-icon" };
          if (item.link && item.link.indexOf(":team_id") > -1) {
            let position = item.link.indexOf(":team_id");
            let firstString = item.link.substring(0, position);
            let lastString = item.link.substring(position + 8);
            let newString = firstString + vm.$route.params.team_id + lastString;
            item.link = newString;
          }
          breadCrumbs.push(item);
          breadCrumbs.push(arrow);
        });
        breadCrumbs.splice(breadCrumbs.length - 1, 1);
      }
      return breadCrumbs;
    }
  },
};
