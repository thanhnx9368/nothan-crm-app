import {
  mapGetters
} from "vuex";

export default {
  computed: {
    // actionLists(){
    //     let vm = this;
    // },
    currentDealId() {
      let vm = this;
      return vm.$route.params.deal_id;
    },

    currentCustomerId() {
      let vm = this;
      return vm.$route.params.customer_id;
    },
    
    ...mapGetters(["userCurrentPermissions", "userCheckAddOn"]),
  },
  watch: {
    '$route'() {
      let vm = this;
      vm.actionLists = vm.getActionLists();
    }
  },
  mounted() {
    let vm = this
    vm.getAsync()
  },
  data() {
    return {
      actionLists: []
    }
  },
  methods: {
    async getAsync() {
      this.actionLists = await this.getActionLists()
      this.reloadTooltip()
    },
    getActionLists() {
      let vm = this;
      let actionLists = [];
      let actions = vm.$route.meta.actions;
      if (actions && actions.length > 0) {
        actions.forEach(function (item) {
          let groups = [];
          if (item.groups) {
            item.groups.forEach(function (group, index) {
              if (index == 0) {
                group.active = 1;
              } else {
                group.active = 0;
              }
              groups.push(group);
            })
            item.groups = groups;
          }
          actionLists.push(item);
        });
      }
      return actionLists;
    },

    actionClick(name_function) {
      let vm = this;
      if (name_function) {
        vm.$bus.$emit(name_function, true);
      }
    },

    hideDropdown(id) {
      const dropdown = document.getElementById(id)
      dropdown.click()
    }
  }
}