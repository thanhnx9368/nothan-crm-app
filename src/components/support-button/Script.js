export default {
  data() {
    return {
      isShowPopup: false,
      data: [{
        icon: 'icon-phone-call',
        text: 'Hotline:',
        subtext: '0922 99 55 68',
        href: 'tel:+84 922 99 55 68'
      }, {
        icon: 'icon-messenger',
        text: 'Chat trực tuyến',
        href: 'https://www.messenger.com/t/438037933602346/?messaging_source=source%3Apages%3Amessage_shortlink'
      }, {
        icon: 'icon-chat-2',
        text: 'Gửi phản hồi',
        href: 'https://nothan.vn/ho-tro-khach-hang-phan-mem-no-than/?utm_source=appnothan&utm_medium=hotrokhachhang&utm_campaign=formhotrokh'
      }, {
        icon: 'icon-play',
        text: 'Xem hướng dẫn sử dụng',
        href: 'https://nothan.vn/video-huong-dan-su-dung-no-than/?utm_source=appnothan&utm_medium=hdsdnothan&utm_campaign=hdsdnothan"'
      }]
    }
  },
}
