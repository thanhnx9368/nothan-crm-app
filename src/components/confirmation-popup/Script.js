export default {
  props: {
    id: '',
    title: {
      type: String,
      default: 'Thông báo',
    },
    content: '',
    icon: '',
    cancelText: {
      type: String,
      default: 'Huỷ bỏ',
    },
    confirmText: {
      type: String,
      default: 'Đồng ý'
    },
    showButton: {
      type: Object,
      default: () => {
        return {
          cancel: true,
          confirm: true
        }
      }
    }
  },
  methods: {
    submit() {
      this.$emit('confirmed')
    },

    cancel() {
      this.$emit('cancel')
    }
  }
}