/* eslint-disable no-undef */
import { mapGetters } from "vuex";

export default {
  data() {
    return {
      nameAddOnIsAuthor: '',
    }
  },
  computed: {
    ...mapGetters(["appInfoObj", "currentTeam"]),
  },
  created() {
    this.$bus.$on('checkAuthorAddCustomer', ($event) => {
      if ($event) {
        this.nameAddOnIsAuthor = $event;
        $('#popupPolicyAddCustomer').modal('show');
      }

    })

  },
}
