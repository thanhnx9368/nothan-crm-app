export default {
  props: {
    data: {
      type: Array,
      default: []
    },
    name: {
      type: String,
      default: "s_attribute"
    },
    label: {
      type: String,
      default: "name"
    },
    value: {
      type: [String, Number],
      default: ""
    },
    col: {
      type: String,
      default: "col-md-12"
    }
  },
  watch: {
    value: {
      handler(after) {
        if (after) {
          this.new_value = after;
        } else {
          this.new_value = '';
        }
      },
      deep: true
    },
    new_value: {
      handler(after) {
        if (after) {
          this.new_value = after;
        } else {
          this.new_value = '';
        }
        this.$emit('input', after)
        this.$emit('keyup', after)
      },
      deep: true
    },
  },
  computed: {},
  data() {
    return {
      new_value: this.value
    };
  },
  methods: {}
};