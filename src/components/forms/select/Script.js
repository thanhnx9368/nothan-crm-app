export default {
  props: {
    data: {
      type: Array,
      default: []
    },
    label: {
      type: String,
      default: "name"
    },
    value: {
      type: [String, Number, Array],
      default: ""
    },
    option_value: {
      type: String,
      default: "id"
    }
  },
};