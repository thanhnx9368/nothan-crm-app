/* eslint-disable no-useless-escape */
export default {
  props: {
    value: {
      type: [String, Number],
      default: ""
    },
    check_number: {
      type: Boolean,
      default: false
    }
  },
  data: function () {
    return {
      isInputActive: false
    };
  },

  computed: {
    displayValue: {
      get: function () {
        if (this.value && this.value != "null") {
          if (this.isInputActive) {
            // Cursor is inside the input field. unformat display value for user
            return String(this.value);
          } else {
            // User is not modifying now. Format display value for user interface
            // return "$ " + this.value.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")
            if (this.check_number && isNaN(this.value)) {
              return this.value;
            }

            if (this.value) {
              this.$bus.$emit(
                "updateFormatValue",
                parseInt(this.value, 10).toString()
              );
              return String(this.value).replace(
                /(\d)(?=(\d{3})+(?:\.\d+)?$)/g,
                "$1."
              );
            }
            return "";
          }
        }
      },
      set: function (modifiedValue) {
        // Recalculate value after ignoring "$" and "," in user input
        let newValue = parseFloat(modifiedValue.replace(/[^\d\.]/g, ""));
        // Ensure that it is not NaN

        if (this.check_number || isNaN(newValue)) {
          newValue = modifiedValue;
        }

        if (!this.check_number && isNaN(newValue)) {
          newValue = "";
        }
        // Note: we cannot set this.value as it is a "prop". It needs to be passed to parent component
        // $emit the event so that parent component gets it
        this.$emit("input", newValue);
        this.$bus.$emit("inputActive", this.isInputActive);
      }
    }
  },
};