/* eslint-disable no-undef */
import {
  mapState,
  mapGetters
} from "vuex";

import {
  ID_BASE_URL
} from "@/types/config";
import {
  PERMISSION_STATEMENT_SALE,
  PERMISSION_STATEMENT_CUSTOMER
} from "@/types/const"
import {
  DELETE_DEVICE_TOKEN_NOTI,
  ORGANIZATION_CURRENT
} from "@/types/actions.type";

import Notifications from '../notifications/Notifications.vue'
import TodoList from '../todo-list/TodoList'
import JwtService from "@/services/Jwt";
import AuthenticateImage from "@/components/authenticateImage/AuthenticateImage.vue";
import Usage from '@/components/usage/Usage.vue'

export default {
  components: {
    AuthenticateImage,
    Notifications,
    TodoList,
    Usage
  },
  data() {
    return {
      PERMISSION_STATEMENT_SALE: PERMISSION_STATEMENT_SALE,
      PERMISSION_STATEMENT_CUSTOMER: PERMISSION_STATEMENT_CUSTOMER,
      idpAddOrganizationUrl: ID_BASE_URL + '/organization/add',
      idpUserInfo: ID_BASE_URL + '/user/info',
      idpUserChangePassword: ID_BASE_URL + '/user/change-password',
      idpChooseOrganization: ID_BASE_URL + '/choose-organization'
    }
  },
  watch: {
    currentTeamId: {
      handler(after) {
        this.checkIsCurrentTeam(after);
      },
      deep: true
    },
  },
  mounted() {
    this.reloadTooltip();
  },
  computed: {
    ...mapState({
      currentTeam: state => state.organization.currentTeam,
      currentUser: state => state.user.currentUser,
      organizations: state => state.organization.list
    }),
    currentRoleId() {
      return this.currentTeam.role_id;
    },
    ...mapGetters(["userCheckAddOn", "userCurrentPermissions", "appInfo", "appInfoObj"]),
  },
  methods: {
    checkIsCurrentTeam(team_id) {
      if (team_id > 0) {
        this.$store.dispatch(ORGANIZATION_CURRENT, team_id)
      }
    },
    async logout() {
      let vm = this;
      await vm.deleteDeviceTokenNoti();
      JwtService.destroyToken();
      return window.location.href = ID_BASE_URL + '/login';
    },
    getCurentRoleByTeamId() {
      let vm = this;
      let listTeam = vm.currentUser && vm.currentUser.teams ? vm.currentUser.teams : {};
      let roleCurrentTeam = {};

      roleCurrentTeam = listTeam ? listTeam[vm.currentTeamId] : '';
      return roleCurrentTeam ? roleCurrentTeam : '';
    },
    redirectOrganizationInfo(teamId) {
      return ID_BASE_URL + '/team/' + teamId + "/info";
    },
    redirectLaunchpad(teamId) {
      return ID_BASE_URL + '/team/' + teamId + "/launchpad";
    },
    reloadTooltip() {
      $(".nt-header-nav-item-organization").tooltip({
        selector: '[data-toggle="m-tooltip"]',
        template: '<div class="m-tooltip tooltip nt-tooltip-organization-setting" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
      });
    },

    async deleteDeviceTokenNoti() {
      let params = {
        type_token: 'web'
      }
      await this.$store.dispatch(DELETE_DEVICE_TOKEN_NOTI, params)
    }
  }
};