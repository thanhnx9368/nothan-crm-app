export const state = {
  myJob: [{
      name: 'Chưa thực hiện',
      data: [{
        id: 1,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 1',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-01-01 20:00:00',
        status: 1
      }, {
        id: 2,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 2',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-02-01 20:00:00',
        status: 2
      }, {
        id: 3,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 3',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-03-01 20:00:00',
        status: 1
      }]
    },
    {
      name: 'Đang thực hiện',
      data: [{
        id: 4,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 4',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-04-01 20:00:00',
        status: 1
      }, {
        id: 5,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 5',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-05-01 20:00:00',
        status: 3
      }, {
        id: 6,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 6',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-06-01 20:00:00',
        status: 1
      }]
    },
    {
      name: 'Đã hoàn thành',
      data: [{
        id: 7,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 7',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-07-01 20:00:00',
        status: 1
      }, {
        id: 8,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 8',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-08-01 20:00:00',
        status: 2
      }, {
        id: 9,
        title: 'Gửi email giới thiệu khóa TNSĐ chuyên sâu cho phụ huynh khóa TNSĐ cơ bản tháng 9',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        deadline: '2019-09-01 20:00:00',
        status: 3
      }]
    }
  ]
}