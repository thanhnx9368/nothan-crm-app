/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";
import {
  EMAIL_SCHEDULER_LIST
} from "@/types/actions.type";

import EmailSchedulerPopup from '../../../components/email-scheduler-popup/EmailSchedulerPopup.vue'
import UserObjectLoading from "@/components/loading/UserObject"
import LoadingTable from "@/components/loading/LoadingTable.vue"
import Pagination from "@/components/pagination/Pagination.vue"

export default {
  components: {
    EmailSchedulerPopup,
    UserObjectLoading,
    Pagination,
    LoadingTable
  },
  created() {
    this.reloadTooltip();
  },
  mounted() {
    this.getEmailSchedulerList();
  },
  computed: {
    ...mapGetters(['currentTeam', 'emailSchedulerList'])
  },
  data() {
    return {
      isLoaderList: false,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {},
    }
  },
  methods: {
    getEmailSchedulerList() {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      if (vm.currentParams.per_page) {
        delete vm.currentParams.per_page;
      }
      vm.isLoaderList = true;
      vm.$store.dispatch(EMAIL_SCHEDULER_LIST, vm.currentParams).then(response => {
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.isLoaderList = false;
      }).catch(() => {});
    },
    getPagination() {
      this.getEmailSchedulerList();
    }
  }
}