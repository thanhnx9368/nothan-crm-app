/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";
import moment from 'moment'
import VueLadda from "vue-ladda";

import EmailSchedulerAddFilter from "../../../components/email-scheduler-add-filter/EmailSchedulerAddFilter.vue"
import {
  EMAIL_SCHEDULER_ADD,
  EMAIL_SCHEDULER_DETAIL
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";
import helpers from '@/utils/utils';
import EmailSchedulerEmailList from '../../../components/email-scheduler-email-list/EmailSchedulerEmailList.vue'
import EmailApi from '@/services/customer-care'
import {
  CustomerCareVM
} from '../../../transformers/index'

const DAY_TYPE = 1;
const WEEK_TYPE = 2;
const MONTH_TYPE = 3;
const YEAR_TYPE = 4;

const customMessagesAddNewEmailScheduler = {
  custom: {
    'email_scheduler_name': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'filter_id': {
      required: 'Không được để trống trường này.'
    },
    'start_date': {
      required: 'Không được để trống trường này.'
    },
    'start_on': {
      required: 'Không được để trống trường này.'
    },
    'repeat_time_value': {
      required: 'Không được để trống trường này.',
      min_value: "Giá trị nhỏ nhất là 1."
    }
  }
};

export default {
  components: {
    EmailSchedulerEmailList,
    EmailSchedulerAddFilter,
    VueLadda,
  },
  created() {
    this.$bus.$on('emailSchedulerAddFilterEmit', ($event) => {
      if ($event) {
        this.emailScheduler.filter_id = $event;
      }
    })
    const {
      email_scheduler_id
    } = this.$route.params
    if (email_scheduler_id) {
      this.isEdit = true
      this.getEmailSchedulerDetails(email_scheduler_id)
    } else {
      this.setEmailScheduler()
    }
  },
  computed: {
    ...mapGetters(['currentTeam', 'emailSchedulerDetails']),
    helpers() {
      return helpers;
    },
    isDisabled() {
      let isChange = true;
      if (this.isEdit) {
        let emailSchedulerIsActive = this.emailScheduler.is_active ? 1 : 0;
        let freezeEmailSchedulerIsActive = this.freezeEmailScheduler.is_active ? 1 : 0;

        let emailSchedulerStartDate = this.emailScheduler.start_date ? this.helpers.formatDateTimeForClient(this.emailScheduler.start_date) : '';
        let freezeEmailSchedulerStartDate = this.freezeEmailScheduler.start_date ? this.helpers.formatDateTimeForClient(this.freezeEmailScheduler.start_date) : '';

        let emailSchedulerStartOn = this.emailScheduler.start_on ? this.helpers.formatTime(this.emailScheduler.start_on) : '';
        let freezeEmailSchedulerStartOn = this.freezeEmailScheduler.start_on ? this.helpers.formatTime(this.freezeEmailScheduler.start_on) : '';

        let emailSchedulerEmailTemplateIds = this.emailScheduler.email_template_ids ? JSON.stringify(this.emailScheduler.email_template_ids) : [];
        let freezeEmailSchedulerEmailTemplateIds = this.freezeEmailScheduler.email_template_ids ? JSON.stringify(this.freezeEmailScheduler.email_template_ids) : [];

        if (this.emailScheduler.name === this.freezeEmailScheduler.name &&
          this.emailScheduler.filter_id === this.freezeEmailScheduler.filter_id &&
          this.emailScheduler.repeat_time_value === this.freezeEmailScheduler.repeat_time_value &&
          this.emailScheduler.repeat_time_type === this.freezeEmailScheduler.repeat_time_type &&
          emailSchedulerEmailTemplateIds === freezeEmailSchedulerEmailTemplateIds &&
          emailSchedulerIsActive === freezeEmailSchedulerIsActive &&
          emailSchedulerStartDate === freezeEmailSchedulerStartDate &&
          emailSchedulerStartOn === freezeEmailSchedulerStartOn
        ) {
          isChange = false;
        }
      }

      if (!isChange || !this.emailScheduler.name || !this.emailScheduler.start_date || !this.emailScheduler.start_on || !this.emailScheduler.repeat_time_value || !this.emailScheduler.filter_id || this.emailScheduler.email_template_ids.length <= 0 || this.errors.any() || this.isLoader) {
        return true
      }

      return false;
    },
  },
  mounted() {
    this.$validator.localize('en', customMessagesAddNewEmailScheduler);
  },
  data() {
    return {
      isEdit: false,
      isLoader: false,
      isLoaderEmailSchedulerDetails: false,
      isSuccess: false,
      server_errors: '',
      emailSchedulerId: '',
      emailScheduler: {},
      freezeEmailScheduler: {},
      isAllData: [],
      repeatTimes: [{
        type: DAY_TYPE,
        name: 'Ngày'
      }, {
        type: WEEK_TYPE,
        name: 'Tuần'
      }, {
        type: MONTH_TYPE,
        name: 'Tháng'
      }, {
        type: YEAR_TYPE,
        name: 'Năm'
      }],
      repeatTimeDefault: [{
        type: DAY_TYPE,
        value: 1,
        name: '1 ngày'
      }, {
        type: WEEK_TYPE,
        value: 1,
        name: '1 tuần'
      }, {
        type: MONTH_TYPE,
        value: 1,
        name: '1 tháng'
      }, {
        type: YEAR_TYPE,
        value: 1,
        name: '1 năm'
      }, {
        type: 0,
        value: '',
        name: 'Khác'
      }]
    }
  },
  methods: {
    removeAllEmailContents() {
      this.$refs.emailSchedulerEmailList.removeAll()
    },

    changeRepeatTimeDefault() {
      let vm = this;
      if (vm.emailScheduler && vm.emailScheduler.repeat_time && vm.emailScheduler.repeat_time.type !== 0) {
        vm.emailScheduler.repeat_time_type = vm.emailScheduler.repeat_time.type;
        vm.emailScheduler.repeat_time_value = vm.emailScheduler.repeat_time.value;
      } else {
        vm.emailScheduler.repeat_time_type = DAY_TYPE;
        vm.emailScheduler.repeat_time_value = 1;
      }
      this.onUpdateDate()
    },

    async getEmailSchedulerDetails(id) {
      this.isLoaderEmailSchedulerDetails = true;
      await this.$store.dispatch(EMAIL_SCHEDULER_DETAIL, id).then((res) => {
        if (res.data.data) {
          this.isLoaderEmailSchedulerDetails = false
        }
      })
      this.setEmailScheduler();
    },

    setEmailScheduler() {
      let today = new Date();
      if (this.isEdit) {
        this.emailScheduler = this.emailSchedulerDetails;
      } else {
        this.emailScheduler = {
          name: '',
          is_active: 0,
          filter_id: '',
          start_date: today,
          start_on: '1970-01-01 09:00:00',
          repeat_time: {
            type: MONTH_TYPE,
            value: 1,
            name: '1 tháng'
          },
          repeat_time_type: MONTH_TYPE,
          repeat_time_value: 1,
          email_template_ids: [],
          end_date: ''
        }
      }

      this.freezeEmailScheduler = Object.assign({}, this.emailScheduler)
      Object.freeze(this.freezeEmailScheduler)

      let {
        email_template_ids,
        templates_schedules,
        send_status
      } = this.emailScheduler

      if (email_template_ids.length) {
        this.$refs.emailSchedulerEmailList.getEmails(email_template_ids, templates_schedules, send_status)
      }
    },

    onUpdateDate() {
      this.onUpdateEmailIds(this.emailScheduler.email_template_ids)
    },

    onUpdateTemplateScheduler(event) {
      this.emailScheduler['templates_schedules'] = event
    },

    onUpdateEmailIds(event) {
      if (!event.length) {
        this.emailScheduler.end_date = ''
        this.emailScheduler.email_template_ids = []
        return
      }
      const {
        start_date,
        repeat_time_type,
        repeat_time_value
      } = this.emailScheduler
      const _value = event && (event.length - 1) * repeat_time_value
      let value
      switch (repeat_time_type) {
        case 1:
          value = moment(start_date).add(_value, 'd')
          break

        case 2:
          value = moment(start_date).add(_value, 'w')
          break

        case 3:
          value = moment(start_date).add(_value, 'M')
          break

        case 4:
          value = moment(start_date).add(_value, 'y')
          break
      }
      this.emailScheduler.email_template_ids = event
      this.emailScheduler.end_date = value.format('DD/MM/YYYY')
    },

    cancelAddNewEmailScheduler() {
      if (this.isEdit) {
        const {
          email_scheduler_id
        } = this.$route.params
        this.$router.push({
          name: 'emailSchedulerDetails',
          params: {
            email_scheduler_id
          }
        })
      } else {
        this.$router.push({
          name: 'emailSchedulerList'
        })
      }
    },

    addNewEmailScheduler(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          if (vm.emailScheduler && vm.emailScheduler.repeat_time && vm.emailScheduler.repeat_time.type !== 0) {
            vm.emailScheduler.repeat_time_type = vm.emailScheduler.repeat_time.type;
            vm.emailScheduler.repeat_time_value = vm.emailScheduler.repeat_time.value;
          }

          vm.onSendData().then((response) => {
            vm.isLoader = false;
            if (response.data.data && response.data.data.status) {
              vm.isSuccess = true;
              vm.emailSchedulerId = response.data.data.id;
              if (this.isEdit) {
                vm.$snotify.success('Chỉnh sửa lịch gửi thành công.', TOAST_SUCCESS);
              } else {
                vm.$snotify.success('Thêm mới lịch gửi thành công.', TOAST_SUCCESS);
              }

              $(".m-portlet__body").animate({
                scrollTop: 0
              }, 300);
              vm.$router.push({
                name: "emailSchedulerDetails",
                params: {
                  team_id: vm.currentTeamId,
                  email_scheduler_id: vm.emailSchedulerId
                }
              });
              return;
            }

            if (response.data.data && !response.data.data.status) {
              if (this.isEdit) {
                vm.$snotify.error('Chỉnh sửa lịch gửi thất bại.', TOAST_ERROR);
              } else {
                vm.$snotify.error('Thêm mới lịch gửi thất bại.', TOAST_ERROR);
              }

              return;
            }

            if (response.data.status_code && response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              if (this.isEdit) {
                vm.$snotify.error('Chỉnh sửa lịch gửi thất bại.', TOAST_ERROR);
              } else {
                vm.$snotify.error('Thêm mới lịch gửi thất bại.', TOAST_ERROR);
              }
              return;
            }
          }).catch(() => {});
        }
      });
    },

    onSendData() {
      const {
        email_scheduler_id
      } = this.$route.params
      const sendEmail = CustomerCareVM.addEmailSchedulerVM.transform(this.emailScheduler);
      if (this.isEdit) {
        sendEmail['templates_schedules'] = this.emailScheduler['templates_schedules']
        return EmailApi.emailSchedulerUpdate(email_scheduler_id, sendEmail)
      }
      return this.$store.dispatch(EMAIL_SCHEDULER_ADD, sendEmail)
    }
  }
}