/* eslint-disable no-undef */

export const getters = {
  emailTemplateList(state) {
    return state.emailTemplateList;
  },
  emailTemplateDetails(state) {
    return state.emailTemplateDetails;
  },
  emailSchedulerList(state) {
    return state.emailSchedulerList;
  },
  emailSchedulerDetails(state) {
    return state.emailSchedulerDetails;
  }

}