import EmailSchedulerList from './views/email-scheduler/list/List.vue'
import EmailTemplateDetails from './views/email-template/show/Show.vue'
import EmailTemplateList from './views/email-template/list/List.vue'
import EmailSchedulerAdd from './views/email-scheduler/add/Add.vue'
import EmailTemplateAdd from './views/email-template/add/Add.vue'
import EmailTemplateEdit from './views/email-template/edit/Edit.vue'
import emailSchedulerDetails from './views/email-scheduler/show/Show.vue'

export default [{
  name: "emailTemplateList",
  path: "/email-template",
  component: EmailTemplateList,
  meta: {
    breadcrumb: [{
      name: "Quản lý mẫu email",
    }, {
      name: "Danh sách mẫu email",
    }],
    actions: [{
      type: 4,
      class: "nt-button nt-button-top nt-button-warning",
      name: "Thêm mẫu email",
      isIcon: 1,
      iconClass: "icon icon-plus",
      nameFunction: "addNewEmailTemplate",
      position: 'before',
      permissionActionName: 'all',
      serviceName: "customer"
    }]
  },
}, {
  name: "emailTemplateAdd",
  path: "/email-template/add",
  component: EmailTemplateAdd,
  meta: {
    breadcrumb: [{
      name: "Quản lý mẫu email",
    }, {
      name: "Danh sách mẫu email",
      link: "/team/:team_id/email-template"
    }, {
      name: "Thêm mẫu email",
    }]
  },
}, {
  name: "emailTemplateDetails",
  path: "/email-template/:email_template_id",
  component: EmailTemplateDetails,
  meta: {
    breadcrumb: [{
      name: "Quản lý mẫu email",
    }, {
      name: "Danh sách mẫu email",
      link: "/team/:team_id/email-template"
    }, {
      name: "Chi tiết mẫu email",
    }],
    actions: [{
      type: 4,
      class: "nt-button nt-button-top nt-button-warning",
      name: "Chỉnh sửa",
      isIcon: 1,
      iconClass: "icon icon-edit",
      nameFunction: "editEmailTemplate",
      position: 'before',
      permissionActionName: 'all',
      serviceName: "customer"
    }]
  },
}, {
  name: "emailTemplateEdit",
  path: "/email-template/:email_template_id/edit",
  component: EmailTemplateEdit,
  meta: {
    breadcrumb: [{
      name: "Quản lý mẫu email",
    }, {
      name: "Danh sách mẫu email",
      link: "/team/:team_id/email-template"
    }, {
      name: "Chỉnh sửa mẫu email",
    }]
  },
}, {
  name: "emailSchedulerList",
  path: "/email-scheduler",
  component: EmailSchedulerList,
  meta: {
    breadcrumb: [{
      name: "Quản lý lịch gửi email tự động",
    }, {
      name: "Danh sách lịch gửi",
    }],
    actions: [{
      type: 1,
      class: "nt-button nt-button-top nt-button-warning",
      name: "Thêm lịch gửi email",
      isIcon: 1,
      iconClass: "icon icon-plus",
      isModal: 1,
      idModal: "#emailSchedulerPopup",
      permissionActionName: 'all',
      serviceName: "customer"
    }]
  },
}, {
  name: "emailSchedulerAdd",
  path: "/email-scheduler-add",
  component: EmailSchedulerAdd,
  meta: {
    breadcrumb: [{
      name: "Quản lý lịch gửi email tự động",
    }, {
      name: "Danh sách lịch gửi",
      link: "/team/:team_id/email-scheduler"
    }, {
      name: "Thêm lịch gửi email định kỳ",
    }],
  },
}, {
  name: "emailSchedulerEdit",
  path: "/email-scheduler/:email_scheduler_id/edit",
  component: EmailSchedulerAdd,
  meta: {
    breadcrumb: [{
      name: "Quản lý lịch gửi email tự động",
    }, {
      name: "Danh sách lịch gửi",
      link: "/team/:team_id/email-scheduler"
    }, {
      name: "Sửa lịch gửi email định kỳ",
    }],
  },
}, {
  name: "emailSchedulerDetails",
  path: "/email-scheduler/:email_scheduler_id",
  component: emailSchedulerDetails,
  meta: {
    breadcrumb: [{
      name: "Quản lý lịch gửi email tự động",
    }, {
      name: "Danh sách lịch gửi",
      link: "/team/:team_id/email-scheduler"
    }, {
      name: "Chi tiết lịch gửi email định kỳ",
    }],
    actions: [{
      type: 4,
      class: "nt-button nt-button-top nt-button-warning",
      name: "Chỉnh sửa",
      isIcon: 1,
      iconClass: "icon icon-edit",
      nameFunction: 'onEditEmailScheduler',
      permissionActionName: 'all',
      serviceName: "customer",
      position: "before"
    }]
  },
}]