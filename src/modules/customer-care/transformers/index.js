import showVM from './show';
import listEmailSchedulerVM from './listEmailScheduler';
import emailTemplateDetailsVM from './emailTemplateDetails';
import addEmailSchedulerVM from './addEmailScheduler';

export const CustomerCareVM = {
  showVM,
  listEmailSchedulerVM,
  emailTemplateDetailsVM,
  addEmailSchedulerVM
}