/* eslint-disable no-undef */
import helpers from '@/utils/utils';
export default {
  transform(data) {
    let transform = {};
    let startDate = data.start_date ? helpers.formatDateTimeForClient(data.start_date) : '';
    let startOn = data.start_on ? helpers.formatTime(data.start_on) : '';

    transform = {
      email_template_ids: data.email_template_ids,
      filter_id: data.filter_id,
      is_active: data.is_active,
      name: data.name,
      repeat_time_type: data.repeat_time_type,
      repeat_time_value: data.repeat_time_value,
      start_date: startDate,
      start_on: startOn,
    };

    return transform;
  },
}