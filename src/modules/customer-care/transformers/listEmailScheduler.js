/* eslint-disable no-undef */
export default {
  transform(data) {
    let vm = this;
    let newData = [];

    data.forEach(function (item) {
      newData.push(vm.transformData(item));
    });

    return newData;
  },

  transformData(data) {
    let transform = {};
    let periodicalDateTimes = JSON.parse(data.periodical_date_times);
    let endDate = periodicalDateTimes && periodicalDateTimes.length > 0 ? periodicalDateTimes[periodicalDateTimes.length - 1] : '';
    transform = {
      id: data.id,
      created_at: data.created_at,
      updated_at: data.updated_at,
      created_by: data.created_by,
      updated_by: data.updated_by,
      name: data.name,
      is_active: data.is_active,
      start_date: data.start_date,
      periodical_date_times: periodicalDateTimes,
      email_template_ids: JSON.parse(data.email_template_ids),
      end_date: endDate
    };
    return transform;
  }
}