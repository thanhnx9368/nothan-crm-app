import CustomerApi from '@/services/customer'

import Loading from '@/components/loading/LoadingTable.vue'

export default {
  components: {
    Loading,
  },
  props: {
    content: ''
  },
  data() {
    return {
      isLoading: false,
      data: '',
      key: 'nt_content',
    }
  },
  created() {
    this.getEmailFrame()
  },
  methods: {
    getEmailFrame() {
      this.isLoading = true
      CustomerApi.emailTemplateEmailFrame({ layout_id: 1 }).then(res => {
        if (res.data.data){
          this.data = res.data.data
          this.isLoading = false
        }
      })
    },

    getPreview() {
      if (!this.data || !this.data.length) {
        return
      }
      return this.data.replace(this.key, this.content)
    }
  }
}