/* eslint-disable no-undef */
import Filter from "@/components/filter/Filter.vue"
import LoadingTable from "@/components/loading/LoadingTable.vue"

import {
  mapGetters
} from "vuex";
import {
  CUSTOMER_GET_FILTER_ATTRIBUTES,
  CUSTOMER_GET_FILTERS,
  GET_COUNT_CUSTOMER_BY_FILTER,
  CUSTOMER_GET_ATTRIBUTES
} from "@/types/actions.type";

export default {
  components: {
    'filter-customer': Filter,
    LoadingTable
  },
  props: {
    filter_id: {
      type: [Number, String],
      required: true,
      default: 0
    },
    server_errors: {
      type: [Object, String],
      default: {}
    },
    is_edit: {
      type: Boolean,
      default: false
    },
    email_scheduler_details: {
      type: Object,
      default: () => {
        return {}
      }
    }
  },
  computed: {
    ...mapGetters(['currentTeam', 'getCustomerFilters', 'attributeFullForFilter', 'attributeCustomerFull', 'filterAttributesCustomer']),
  },
  mounted() {
    let vm = this;
    vm.getDataAsync();
  },

  data() {
    return {
      currentParams: {},
      cacheForFilterAttributes: "",
      isLoaderGetCustomerAttribute: true,
      isLoaderGetCountCustomer: false,
      objectEmit: {},
      filterId: '',
      totalCustomer: 0,
      totalEmailCustomer: 0,
      hasFilterId: true,
      idShowEmailSchedulerMessageFilter: 'show_email_scheduler_message_filter',
      contentMessageFilter: '',
      cancelTextMessageFilter: 'Đóng',
      showButton: {
        cancel: true,
        confirm: false
      }
    }

  },
  methods: {
    async getDataAsync() {
      await this.getAttributes();
      await this.getFilters();
      let filterId = this.filterId;
      if (this.is_edit) {
        filterId = this.filter_id;
      }
      await this.showFilterGetId({
        id: filterId
      });

      await this.$bus.$emit('resetFilterByComponentParent', filterId);
    },

    async getAttributes() {
      let vm = this;
      vm.isLoaderGetCustomerAttribute = true;
      await vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then((res) => {
        if (res.data.data) {
          vm.isLoaderGetCustomerAttribute = false;
        }
      }).catch(() => {});
    },

    async getCountCustomerByFilter() {
      let vm = this;
      vm.hasFilterId = false;

      let filterId = this.filterId;
      if (vm.is_edit) {
        filterId = vm.filter_id;
      }

      if (vm.getCustomerFilters && vm.getCustomerFilters.length > 0) {
        vm.getCustomerFilters.forEach((filter_data) => {
          if (filter_data.id === filterId) {
            vm.hasFilterId = true;
          }
        });
      }

      if (vm.hasFilterId) {
        await vm.$store.dispatch(GET_COUNT_CUSTOMER_BY_FILTER, vm.currentParams).then((res) => {
          if (res.data.data) {
            vm.totalCustomer = res.data.data.customer_count;
            vm.totalEmailCustomer = res.data.data.email_count;
            vm.isLoaderGetCountCustomer = false;
          }
        }).catch(() => {});
      } else {
        vm.isLoaderGetCountCustomer = false;
      }
    },

    async getFilters() {
      let vm = this;
      await vm.$store.dispatch(CUSTOMER_GET_FILTERS).then((res) => {
        if (res.data.data && res.data.data.length > 0) {
          // Set default filter id
          vm.filterId = res.data.data[0] && res.data.data[0].id ? res.data.data[0].id : '';
        }
      }).catch(() => {});
    },

    filterShow(show) {
      let vm = this;
      vm.showFilter = show;
    },

    filterAllData(allData) {
      let vm = this;
      vm.isAllData = allData;
    },

    startFilter(params) {
      let vm = this;
      if (params.filter) {
        vm.currentParams.filter = params.filter;
      } else if (!params.filter && vm.currentParams.filter) {
        delete vm.currentParams.filter;
      }
    },

    showFilterGetId(filter) {
      let vm = this;
      let filter_id = filter && filter.id && Number.isInteger(filter.id) ? filter.id : '';
      if (filter_id) {
        vm.filterId = filter_id;
        vm.isLoaderGetCountCustomer = true;
        delete vm.server_errors.filter_id
        vm.hasFilterId = true;
        vm.getFilterAttributes(filter_id);
        vm.$bus.$emit('emailSchedulerAddFilterEmit', vm.filterId);
      }
    },

    getFilterAttributes(filterId) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_FILTER_ATTRIBUTES, filterId).then((response) => {
        let data = [];
        let cacheParams = [];
        let multiAttributes = [];
        response.data.data.forEach(function (item) {
          vm.checkExistAttributeValueOptions(item);
          let temp = {
            id: item.id,
            filter_id: item.filter_id,
            attribute_id: item.attribute_id,
            attribute_name: item.attribute_name,
            attribute_code: item.attribute_code,
            data_type: item.data_type,
            attribute_values: item.attribute_values,
            external_attribute: item.external_attribute,
            code_external_attribute: item.code_external_attribute,
            attribute_options: item.attribute_options,
            show_position: item.show_position,
            is_auto_gen: vm.thenIsAutoGenerate(item),
            is_disabled: true,
            created_at: item.created_at,
            updated_at: item.updated_at
          };
          cacheParams.push(temp);
          if ((item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && (item.attribute_values.id == 6 || item.attribute_values.id == 7 || item.attribute_values.id != 6 && item.attribute_values.id != 7 && Array.isArray(item.attribute_values.attribute_value)) || (item.data_type == 6 || item.data_type == 7) && item.attribute_values.condition == '<>') {
            multiAttributes.push(item);
          } else {
            let dataType = vm.getDataType(item);
            let value = {
              attribute_id: item.attribute_values.attribute_id,
              data_type: dataType,
              attribute_value: item.attribute_values.attribute_value,
              condition: item.attribute_values.condition,
              dateRange: item.attribute_values.dateRange ? item.attribute_values.dateRange : "",
              errors: item.attribute_values.errors ? item.attribute_values.errors : "",
              id: item.attribute_values.id ? item.attribute_values.id : "",
              rangeDate: item.attribute_values.rangeDate ? item.attribute_values.rangeDate : "",
              type: item.attribute_values.type ? item.attribute_values.type : "",
              numberRange: item.attribute_values.numberRange ? item.attribute_values.numberRange : "",
              error: item.attribute_values.error ? item.attribute_values.error : "",
              filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
            };
            data.push(value);
          }
        })
        if (multiAttributes && multiAttributes.length > 0) {
          let multiParams = vm.getMultipleParams(multiAttributes);
          $.merge(data, multiParams);
        }

        // Set cache filter
        vm.attributeCustomerFull.forEach(function (item) {
          if (cacheParams && cacheParams.length > 0) {
            cacheParams.forEach(function (attribute_filter) {
              if (item.attribute_code == attribute_filter.attribute_code || item.attribute_code == attribute_filter.code_external_attribute) {
                attribute_filter.is_hidden = item.is_hidden;
              }
            });
          }
        })

        vm.cacheForFilterAttributes = JSON.stringify({
          data: cacheParams
        });
        vm.currentParams.filter = data;
        vm.getCountCustomerByFilter();
      }).catch(() => {});
    },

    checkExistAttributeValueOptions(attribute_filter) {
      // Kiểm tra tồn tại attribute option
      let dataType = [8, 9, 10, 11, 13, 14];
      if (attribute_filter && attribute_filter.data_type && dataType.includes(attribute_filter.data_type)) {

        let count = 0;
        let attributeOptionsIds = [];
        let attributeOptionsObj = {};

        if (attribute_filter.attribute_values.attribute_value && attribute_filter.attribute_values.attribute_value.length > 0) {
          attribute_filter.attribute_options.forEach(function (attribute_option) {
            attributeOptionsIds.push(attribute_option.id);
          });

          attribute_filter.attribute_values.attribute_value.forEach(function (attribute_option_id) {
            if (attributeOptionsIds.includes(attribute_option_id) || attribute_option_id === "empty_value") {
              if (!attributeOptionsObj[attribute_option_id]) {
                attributeOptionsObj[attribute_option_id] = attribute_option_id;
              }
            } else {
              count++;
            }
          })
        }

        if (count > 0) {
          attribute_filter.attribute_values.is_attribute_value_option_not_exist = true;
        } else {
          attribute_filter.attribute_values.is_attribute_value_option_not_exist = false;
        }

        if (Object.keys(attributeOptionsObj).length > 0) {
          attribute_filter.attribute_values.attribute_value = Object.values(attributeOptionsObj).map(function (value) {
            if (value === "empty_value") {
              return value;
            }
            return parseInt(value);
          });
        } else {
          attribute_filter.attribute_values.attribute_value = '';
        }
      }
    },

    getMultipleParams(items) {
      let multiArgs = [];
      items.forEach(function (item) {
        if (Array.isArray(item.attribute_values.attribute_value)) {
          let temp_data = item.attribute_values.attribute_value;
          if (item.attribute_values.id != 7 && item.attribute_values.id != 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_data[0] != 'undefined' && typeof temp_data[1] != 'undefined') {
            if (temp_data[0]) {
              let temp_args_min = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: temp_data[0],
                condition: ">=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              };
              multiArgs.push(temp_args_min);
            }
            if (temp_data[1]) {
              let temp_args_max = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: temp_data[1],
                condition: "<=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              }
              multiArgs.push(temp_args_max);
            }
          }
        } else if (item.attribute_values.rangeDate && Array.isArray(item.attribute_values.rangeDate) && item.attribute_values.rangeDate.length > 0) {
          let temp_range_date = item.attribute_values.rangeDate;
          if (item.attribute_values.id == 7 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_range_date[0] != 'undefined' && typeof temp_range_date[1] != 'undefined') {
            let dateMin = new Date(temp_range_date[0]);
            let min = dateMin.getDate() + '/' + (dateMin.getMonth() + 1) + '/' + dateMin.getFullYear();
            if (min != '1/1/1970') {
              let temp_args_min = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: min,
                condition: ">=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              };
              multiArgs.push(temp_args_min);
            }
            let dateMax = new Date(temp_range_date[1]);
            let max = dateMax.getDate() + '/' + (dateMax.getMonth() + 1) + '/' + dateMax.getFullYear();
            if (max != '1/1/1970') {
              let temp_args_max = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: max,
                condition: "<=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              }
              multiArgs.push(temp_args_max);
            }
          }
        } else if (item.attribute_values.dateRange && Object.keys(item.attribute_values.dateRange).length > 0) {
          if (item.attribute_values.id == 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at'))) {
            let temp_args_max = {
              attribute_id: item.attribute_values.attribute_id,
              data_type: "2",
              attribute_code: item.attribute_code,
              attribute_value: item.attribute_values.dateRange,
              condition: "=",
              filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
            }
            multiArgs.push(temp_args_max);
          }
        } else if (typeof item.attribute_values.attribute_value == 'string' || typeof item.attribute_values.attribute_value == 'object') {
          if (item.data_type == 6 || item.data_type == 7) {
            let temp_data = item.attribute_values.numberRange;
            if (typeof temp_data.min != 'undefined' && typeof temp_data.max != 'undefined') {
              if (temp_data.min) {
                let temp_args_min = {
                  attribute_id: item.attribute_values.attribute_id,
                  data_type: "1",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data.min,
                  condition: ">="
                };
                multiArgs.push(temp_args_min);
              }
              if (temp_data.max) {
                let temp_args_max = {
                  attribute_id: item.attribute_values.attribute_id,
                  data_type: "1",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data.max,
                  condition: "<="
                }
                multiArgs.push(temp_args_max);
              }
            }
          }
        } else {
          return;
        }
      })
      return multiArgs;
    },

    getDataType(item) {
      if (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) {
        return "2";
      } else if (item.attribute_code == "total_paid_amount") {
        return "3";
      } else {
        return "1";
      }
    },

    thenIsAutoGenerate(item) {
      if ($.inArray(item.attribute_code, ["sku", "full_name", "level", "customer_type", "total_price_when_buyer", "total_product_when_enduser", "total_product_when_buyer", "total_payment_amount", "user_id", "created_at", "updated_at"]) > -1) {
        return true;
      } else {
        return false;
      }
    },

    cancelFilter(status) {
      let vm = this;
      if (status) {
        vm.currentParams = {};
      }
    },
  }
}