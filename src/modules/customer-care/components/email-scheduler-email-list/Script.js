import draggable from "vuedraggable";
import moment from 'moment'

import EmailSchedulerEmailPopup from '../email-scheduler-email-popup/EmailSchedulerEmailPopup.vue'
import Confirmation from '@/components/confirmation-popup/Confirmation.vue'
import utils from '@/utils/utils'
import EmailApi from '@/services/customer-care'
import Loading from '@/components/loading/LoadingTable.vue'

const repeatType = {
  day: 1,
  week: 2,
  month: 3,
  year: 4
}

export default {
  components: {
    EmailSchedulerEmailPopup,
    Confirmation,
    draggable,
    Loading,
  },
  data() {
    return {
      numberOfTemplates: 0,
      emails: [],
      selectedIds: [],
      templateSchedulers: [],
      uid: `nt_confirmation${utils.uid()}`,
      isLoading: false,
      numberOfSentEmails: 0
    }
  },
  props: {
    isDetails: {
      type: Boolean,
      default: false
    },
    isEdit: {
      type: Boolean,
      default: false
    },
    data: {
      type: Object,
      default: null
    }
  },
  watch: {
    data: {
      handler() {
        this.predictSendTime()
      },
    }
  },
  methods: {
    getEmails(selectedIds = [], templates_schedules = {}, send_status = {}) {
      const ids = [],
        sendIds = []
      selectedIds.forEach(id => {
        ids.push(id)
        const index = ids.indexOf(id)
        if (index === -1) {
          sendIds.push(id)
        }
      })
      this.isLoading = true
      EmailApi.getEmailsByIds({
        email_ids: ids
      }).then(res => {
        this.isLoading = false
        const data = res.data && res.data.data
        const emails = []
        const emailIds = data.map(t => t.id)
        ids.forEach((id, i) => {
          const index = emailIds.indexOf(id)
          emails[i] = {
            ...data[index],
            selectedTimes: 1,
            key: templates_schedules[i],
            sendStatus: send_status[templates_schedules[i]]
          }
        })
        this.onUpdateEmailContents(emails)
      })
    },

    predictSendTime() {
      this.emails.forEach((email, index) => {
        email.sendDate = this.getSendDate(index)
        email.isDisabled = moment().isAfter(email.sendDate)
      })
    },

    onUpdateEmailContents(emails) {
      emails.forEach(email => {
        for (let index = 0; index < email.selectedTimes; index++) {
          const {
            id,
            code,
            title,
            key,
            sendStatus
          } = email
          this.selectedIds.push(id)
          if (this.isEdit) {
            this.templateSchedulers.push({
              id: key,
              templateId: id
            })
          }
          const sendDate = this.getSendDate(this.emails.length)
          this.emails.push({
            id,
            code,
            title,
            key,
            sendDate,
            sendStatus,
            isDisabled: moment().isAfter(sendDate)
          })
        }
      })
      this.getNumberOfTemplates()
      this.$emit('onUpdateEmailIds', this.selectedIds)
      if (this.isEdit) {
        this.$emit('onUpdateTemplateScheduler', this.templateSchedulers)
      }
      if (this.isEdit || this.isDetails) {
        const numberOfSentSuccess = this.emails.filter(t => t.sendStatus).map(t => t.sendStatus.success)
        const numberOfSentFail = this.emails.filter(t => t.sendStatus).map(t => t.sendStatus.fail)
        this.numberOfSentEmails = numberOfSentSuccess.reduce((a, b) => a + b, 0) + numberOfSentFail.reduce((a, b) => a + b, 0) || 0
      }
    },

    getSendDate(index) {
      const {
        date,
        time,
        type,
        value
      } = this.data
      if (!time) {
        return
      }
      const _time = utils.formatTime(time).split(':')
      const _date = moment(date)
      _date.set({
        hour: _time[0],
        minute: _time[1]
      })
      if (index) {
        switch (type) {
          case repeatType.day:
            return _date.add(value * index, 'd')

          case repeatType.week:
            return _date.add(value * index, 'w')

          case repeatType.month:
            return _date.add(value * index, 'M')

          case repeatType.year:
            return _date.add(value * index, 'y')
        }
      }
      return _date.format()
    },

    getNumberOfTemplates() {
      const codes = []
      this.emails.forEach(email => {
        const index = codes.indexOf(email.code)
        if (index === -1) {
          codes.push(email.code)
        }
      })
      this.numberOfTemplates = codes.length
    },

    removeEmail(index) {
      this.emails.splice(index, 1)
      this.selectedIds = this.emails.map(t => t.id)
      this.templateSchedulers.splice(index, 1)
      this.getNumberOfTemplates()
      this.predictSendTime()
      this.$emit('onUpdateEmailIds', this.selectedIds)
      if (this.isEdit) {
        this.$emit('onUpdateTemplateScheduler', this.templateSchedulers)
      }
    },

    onChange(event) {
      if (event.moved) {
        this.selectedIds = this.emails.map(t => t.id)
        this.templateSchedulers = this.emails.map(t => ({
          id: t.key,
          templateId: t.id
        }))
        this.predictSendTime()
        this.$emit('onUpdateEmailIds', this.selectedIds)
        if (this.isEdit) {
          this.$emit('onUpdateTemplateScheduler', this.templateSchedulers)
        }
      }
    },

    removeAll() {
      this.$refs[`span${this.uid}`].click()
    },

    reset() {
      this.emails = []
      this.selectedIds = []
      this.numberOfTemplates = 0
      this.$emit('onUpdateEmailIds', this.selectedIds)
      if (this.isEdit) {
        this.$emit('onUpdateTemplateScheduler', this.templateSchedulers)
      }
    }
  }
}