import TopStatsBox from '../stats-box/TopStatsBox.vue';

export default {
  props: {
    data: {
      required: true
    }
  },
  name: "TopStats",
  components: {
    TopStatsBox
  }
}
