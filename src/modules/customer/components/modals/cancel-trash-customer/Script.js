/* eslint-disable no-undef */
export default {
  props: {
    is_deleted_multiple: {
      type: Boolean,
      default: false
    },
    customer_name: {
      type: String,
      default: ''
    },
  },
  methods: {
    closeModal() {
      $('.dropdown-backdrop').removeClass('show')
    }
  }
}