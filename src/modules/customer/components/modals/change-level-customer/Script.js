/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import { CUSTOMER_UPDATE_LEVEL } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

const customerMessageAssigners = {
  custom: {
    'levelSelected': {
      required: 'Không được để trống trường này.',
    },
  }
};

export default {
  props: {
    customer_checked: {
      type: Array,
      required: true
    },
    pagination: {
      type: Object,
      required: true
    }
  },

  data() {
    return {
      customerLevel: '',
      totalSuccess: 0
    }
  },

  mounted() {
    let vm = this;
    vm.$validator.localize('en', customerMessageAssigners);
  },

  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["attributeCustomerFull"]),
    attributeLevelCustomer() {
      return helpers.getAttributeByAttributeCode(this.attributeCustomerFull, 'level');
    },
    attributeOptionsLevelCustomer() {
      let attribute = helpers.getAttributeByAttributeCode(this.attributeCustomerFull, 'level');
      let options = attribute.attribute_options;
      options = options.sort(function (a, b) {
        return a.sort_order - b.sort_order;
      });
      return options;
    }
  },

  methods: {
    changeLevelCustomer(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {

          let type = '';
          let customers_id = [];

          if (vm.customer_checked.length > 0) {
            type = 1;
            customers_id = vm.customer_checked.map(customer => {
              return customer.id
            })
          } else {
            type = 2;
          }

          let params = {
            team_id: parseInt(this.$route.params.team_id),
            type: type,
            listCustomer: customers_id,
            level: vm.customerLevel

          };

          vm.$store.dispatch(CUSTOMER_UPDATE_LEVEL, params).then((response) => {

            if (response.status === 422) {
              let errors = response.data.errors;
              if (errors.level_not_exist) {
                $("#changeDataLevel").modal('show');
              }

              vm.$snotify.error('Thay đổi cấp độ khách hàng thất bại.', TOAST_ERROR);
              return;
            }

            if (response.data.data.status) {
              vm.$snotify.success('Thay đổi cấp độ khách hàng thành công.', TOAST_SUCCESS);
              $('#changeLevelForCustomer').modal('hide');
              $('#messageChangeLevelForCustomerSuccess').modal('show');
              vm.totalSuccess = response.data.data.total;
              vm.$emit('change_level_customer_success', true);
              vm.resetForm();
            } else {
              vm.$snotify.error('Thay đổi cấp độ khách hàng thất bại.', TOAST_ERROR);
            }
          }).catch(error => {
            vm.$snotify.error('Thay đổi cấp độ khách hàng thất bại.', TOAST_ERROR);
            console.log(error);
          });
        }
      })
    },
    onChangeInput($event) {
      let vm = this;
      vm.customerLevel = $event
    },
    cancelUpdate() {
      let vm = this;
      vm.resetForm();
    },
    resetForm() {
      let vm = this;
      vm.customerLevel = '';
      vm.$validator.reset();
    },
    reloadPage() {
      return window.location.reload();
    }
  },
}
