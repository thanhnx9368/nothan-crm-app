/* eslint-disable no-undef */
import helpers from "@/utils/utils";
import {
  mapGetters
} from "vuex";
import {
  CUSTOMER_UPDATE_ASSIGNERS
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";

const customerMessageAssigners = {
  custom: {
    'usersSelected': {
      required: 'Không được để trống trường này.',
    },
  }
};

export default {
  props: {
    customer_checked: {
      type: Array,
      required: true
    },
    pagination: {
      type: Object,
      required: true
    }
  },

  data() {
    return {
      usersSelected: [],
      customerSelectedHTML: "",
      totalSuccess: 0,
      isLoader: false
    }
  },

  watch: {},

  mounted() {
    let vm = this;
    vm.$validator.localize('en', customerMessageAssigners);
  },

  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["listUsers", "currentTeam", "currentUser"]),
  },

  methods: {
    updateAssigner(scope) {
      let vm = this;
      let assigners = [];
      let newAssignerSendMail = {};
      newAssignerSendMail['data'] = [];

      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          vm.usersSelected.forEach(function (userSelectedId) {
            vm.listUsers.find(function (user) {
              if (user.id == userSelectedId) {
                assigners.push(user.id);

                // Convert data send mail
                newAssignerSendMail['data'].push({
                  'email': user.email,
                  'user_name': user.full_name,
                  'team_name': vm.currentTeam.name,
                  'managment_name': vm.currentUser.full_name,
                  'customer_count': vm.customer_checked && vm.customer_checked.length > 0 ? vm.customer_checked.length : vm.pagination.total_item
                });
              }
            });
          });

          let params = {
            team_id: vm.currentTeamId,
            user_ids: assigners,
            customer_ids: vm.customer_checked.map(customer => customer.id),
            data_mail: newAssignerSendMail['data'],
            type_mail: 8
          }

          vm.$store.dispatch(CUSTOMER_UPDATE_ASSIGNERS, params).then((response) => {
            if (response.data.data.status) {
              vm.$snotify.success('Thêm người phụ trách thành công.', TOAST_SUCCESS);
              vm.$emit('assigners_success', true);
              $('#addMultipleChargeUser').modal('hide');
              $('#messageAddMultipleChargeUserSuccess').modal('show');
              vm.resetForm();

              vm.totalSuccess = response.data.data.total;
            } else {
              vm.$snotify.error('Thêm người phụ trách thất bại.', TOAST_ERROR);
            }
            vm.isLoader = false;
          }).catch(error => {
            vm.isLoader = false;
            vm.$snotify.error('Thêm người phụ trách thất bại.', TOAST_ERROR);
            console.log(error);
          });
        }
      })
    },
    cancelUpdate() {
      let vm = this;
      vm.resetForm();
    },
    resetForm() {
      let vm = this;
      vm.usersSelected = [];
      vm.$validator.reset();
    },
  },
}