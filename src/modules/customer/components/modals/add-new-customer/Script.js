/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex"

import helpers from "@/utils/utils"
import {
  CUSTOMER_CREATE,
  CUSTOMER_ADD_NEW_CHECK_EXIST
} from "@/types/actions.type"
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE
} from "@/types/const"
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config"

const doneTypingInterval = 500

export default {
  data() {
    return {
      attributeCodes: ['full_name', 'phone', 'email', 'address', 'birthday_set_to_date'],
      customer: {
        is_active: 1,
        assigners: [],
        attributes: [],
        team_id: this.currentTeamId ? this.currentTeamId : ''
      },
      customerId: '',
      emailIsExists: false,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isEmail: IS_EMAIL,
      isLoader: false,
      isLoaderCheckExists: false,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isNumber: IS_NUMBER,
      isPhone: IS_PHONE,
      isPrice: IS_PRICE,
      isRadio: IS_RADIO,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isSuccess: false,
      isTag: IS_TAG,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
      messageErrorServer: '',
      phoneIsExists: false,
      server_errors: '',
    }
  },
  computed: {
    helpers() {
      return helpers
    },
    ...mapGetters(["currentUser", "currentTeam", "attributeAddNew"]),
    listAttributeCodesByAllAttributes() {
      this.customer.attributes = this.attributeAddNew
      const hiddenAtts = ['total_price_when_buyer', 'total_product_when_buyer', 'total_product_when_enduser', 'total_payment_amount', 'guid']
      return this.attributeAddNew.filter(t => hiddenAtts.indexOf(t.attribute_code) === -1)

    }
  },

  methods: {
    createNewCustomer(scope) {
      this.$validator.validateAll(scope).then(() => {
        if (!this.errors.any()) {
          this.isLoader = true
          this.customer.team_id = this.currentTeamId
          this.customer.user_id = this.currentUser.id
          this.customer.attributes.forEach( (attribute) => {
            if (attribute.attribute_code == 'phone') {
              attribute.attribute_value = attribute.attribute_value ? attribute.attribute_value.replace(/ /g, "") : ''
            }
            if (attribute.attribute_code == "total_price_when_buyer" || attribute.attribute_code == "total_product_when_buyer" || attribute.attribute_code == "total_product_when_enduser" || attribute.attribute_code == "total_payment_amount") {
              attribute.attribute_value = "0"
            }

            if (attribute.data_type.id == this.isDate) {
              attribute.attribute_value = attribute.attribute_value ? helpers.formatDateTime(attribute.attribute_value) : ''
            }
            if (attribute.data_type.id == this.isDateTime) {
              attribute.attribute_value = attribute.attribute_value ? helpers.formatDateTime(attribute.attribute_value, true) : ''
            }
          })

          this.$store.dispatch(CUSTOMER_CREATE, this.customer).then(response => {
            if (response.data.status_code && response.data.status_code == 422) {
              this.server_errors = response.data.errors
              this.$snotify.error('Thêm mới khách hàng thất bại.', TOAST_ERROR)
            } else if (response.data.status_code && response.data.status_code == 405) {
              this.$bus.$emit('checkAuthorAddCustomer', ' ')
            } else {
              if (response.data.data && response.data.data.status) {
                this.customerId = response.data.data.id
                this.isSuccess = true
                this.$emit("add_new_success", true)
                this.$snotify.success('Thêm mới khách hàng thành công.', TOAST_SUCCESS)
                $(".s--modal .modal-body").animate({
                  scrollTop: 0
                }, 300)
                this.resetCustomer()
              }
            }
            this.isLoader = false
          }).catch(() => {
            // if ( error.response.status_code && error.response.status_code === 405 ) {
            //     this.$bus.$emit('checkAuthorAddCustomer', error.response.data.errors.message)
            //     return
            // }
            this.$snotify.error('Thêm mới khách hàng thất bại.', TOAST_ERROR)
            this.messageErrorServer = 'Lỗi, vui lòng thử lại sau.'

            $(".s--modal .modal-body").animate({
              scrollTop: 0
            }, 300)
            this.isLoader = false
          })
        }
      })
    },

    resetCustomer() {
      this.customer.attributes.forEach( (attribute) => {
        attribute.attribute_value = ""
      })
      this.$validator.reset()
      this.server_errors = ''
    },

    reset() {
      this.isSuccess = false
      this.resetCustomer()
      this.emailIsExists = false
      this.phoneIsExists = false
    },

    checkPhoneExists(id, value) {
      value = value ? value.replace(/ /g, "") : ""
      if (value.length >= 10) {
        this.phoneIsExists = false
        clearTimeout(this.typingTimer)
        this.typingTimer = setTimeout( () => {
          this.isLoaderCheckExists = true
          let data = {
            attribute_id: id,
            attribute_value: value.replace(/ /g, ""),
            team_id: this.currentTeamId
          }
          this.$store.dispatch(CUSTOMER_ADD_NEW_CHECK_EXIST, data).then(response => {
            if (response.data.meta) {
              if (response.data.meta.is_exists) {
                this.phoneIsExists = true
              }
              this.isLoaderCheckExists = false
            }
          }).catch(error => {
            this.isLoaderCheckExists = false
            console.log(error)
          })
        }, doneTypingInterval)
      }
    },

    checkEmailExists(id, value) {
      if (this.helpers.checkIsEmail(value)) {
        this.emailIsExists = false
        clearTimeout(this.typingTimer)
        this.typingTimer = setTimeout( () => {
          this.isLoaderCheckExists = true
          let data = {
            attribute_id: id,
            attribute_value: value,
            team_id: this.currentTeamId
          }
          this.$store.dispatch(CUSTOMER_ADD_NEW_CHECK_EXIST, data).then(response => {
            if (response.data.meta) {
              if (response.data.meta.is_exists) {
                this.emailIsExists = true
              }
              this.isLoaderCheckExists = false
            }
          }).catch(error => {
            this.isLoaderCheckExists = false
            console.log(error)
          })
        }, doneTypingInterval)
      }
    }
  },
}