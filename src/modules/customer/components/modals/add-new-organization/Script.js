/* eslint-disable no-undef */
import helpers from "@/utils/utils";
import { mapGetters } from "vuex";
import {
  CUSTOMER_CREATE,
  CUSTOMER_ADD_NEW_CHECK_EXIST
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR, TOAST_WARNING } from "@/types/config";

const customMessagesCustomerAddOrganizationModal = {
  custom: {
    'contact_name0': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'contact_email0': {
      regex: 'Định dạng email không đúng.',
      max: "Giới hạn 250 ký tự."
    },
    'contact_phone0': {
      regex: 'Định dạng số điện thoại không đúng.'
    },
    'contact_name1': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'contact_email1': {
      regex: 'Định dạng email không đúng.',
      max: "Giới hạn 250 ký tự."
    },
    'contact_phone1': {
      regex: 'Định dạng số điện thoại không đúng.'
    },
  }
};

const doneTypingInterval = 500;

export default {
  props: {
    current_add_customer_type: {
      type: Object,
      required: true
    }
  },
  data() {
    return {
      isSuccess: false,
      attributeCodes: ['full_name', 'phone', 'email', 'address'],
      customer: {
        is_active: 1,
        customer_type_id: 0,
        assigners: [1],
        attributes: [],
        contacts: [
          {
            full_name: '',
            phone: '',
            email: ''
          },
          {
            full_name: '',
            phone: '',
            email: ''
          }
        ],
      },
      emailIsExists: false,
      phoneIsExists: false,
      isLoader: false,
      customerId: '',
      isLoaderCheckExists: false,
      server_errors: '',
      messageErrorServer: ''
    }
  },

  watch: {
  },

  mounted() {
    let vm = this;
    vm.$validator.localize('en', customMessagesCustomerAddOrganizationModal);
  },

  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["currentUser", "currentTeam", "attributeAddNew"]),
    listAttributeCodesByAllAttributes() {
      let vm = this;
      let result = helpers.getAttributeCodesByAllAttributes(vm.attributeAddNew, vm.attributeCodes);
      vm.customer.attributes = result;
      return result;
    },
    isDisabled() {
      let vm = this;
      let count = 0;
      let attributes = vm.listAttributeCodesByAllAttributes;
      if (attributes && attributes.length > 0) {
        attributes.forEach(function (item) {
          if (item.attribute_code == 'full_name') {
            if (!item.attribute_value || item.attribute_value == '' || item.attribute_value == undefined) {
              count++;
            }
          }
        });
      }
      if (count) {
        return true;
      } else {
        return false;
      }
    }
  },

  methods: {
    createNewOrganizationCustomer(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          vm.customer.team_id = vm.currentTeamId;
          vm.customer.user_id = vm.currentUser.id;
          vm.customer.customer_type_id = vm.current_add_customer_type.id;
          vm.customer.attributes.forEach(function (attribute) {
            if (attribute.attribute_code == 'phone') {
              attribute.attribute_value = attribute.attribute_value ? attribute.attribute_value.replace(/ /g, "") : '';
            }
          });

          if (vm.customer.contacts && vm.customer.contacts.length > 0) {
            vm.customer.contacts.forEach(function (contact) {
              contact.phone = contact.phone ? contact.phone.replace(/ /g, "") : '';
            });
          }
          vm.$store.dispatch(CUSTOMER_CREATE, vm.customer).then(response => {
            if (response.data.status_code && response.data.status_code == 405) {
              vm.$snotify.warning(`Tổ chức của bạn đã lưu hết số lượng khách hàng của tính năng. Vui lòng liên hệ để nâng cấp tính năng.`, TOAST_WARNING);
            } else if (response.data.status_code && response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              vm.$snotify.error('Thêm mới khách hàng thất bại.', TOAST_ERROR);
            } else {
              if (response.data.data && response.data.data.status) {
                vm.customerId = response.data.data.id;
                vm.isSuccess = true;
                vm.$emit("add_new_success", true);
                vm.$snotify.success('Thêm mới khách hàng thành công.', TOAST_SUCCESS);
                $(".s--modal .modal-body").animate({ scrollTop: 0 }, 300);
                vm.resetCustomer();
              }
            }
            vm.isLoader = false;
          }).catch(() => {
            vm.isLoader = false;
            vm.messageErrorServer = 'Lỗi, vui lòng thử lại sau.';
            $(".s--modal .modal-body").animate({ scrollTop: 0 }, 300);
            vm.$snotify.error('Thêm mới khách hàng thất bại.', TOAST_ERROR);
          });
        }
      });
    },

    resetCustomer() {
      let vm = this;
      vm.customer.attributes.forEach(function (attribute) {
        attribute.attribute_value = "";
      });
      vm.customer.contacts.forEach(function (contact) {
        contact.full_name = "";
        contact.phone = "";
        contact.email = "";
      });
      vm.$validator.reset();
      vm.server_errors = '';
    },

    reset() {
      let vm = this;
      vm.isSuccess = false;
      vm.resetCustomer();
      vm.emailIsExists = false;
      vm.phoneIsExists = false;
      vm.$emit("cancel_add_new", true);
      $('input').val('');
    },

    checkPhoneExists(id, value) {
      let vm = this;
      value = value ? value.replace(/ /g, "") : "";
      if (value.length >= 10) {
        vm.phoneIsExists = false;
        clearTimeout(vm.typingTimer);
        vm.typingTimer = setTimeout(function () {
          vm.isLoaderCheckExists = true;
          let data = {
            attribute_id: id,
            attribute_value: value.replace(/ /g, "")
          }

          vm.$store.dispatch(CUSTOMER_ADD_NEW_CHECK_EXIST, data).then(response => {
            if (response.data.meta) {
              if (response.data.meta.is_exists) {
                vm.phoneIsExists = true;
              }
              vm.isLoaderCheckExists = false;
            }
          }).catch(error => {
            vm.isLoaderCheckExists = false;
            console.log(error);
          });
        }, doneTypingInterval);
      }
    },

    checkEmailExists(id, value) {
      let vm = this;
      if (vm.helpers.checkIsEmail(value)) {
        vm.emailIsExists = false;
        clearTimeout(vm.typingTimer);
        vm.typingTimer = setTimeout(function () {
          vm.isLoaderCheckExists = true;
          let data = {
            attribute_id: id,
            attribute_value: value
          }
          vm.$store.dispatch(CUSTOMER_ADD_NEW_CHECK_EXIST, data).then(response => {
            if (response.data.meta) {
              if (response.data.meta.is_exists) {
                vm.emailIsExists = true;
              }
              vm.isLoaderCheckExists = false;
            }
          }).catch(error => {
            vm.isLoaderCheckExists = false;
            console.log(error);
          });
        }, doneTypingInterval);
      }
    }
  },
}
