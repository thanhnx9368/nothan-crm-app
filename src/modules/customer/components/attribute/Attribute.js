import helpers from "@/utils/utils";
import { mapGetters } from "vuex";
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE
} from "@/types/const";

export default {
  inject: {
    $validator: '$validator'
  },
  props: {
    attributes: {
      type: Array,
      default: []
    },
    server_errors: {
      type: [Array, Object, String],
      default: []
    },
    scope: {
      type: String,
      default: 'scope'
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["listUsers"]),
  },
  data() {
    return {
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
    }
  },
  methods: {
    changeDataInputType(attribute_code) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute_code]) {
          vm.server_errors[attribute_code] = '';
        }
      }, 500);
    }
  }
}
