import helpers from "@/utils/utils";
import { PERMISSION_SHOW_CUSTOMER } from "@/types/const";
import { mapGetters } from "vuex";

export default {
  props: {
    contacts: {
      type: Array,
      required: true
    },
    search: {
      type: String,
      required: false,
      default: ""
    },
  },
  data() {
    return {
      PERMISSION_SHOW_CUSTOMER: PERMISSION_SHOW_CUSTOMER
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["currentTeam", "userCheckAddOn", "userCurrentPermissions"]),
  },
  methods: {
    getCustomerLinkDetail(contact) {
      let customer_id = contact.customer_id;
      let team_id = parseInt(this.$route.params.team_id);
      if (customer_id) {
        window.open('/team/' + team_id + '/customers/' + customer_id, '_blank');
      }
    },
  }
}

