/* eslint-disable no-undef */
import {
  mapGetters,
  mapState
} from "vuex"

import helpers from '@/utils/utils'
import exportExcel from '@/utils/exportExcel'
import Loading from '@/components/loading/loading-system/Loading'

import {
  CUSTOMER_GET_ALL,
  CUSTOMER_GET_ATTRIBUTES,
  CUSTOMER_GET_FILTERS,
  CUSTOMER_GET_FILTER_ATTRIBUTES,
  CUSTOMER_GET_DATA_FOR_EXCEL,
  CUSTOMER_GET_ALL_ASSIGNER_IN_TEAM,
  CUSTOMER_CHECK_HAS_DEAL_BEFORE_DELETE,
  CHANGE_TABLE_ATTRIBUTE_CUSTOMER,
} from "@/types/actions.type";
import CustomColumn from "@/components/column/CustomColumns"
import UserObjectLoading from "@/components/loading/UserObject";
import Filter from "@/components/filter/Filter.vue";
import Pagination from "@/components/pagination/Pagination.vue";
import NoteDetailsPopup from '../../components/modals/note-details-popup/NoteDetailsPopup.vue';
import EmailSend from '../../components/email-send/EmailSend.vue';
import UpdateAttributeCustomers from '../../components/update-attribute-customers/UpdateAttributeCustomers.vue'

import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE,
} from "@/types/const";

import {
  PERMISSION_UPDATE_PERSON_IN_CHARGE,
  PERMISSION_DELETE_PERSON_IN_CHARGE,
  PERMISSION_EXPORT_CUSTOMER,
  PERMISSION_SHOW_CUSTOMER,
  PERMISSION_MOVE_CUSTOMER_TO_TRASH,
  PERMISSION_UPDATE_LEVEL_MULTIPLE,
  PERMISSION_CUSTOMER_SEND_MULTIPLE_EMAIL
} from "@/const/permission";

// import AddNewIndividualCustomer from "./modal/AddNewIndividualCustomer.vue";
// import AddNewOrganizationCustomer from "./modal/AddNewOrganizationCustomer.vue";
import ShowContactsByCustomerId from "../../components/contact/ShowContactsByCustomerId.vue";
import AddChargeUsersForCustomer from "../../components/modals/add-charge-user/AddChargeUsersForCustomer.vue";
import AddNewCustomer from "../../components/modals/add-new-customer/AddNewCustomer.vue";
import CancelTrashCustomer from "../../components/modals/cancel-trash-customer/CancelTrashCustomer.vue";
import ChangeLevelForCustomer from "../../components/modals/change-level-customer/ChangeLevelForCustomer.vue";
import DeleteChargeUsersForCustomer from "../../components/modals/detele-charge-user/DeleteChargeUsersForCustomer.vue";
import ImportCustomer from "../../components/modals/import-customer/ImportCustomer.vue";
import TrashCustomer from "../../components/modals/trash-customer/TrashCustomer.vue";

import customerAPI from "@/services/customer"

const SEARCH_TIMEOUT_DEFAULT = 10000;

export default {
  components: {
    Loading,
    pagination: Pagination,
    ImportCustomer: ImportCustomer,
    // AddNewIndividualCustomer: AddNewIndividualCustomer,
    // AddNewOrganizationCustomer: AddNewOrganizationCustomer,
    AddNewCustomer: AddNewCustomer,
    AddChargeUsersForCustomer: AddChargeUsersForCustomer,
    DeleteChargeUsersForCustomer: DeleteChargeUsersForCustomer,
    ChangeLevelForCustomer: ChangeLevelForCustomer,
    filterCustomer: Filter,
    "user-object-loading": UserObjectLoading,
    "show-all-contacts": ShowContactsByCustomerId,
    TrashCustomer: TrashCustomer,
    CancelTrashCustomer: CancelTrashCustomer,
    CustomColumn: CustomColumn,
    NoteDetailsPopup,
    EmailSend,
    UpdateAttributeCustomers
  },
  mounted() {
    this.getAttributes()
    this.loadAsync()
    this.getFilters()
    this.reloadTooltip()

    if (this.isLoaderDefault) {
      this.getConfigFormattingDisplay()
    }
  },
  watch: {
    isLoader: {
      handler(after) {
        if (after === true) {
          this.checkShowHideScrollBtn();
        }
      },
      deep: true
    },
    isLoaderDefault: {
      handler(after) {
        if (after === true) {
          this.getConfigFormattingDisplay();
        }
      },
      deep: true
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    isLoaderDefault() {
      let vm = this;
      if (vm.currentUser && vm.currentUser.id && vm.currentTeamId) {
        return true;
      } else {
        return false;
      }
    },
    isLoader() {
      let vm = this;
      if (vm.dataReady === true && vm.attributeReady === true) {
        return true;
      } else {
        return false;
      }
    },
    ...mapGetters(["attributeCustomerUsingReports", "listCustomers", "attributeCustomerFull", "currentUser", "currentTeam", "listUsers", "getDataExportExcel", "getCustomerFilters", "filterAttributesCustomer", "attributeFullForFilter", "userCheckAddOn", "userCurrentPermissions"]),
    ...mapState({
      allAttribute: state => state.customer.attributes,
    })
  },

  data() {
    return {
      isLoadingS: false,
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,

      PERMISSION_UPDATE_PERSON_IN_CHARGE: PERMISSION_UPDATE_PERSON_IN_CHARGE,
      PERMISSION_DELETE_PERSON_IN_CHARGE: PERMISSION_DELETE_PERSON_IN_CHARGE,
      PERMISSION_EXPORT_CUSTOMER: PERMISSION_EXPORT_CUSTOMER,
      PERMISSION_SHOW_CUSTOMER: PERMISSION_SHOW_CUSTOMER,
      PERMISSION_MOVE_CUSTOMER_TO_TRASH: PERMISSION_MOVE_CUSTOMER_TO_TRASH,
      PERMISSION_UPDATE_LEVEL_MULTIPLE: PERMISSION_UPDATE_LEVEL_MULTIPLE,
      PERMISSION_CUSTOMER_SEND_MULTIPLE_EMAIL: PERMISSION_CUSTOMER_SEND_MULTIPLE_EMAIL,
      dataReady: false,
      attributeReady: false,
      customerChecked: [],
      assignersCustomerChecked: [],
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0,
        total_item_without_deleted: 0
      },
      currentParams: {},
      customer_types: [],
      beforeSearchCache: null,
      customerSelected: null,
      sortAttributeValue: 'DESC',
      currentSortAttribute: null,
      isSearch: false,
      loadSearch: false,
      isLoaderListCustomer: false,
      currentAddCustomerType: null,
      loadDataCustomer: false,
      showFilter: false,
      isAllData: true,
      customerCheckedPerPage: [],
      customerCheckAllPerPage: [],
      oldCurrentPage: 0,
      listAssignerInTeam: [],
      cacheForFilterAttributes: "",
      searchText: "",

      selectedIds: [],
      totalDeleted: 0,
      isDeletedMultiple: false,
      customerName: '',
      isLoaderDelete: false,
      dataFixPositionCustomColumn: [
        'sku',
        'full_name',
      ],
      attributes_config_by_user: [],
      customColumn: {
        buttonName: 'Tùy chỉnh bảng',
        columnName: 'Tùy chỉnh bảng:'
      },
      latestNotePopup: 'latest_note_popup',
      latestNoteContent: '',
      currentPerPage: 10,
      perPageDefined: [10, 50, 100],
      configFormattingDisplay: [],
      configFormattingDisplayCurrent: {},
      isToggleDropdownListActions: false
    }
  },
  methods: {
    getConfigFormattingDisplay() {
      let teamId = this.currentTeamId ? this.currentTeamId : '';
      let userId = this.currentUser.id ? this.currentUser.id : '';

      this.configFormattingDisplay = [{
          id: 1,
          name: 'Mặc định',
          team_id: teamId,
          user_id: userId
        },
        {
          id: 2,
          name: 'Cố định bảng',
          team_id: teamId,
          user_id: userId
        }
      ];

      let nameFormattingDisplay = "configListCustomerFormattingDisplayCurrent_" + userId + "_" + teamId;
      let configFormattingDisplayCurrent = localStorage.getItem(nameFormattingDisplay) ? JSON.parse(localStorage.getItem(nameFormattingDisplay)) : {}
      if (Object.keys(configFormattingDisplayCurrent).length && configFormattingDisplayCurrent.team_id === this.currentTeamId && configFormattingDisplayCurrent.user_id === this.currentUser.id) {
        this.configFormattingDisplayCurrent = configFormattingDisplayCurrent;
      } else {
        this.configFormattingDisplayCurrent = this.configFormattingDisplay && this.configFormattingDisplay.length ? this.configFormattingDisplay[0] : ''
      }
    },

    changeFormattingDisplay() {
      let nameFormattingDisplay = "configListCustomerFormattingDisplayCurrent_" + this.currentUser.id + "_" + this.currentTeamId;
      localStorage.setItem(nameFormattingDisplay, JSON.stringify(this.configFormattingDisplayCurrent));
      this.checkShowHideScrollBtn();
    },

    toggleCustomerAction(elClass) {
      $(elClass).addClass('nt-table-div-list-fix-columns-reset')
      $('nt-table-div-row').addClass('nt-table-div-row-reset')
      $('.dropdown-backdrop').addClass('show')
      document.getElementById('nt_table_div_scroll').scrollLeft = 0
    },

    hideCustomerAction() {
      $('.nt-table-div-list-fix-columns').removeClass('nt-table-div-list-fix-columns-reset')
      $('nt-table-div-row').removeClass('nt-table-div-row-reset')
      $('.dropdown-backdrop').removeClass('show')
    },

    toggleCustomerContact(elId) {
      $(elId).toggleClass('is-active');
      let clientWidth = $('.nt-table-div-scroll').width();
      let clientWidthContact = Math.round(clientWidth * 0.75);
      let contactLeft = Math.round((clientWidth - clientWidthContact) / 2);
      if (window.innerWidth <= 768) {
        $(elId + ' .nt-table-div-row').css({
          'width': '75%',
          'margin': '10px 20px'
        });
      } else {
        $(elId + ' .nt-table-div-row').css({
          'width': clientWidthContact,
          'margin': '10px 0',
          'position': 'sticky',
          'left': contactLeft + 'px'
        });
      }

    },

    checkShowHideScrollBtn() {
      setTimeout(() => {
        $('.nt-table-div-scroll-btn .vertical').removeClass('is-active');
        $('.nt-table-div-scroll-btn .horizontal').removeClass('is-active');

        $('.nt-table-div-scroll-btn .horizontal').addClass('without-vertical');
        $('.nt-table-div').removeClass('horizontal');
        $('.nt-table-div').removeClass('vertical');

        if ($('.nt-table-div').width() > $('#nt_table_div_scroll').width()) {
          $('.nt-table-div-scroll-btn .horizontal').addClass('is-active');
          $('.nt-table-div').addClass('horizontal');
        }

        if ($('.nt-table-div').height() > $('#nt_table_div_scroll').height()) {
          $('.nt-table-div-scroll-btn .vertical').addClass('is-active');
          $('.nt-table-div-scroll-btn .horizontal').removeClass('without-vertical');
          $('.nt-table-div').addClass('vertical');

        }
      }, 100);
    },

    // getUrlParams() {
    //     let vm = this;
    //     let oldParams = router.history.current.query;
    //     if (!vm.loadDataCustomer && oldParams && Object.keys(oldParams).length > 0) {
    //         if (oldParams["page"]) {
    //             vm.pagination.current_page = parseInt(oldParams["page"]);
    //             vm.currentParams.page = parseInt(oldParams["page"]);
    //         }filterData
    //         if (oldParams["search"]) {
    //             vm.currentParams.search = oldParams["search"];
    //             vm.searchText = oldParams["search"];
    //         }
    //     }
    // },
    async loadAsync() {
      await this.getConfigListCustomer()
      await this.getAll()
    },

    setCurrentParams() {
      let vm = this;

      // Per page
      let perPage = this.$route.query.per_page ? this.$route.query.per_page : vm.currentParams.per_page;
      perPage = !perPage || isNaN(parseInt(perPage)) ? 10 : parseInt(perPage);
      perPage = vm.perPageDefined.indexOf(perPage) === -1 ? vm.currentPerPage : perPage;
      vm.currentPerPage = perPage;
      vm.currentParams.per_page = perPage;

      // Current page
      let currentPage = this.$route.query.page ? this.$route.query.page : vm.currentParams.page;
      currentPage = !currentPage || isNaN(parseInt(currentPage)) ? 1 : parseInt(currentPage);
      vm.currentParams.page = currentPage;
      vm.pagination.current_page = currentPage;

      //Search
      let search = this.$route.query.search ? this.$route.query.search : vm.currentParams.search;
      search = search ? search.trim() : '';
      vm.currentParams.search = search;
      vm.searchText = search;
      vm.loadSearch = search ? true : false;
    },

    async getAll(type = "all") {
      let vm = this;

      vm.setCurrentParams();

      vm.isLoaderListCustomer = true;
      await vm.$store.dispatch(CUSTOMER_GET_ALL, vm.currentParams).then(response => {
        if (type != "pagination") {
          vm.customerChecked = [];
        }
        if (typeof vm.customerCheckAllPerPage[response.data.meta.current_page] == "undefined") {
          vm.customerCheckAllPerPage[response.data.meta.current_page] = false;
        }
        if (typeof vm.customerCheckedPerPage[response.data.meta.current_page] == "undefined") {
          vm.customerCheckedPerPage[response.data.meta.current_page] = [];
        }
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.total_item_without_deleted = response.data.meta.total_without_deleted;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.oldCurrentPage = response.data.meta.current_page;
        let items = 0;
        if (response.data.meta.current_page < response.data.meta.last_page) {
          items = response.data.meta.per_page;
        } else {
          items = response.data.meta.total - (response.data.meta.per_page * (response.data.meta.last_page - 1));
        }
        vm.pagination.items_in_page = parseInt(items);

        vm.beforeSearchCache = JSON.stringify(response.data.data);

        if (vm.loadSearch) {
          vm.isSearch = true;
        } else {
          vm.isSearch = false;
        }

        // highlight keySearch
        let textSearch = helpers.convertLanguages(vm.searchText);

        if (vm.listCustomers && vm.listCustomers.length > 0) {

          vm.listCustomers.forEach(function (customer) {

            if (customer.contacts && customer.contacts.length > 0) {

              customer.contacts.forEach(function (contact) {

                if (contact.email) {

                  if (vm.loadSearch) {
                    let valConverted = helpers.convertLanguages(contact.email);

                    if (valConverted.indexOf(textSearch) >= 0) {

                      let startIndexSearchFromValueConverted = valConverted.indexOf(textSearch);
                      let searchTextLength = textSearch.length;
                      let positionTextSearchInValue = contact.email.substr(startIndexSearchFromValueConverted, searchTextLength);

                      contact.email = contact.email.replace(positionTextSearchInValue, '<span class="hightlight">' + positionTextSearchInValue + '</span>');
                    }
                  }
                }

                if (contact.phone) {
                  contact.phone = contact.phone.replace(/(\d{4})(\d{3})(\d{3,5})/, '$1 $2 $3');

                  if (contact.phone.indexOf(vm.searchText) >= 0) {
                    contact.phone = contact.phone.replace(vm.searchText, '<span class="hightlight">' + vm.searchText + '</span>');
                  }
                }
              });
            }
          });
        }

        if (vm.listCustomers && vm.listCustomers.length > 0 && vm.allAttribute && vm.allAttribute.length > 0) {
          vm.highlightCustomerAfterSearch();
        }

        vm.checkShowHideScrollBtn();
      }).finally(() => {
        this.dataReady = true
        this.loadDataCustomer = false
        this.isLoaderListCustomer = false
      })
    },

    async getAttributes() {
      let vm = this;
      await vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES)
    },

    async getConfigListCustomer() {
      await customerAPI.getConfigListCustomer().then((res) => {
        this.attributes_config_by_user = res.data.data.attributes ? res.data.data.attributes : []
        this.attributes_config_by_user = this.attributes_config_by_user.filter(attribute => attribute.attribute_code !== 'guid')
        this.attributes_config_by_user.sort((a, b) => {
          return a.sort_order - b.sort_order
        })
        this.currentParams.per_page = res.data.data.per_page ? res.data.data.per_page : this.currentPerPage
        this.attributeReady = true
      })
    },

    checkedCustomer(customer) {
      let vm = this;
      if (!vm.checkExistValueInChecked(customer)) {
        if (vm.customerCheckedPerPage[vm.oldCurrentPage]) {
          vm.customerCheckedPerPage[vm.oldCurrentPage].push(customer);
        }
      } else {
        let newChecked = [];
        vm.customerCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
          if (item.id !== customer.id) {
            newChecked.push(item);
          }
        });
        vm.customerCheckedPerPage[vm.oldCurrentPage] = newChecked;
      }

      let itemCheckedInPage = vm.customerCheckedPerPage[vm.oldCurrentPage];
      if (itemCheckedInPage && vm.pagination.items_in_page == itemCheckedInPage.length) {
        vm.customerCheckAllPerPage[vm.oldCurrentPage] = true;
      } else {
        vm.customerCheckAllPerPage[vm.oldCurrentPage] = false;
      }
    },

    checkExistValueInChecked(customer) {
      let vm = this;
      if (vm.customerCheckedPerPage[vm.oldCurrentPage] && vm.customerCheckedPerPage[vm.oldCurrentPage].length > 0) {
        let exist = vm.customerCheckedPerPage[vm.oldCurrentPage].find(function (item) {
          return item.id == customer.id;
        })
        if (exist && Object.keys(exist).length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },

    checkedAllToggle() {
      let vm = this;
      if (!vm.customerCheckAllPerPage[vm.oldCurrentPage]) {
        let itemIsChecked = [];
        if (vm.customerCheckedPerPage[vm.oldCurrentPage] && vm.customerCheckedPerPage[vm.oldCurrentPage].length > 0) {
          vm.customerCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
            itemIsChecked.push(item.id);
          })
        }
        let newChecked = [];
        vm.listCustomers.forEach(function (customer) {
          if ($.inArray(customer.id, itemIsChecked) == -1) {
            vm.customerChecked.push(customer);
            newChecked.push(customer);
          }
        });
        vm.customerCheckedPerPage[vm.oldCurrentPage] = newChecked;
      } else {
        let newChecked = [];
        vm.customerChecked.forEach(function (customer) {
          if (customer.in_page != vm.pagination.current_page) {
            newChecked.push(customer);
          }
        });
        vm.customerCheckedPerPage[vm.oldCurrentPage] = newChecked;
        vm.customerChecked = newChecked;
      }

      let customerChecked = [];
      if (vm.customerChecked && vm.customerChecked.length > 0) {
        vm.customerChecked.forEach(function (customer) {
          if (!customer.deleted_at) {
            customerChecked.push(customer);
          }
        });
      }
      vm.customerChecked = customerChecked;
    },

    sortInColumnCustomers(attributeId) {
      let vm = this;
      if (vm.currentSortAttribute !== attributeId) {
        vm.sortAttributeValue = 'DESC';
        vm.currentSortAttribute = attributeId;
      }
      if (vm.sortAttributeValue === 'ASC') {
        vm.sortAttributeValue = 'DESC';
      } else if (vm.sortAttributeValue === 'DESC') {
        vm.sortAttributeValue = 'ASC';
      }
      vm.pagination.current_page = 1;
      vm.currentParams.sort = {
        order: vm.sortAttributeValue,
        attribute_id: attributeId
      };
      vm.getAll();
    },

    getPaginationCustomers(page) {
      let vm = this;
      vm.loadDataCustomer = true;
      vm.currentParams.page = page;
      vm.setQueryToUrl();
      vm.getAll("pagination");
      vm.closeCallspanCustomerContactsTable();
      $('#table-responsive-data').animate({
        scrollLeft: 0
      }, 200);
    },

    customerSearch(event) {
      let vm = this;
      let keyCode = event.keyCode;
      let searchText = event.target.value ? event.target.value : '';
      searchText = searchText ? searchText.trim() : '';

      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }

      if (keyCode === 13) {
        vm.customerSearchSetQuery(searchText);
        vm.doneTyping();
      } else {
        vm.clearTimeoutSearch = setTimeout(() => {
          vm.customerSearchSetQuery(searchText);
          vm.doneTyping();
        }, SEARCH_TIMEOUT_DEFAULT);
      }
    },

    customerSearchForClick() {
      let vm = this;
      let searchText = vm.$refs.customerSearch.value ? vm.$refs.customerSearch.value : '';
      searchText = searchText ? searchText.trim() : '';
      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }
      vm.customerSearchSetQuery(searchText);
      vm.doneTyping();
    },

    customerSearchSetQuery(searchText) {
      let vm = this;
      vm.currentParams.page = 1;
      vm.currentParams.search = searchText;
      vm.searchText = searchText;
      vm.customerChecked = [];
      vm.customerCheckAllPerPage = [];
      vm.customerCheckedPerPage = [];
      vm.setQueryToUrl();
      vm.$refs.customerSearch.blur();
    },

    customerTyping() {
      let vm = this;
      clearTimeout(vm.typingTimer);
    },

    doneTyping() {
      let vm = this;
      if (vm.searchText && vm.searchText.length) {
        vm.pagination.current_page = 1;
        vm.currentParams.search = vm.searchText;
        vm.loadDataCustomer = true;
        vm.getAll();
        vm.loadSearch = true;
      } else {
        if (vm.currentParams.search) {
          vm.pagination.current_page = 1;
          delete vm.currentParams.search;
        }
        if (!vm.searchText || (vm.searchText && vm.searchText.length == 0)) {
          vm.loadDataCustomer = true;
          vm.getAll();
          vm.loadSearch = false;
        }
      }
    },

    highlightCustomerAfterSearch() {
      let vm = this;
      let textSearch = helpers.convertLanguages(vm.searchText);

      let attributesCode = {}

      vm.allAttribute.forEach((attribute) => {
        attributesCode[attribute.id] = attribute.attribute_code
      })

      vm.listCustomers.forEach( (customer) => {

        if (vm.$route.query.search && customer.contacts.length > 0) {
          setTimeout(() => {
            vm.toggleCustomerContact('#customer_group_' + customer.id)
            $('[data-target="#customer_group_' + customer.id + '"]').removeClass('collapsed')
            $('[data-target="#customer_group_' + customer.id + '"]').attr('aria-expanded', true)
          }, 10)
        }

        customer.attributes.forEach( (value_attribute) => {
          if (value_attribute.value && attributesCode[value_attribute.attribute_id] === 'sku' || attributesCode[value_attribute.attribute_id] === 'full_name' || attributesCode[value_attribute.attribute_id] === 'email' || attributesCode[value_attribute.attribute_id] === 'phone') {

            if (attributesCode[value_attribute.attribute_id] === 'phone') {
              value_attribute.value = value_attribute.value.replace(/(\d{4})(\d{3})(\d{3,5})/, '$1 $2 $3');
            }

            if (vm.loadSearch) {
              let valConverted = helpers.convertLanguages(value_attribute.value);

              if (valConverted.indexOf(textSearch) >= 0) {

                let startIndexSearchFromValueConverted = valConverted.indexOf(textSearch);
                let searchTextLength = textSearch.length;
                let positionTextSearchInValue = value_attribute.value.substr(startIndexSearchFromValueConverted, searchTextLength);

                value_attribute.value = value_attribute.value.replace(positionTextSearchInValue, '<span class="hightlight">' + positionTextSearchInValue + '</span>');
              }
            }
          }
        })
      })
    },

    afterCreateSuccess() {
      let vm = this;
      vm.customerChecked = [];
      vm.customerCheckAllPerPage = [];
      vm.customerCheckedPerPage = [];
      vm.getAll();
    },

    clearDataBeforeAddNew() {
      $("body").addClass("s--show-modal__overflow-hidden");
    },

    cancelAddNew() {
      let vm = this;
      vm.currentAddCustomerType = vm.customer_types[0];
      $("body").removeClass("s--show-modal__overflow-hidden");
    },

    filterShow(show) {
      let vm = this;
      vm.showFilter = show;
      vm.customerChecked = [];
      vm.customerCheckAllPerPage = [];
      vm.customerCheckedPerPage = [];
    },

    filterAllData(allData) {
      let vm = this;
      vm.isAllData = allData;
    },

    getStyleWhenShowFilter() {
      let vm = this;
      if (!vm.showFilter && vm.isAllData) {
        return '100%';
      } else if (!vm.showFilter && !vm.isAllData) {
        return 'calc(100% - 90px)';
      } else {
        return 'calc(100% - 340px)';
      }
    },

    getStyleMarginTopShowFilter() {
      let vm = this;
      if (!vm.showFilter && vm.isAllData) {
        return '0';
      } else if (!vm.showFilter && !vm.isAllData) {
        return '57.5px';
      } else {
        return '57.5px';
      }
    },

    getStyleFilter() {
      let vm = this;
      if (!vm.showFilter && vm.isAllData) {
        return '320px';
      } else if (!vm.showFilter && !vm.isAllData) {
        return '70px';
      } else {
        return '320px';
      }
    },

    startFilter(params) {
      let vm = this;
      if (params.filter) {
        vm.pagination.current_page = 1;
        vm.currentParams.filter = params.filter;
      } else if (!params.filter && vm.currentParams.filter) {
        vm.pagination.current_page = 1;
        delete vm.currentParams.filter;
      }
      vm.currentParams.page = 1;
      vm.setQueryToUrl();
      vm.loadDataCustomer = true;
      vm.getAll();
    },

    cancelFilter(status) {
      let vm = this;
      if (status) {
        vm.pagination.current_page = 1;
        delete vm.currentParams.filter;
        vm.loadDataCustomer = true;
        vm.getAll();
      }
    },

    getFilters() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_FILTERS).then(() => {}).catch(() => {});
    },

    showFilterGetId(filter) {
      let vm = this;
      if ($.isNumeric(filter.id)) {
        vm.getFilterAttributes(filter.id);
      }
    },

    getFilterAttributes(filterId) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_FILTER_ATTRIBUTES, filterId).then(response => {
        let data = [];
        let cacheParams = [];
        let multiAttributes = [];
        response.data.data.forEach(function (item) {
          vm.checkExistAttributeValueOptions(item);
          let temp = {
            id: item.id,
            filter_id: item.filter_id,
            attribute_id: item.attribute_id,
            attribute_name: item.attribute_name,
            attribute_code: item.attribute_code,
            data_type: item.data_type,
            attribute_values: item.attribute_values,
            external_attribute: item.external_attribute,
            code_external_attribute: item.code_external_attribute,
            attribute_options: item.attribute_options,
            show_position: item.show_position,
            is_auto_gen: vm.thenIsAutoGenerate(item),
            is_disabled: true,
            created_at: item.created_at,
            updated_at: item.updated_at
          };
          cacheParams.push(temp);
          if ((item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && (item.attribute_values.id == 6 || item.attribute_values.id == 7 || item.attribute_values.id != 6 && item.attribute_values.id != 7 && Array.isArray(item.attribute_values.attribute_value)) || (item.data_type == 6 || item.data_type == 7) && item.attribute_values.condition == '<>') {
            multiAttributes.push(item);
          } else {
            let dataType = vm.getDataType(item);
            let value = {
              attribute_id: item.attribute_values.attribute_id,
              data_type: dataType,
              attribute_value: item.attribute_values.attribute_value,
              condition: item.attribute_values.condition,
              dateRange: item.attribute_values.dateRange ? item.attribute_values.dateRange : "",
              errors: item.attribute_values.errors ? item.attribute_values.errors : "",
              id: item.attribute_values.id ? item.attribute_values.id : "",
              rangeDate: item.attribute_values.rangeDate ? item.attribute_values.rangeDate : "",
              type: item.attribute_values.type ? item.attribute_values.type : "",
              numberRange: item.attribute_values.numberRange ? item.attribute_values.numberRange : "",
              error: item.attribute_values.error ? item.attribute_values.error : "",
              filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
            };
            data.push(value);
          }
        })
        if (multiAttributes && multiAttributes.length > 0) {
          let multiParams = vm.getMultipleParams(multiAttributes);
          $.merge(data, multiParams);
        }

        // Set cache filter
        vm.attributeCustomerFull.forEach(function (item) {
          if (cacheParams && cacheParams.length > 0) {
            cacheParams.forEach(function (attribute_filter) {
              if (item.attribute_code == attribute_filter.attribute_code || item.attribute_code == attribute_filter.code_external_attribute) {
                attribute_filter.is_hidden = item.is_hidden;
              }
            });
          }
        })

        vm.cacheForFilterAttributes = JSON.stringify({
          data: cacheParams
        });
        vm.pagination.current_page = 1;
        vm.currentParams.filter = data;
        vm.loadDataCustomer = true;
        vm.getAll();
      }).catch(() => {});
    },

    checkExistAttributeValueOptions(attribute_filter) {
      // Kiểm tra tồn tại attribute option
      let dataType = [8, 9, 10, 11, 13, 14];
      if (attribute_filter && attribute_filter.data_type && dataType.includes(attribute_filter.data_type)) {

        let count = 0;
        let attributeOptionsIds = [];
        let attributeOptionsObj = {};

        if (attribute_filter.attribute_values.attribute_value && attribute_filter.attribute_values.attribute_value.length > 0) {
          attribute_filter.attribute_options.forEach(function (attribute_option) {
            attributeOptionsIds.push(attribute_option.id);
          });

          attribute_filter.attribute_values.attribute_value.forEach(function (attribute_option_id) {
            if (attributeOptionsIds.includes(attribute_option_id) || attribute_option_id === "empty_value") {
              if (!attributeOptionsObj[attribute_option_id]) {
                attributeOptionsObj[attribute_option_id] = attribute_option_id;
              }
            } else {
              count++;
            }
          })
        }

        if (count > 0) {
          attribute_filter.attribute_values.is_attribute_value_option_not_exist = true;
        } else {
          attribute_filter.attribute_values.is_attribute_value_option_not_exist = false;
        }

        if (Object.keys(attributeOptionsObj).length > 0) {
          attribute_filter.attribute_values.attribute_value = Object.values(attributeOptionsObj).map(function (value) {
            if (value === "empty_value") {
              return value;
            }
            return parseInt(value);
          });
        } else {
          attribute_filter.attribute_values.attribute_value = '';
        }
      }
    },

    getMultipleParams(items) {
      let multiArgs = [];
      items.forEach(function (item) {
        if (Array.isArray(item.attribute_values.attribute_value)) {
          let temp_data = item.attribute_values.attribute_value;
          if (item.attribute_values.id != 7 && item.attribute_values.id != 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_data[0] != 'undefined' && typeof temp_data[1] != 'undefined') {
            if (temp_data[0]) {
              let temp_args_min = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: temp_data[0],
                condition: ">=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              };
              multiArgs.push(temp_args_min);
            }
            if (temp_data[1]) {
              let temp_args_max = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: temp_data[1],
                condition: "<=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              }
              multiArgs.push(temp_args_max);
            }
          }
        } else if (item.attribute_values.rangeDate && Array.isArray(item.attribute_values.rangeDate) && item.attribute_values.rangeDate.length > 0) {
          let temp_range_date = item.attribute_values.rangeDate;
          if (item.attribute_values.id == 7 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_range_date[0] != 'undefined' && typeof temp_range_date[1] != 'undefined') {
            let dateMin = new Date(temp_range_date[0]);
            let min = dateMin.getDate() + '/' + (dateMin.getMonth() + 1) + '/' + dateMin.getFullYear();
            if (min != '1/1/1970') {
              let temp_args_min = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: min,
                condition: ">=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              };
              multiArgs.push(temp_args_min);
            }
            let dateMax = new Date(temp_range_date[1]);
            let max = dateMax.getDate() + '/' + (dateMax.getMonth() + 1) + '/' + dateMax.getFullYear();
            if (max != '1/1/1970') {
              let temp_args_max = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: max,
                condition: "<=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              }
              multiArgs.push(temp_args_max);
            }
          }
        } else if (item.attribute_values.dateRange && Object.keys(item.attribute_values.dateRange).length > 0) {
          if (item.attribute_values.id == 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at'))) {
            let temp_args_max = {
              attribute_id: item.attribute_values.attribute_id,
              data_type: "2",
              attribute_code: item.attribute_code,
              attribute_value: item.attribute_values.dateRange,
              condition: "=",
              filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
            }
            multiArgs.push(temp_args_max);
          }
        } else if (typeof item.attribute_values.attribute_value == 'string' || typeof item.attribute_values.attribute_value == 'object') {
          if (item.data_type == 6 || item.data_type == 7) {
            let temp_data = item.attribute_values.numberRange;
            if (typeof temp_data.min != 'undefined' && typeof temp_data.max != 'undefined') {
              if (temp_data.min) {
                let temp_args_min = {
                  attribute_id: item.attribute_values.attribute_id,
                  data_type: "1",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data.min,
                  condition: ">="
                };
                multiArgs.push(temp_args_min);
              }
              if (temp_data.max) {
                let temp_args_max = {
                  attribute_id: item.attribute_values.attribute_id,
                  data_type: "1",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data.max,
                  condition: "<="
                }
                multiArgs.push(temp_args_max);
              }
            }
          }
        } else {
          return;
        }
      })
      return multiArgs;
    },

    convertDateTime(attribute) {
      let date_id = attribute.attribute_values.id;
      let today = new Date();
      let yesterday = new Date(today);
      yesterday.setDate(today.getDate() - 1);
      let yesterdayValue = yesterday.getDate() + '/' + (yesterday.getMonth() + 1) + '/' + yesterday.getFullYear();
      let todayValue = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
      if (attribute.data_type == 2 || attribute.data_type == 3 || attribute.external_attribute == 1 && (attribute.code_external_attribute == 'created_at' || attribute.code_external_attribute == 'updated_at')) {
        if (date_id == "empty_value") {
          return "empty_value";
        } else if (date_id == 2) {
          return yesterdayValue;
        } else if (date_id == 3) {
          return todayValue;
        } else if (date_id == 4) {
          let firstWeek = today.getDate() - today.getDay();
          if (firstWeek < 0) {
            return
          }
          let lastWeek = firstWeek + 6;
          let newFirstWeek = new Date();
          let firstWeekDate = new Date(newFirstWeek.setDate(firstWeek));
          let newLastWeek = new Date();
          let lastWeekDate = new Date(newLastWeek.setDate(lastWeek));
          let firstDayOfWeek = firstWeekDate.getDate() + '/' + (firstWeekDate.getMonth() + 1) + '/' + firstWeekDate.getFullYear();
          let lastDayOfWeek = lastWeekDate.getDate() + '/' + (lastWeekDate.getMonth() + 1) + '/' + lastWeekDate.getFullYear();
          let returnValue = [
            firstDayOfWeek,
            lastDayOfWeek
          ];
          return returnValue;
        } else if (date_id == 5) {
          let firstMonthDate = new Date(today.getFullYear(), today.getMonth(), 1);
          let lastMonthDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          let firstDayOfMonth = firstMonthDate.getDate() + '/' + (firstMonthDate.getMonth() + 1) + '/' + firstMonthDate.getFullYear();
          let lastDayOfMonth = lastMonthDate.getDate() + '/' + (lastMonthDate.getMonth() + 1) + '/' + lastMonthDate.getFullYear();
          let returnValue = [
            firstDayOfMonth,
            lastDayOfMonth
          ];
          return returnValue;
        }
      }
    },

    getDataType(item) {
      if (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) {
        return "2";
      } else if (item.attribute_code == "total_paid_amount") {
        return "3";
      } else {
        return "1";
      }
    },

    thenIsAutoGenerate(item) {
      if ($.inArray(item.attribute_code, ["sku", "full_name", "level", "customer_type", "total_price_when_buyer", "total_product_when_enduser", "total_product_when_buyer", "total_payment_amount", "user_id", "created_at", "updated_at"]) > -1) {
        return true;
      } else {
        return false;
      }
    },

    getQuantityCustomerAction() {
      let vm = this;
      let result = 0;
      if (vm.customerChecked.length > 0) {
        if (vm.customerChecked.length > vm.pagination.total_item_without_deleted) {
          result = vm.pagination.total_item_without_deleted;
        } else {
          result = vm.customerChecked.length;
        }
      } else {
        result = vm.pagination.total_item_without_deleted;
      }
      return result;
    },
    changeLevelCustomerSuccess() {
      let vm = this;
      vm.getAll();
      vm.customerChecked = [];
      vm.customerCheckedPerPage = [];
      vm.customerCheckAllPerPage = [];
    },
    assignersSuccess() {
      let vm = this;
      vm.getAll();
      vm.customerChecked = [];
      vm.customerCheckedPerPage = [];
      vm.customerCheckAllPerPage = [];
      vm.getAllAssignerInTeam();
    },

    exportFileExcel() {
      let vm = this;
      vm.currentParams.page = 1;
      vm.loadDataCustomer = true;
      if (vm.customerChecked.length > 0) {
        setTimeout(function () {
          vm.loadDataCustomer = false;
          exportExcel.exportToExcel(vm.customerChecked, vm.attributeCustomerFull, vm.listUsers, "customer");
        }, 1000)
      } else {
        vm.currentParams.per_page = vm.pagination.total_item;
        vm.$store.dispatch(CUSTOMER_GET_DATA_FOR_EXCEL, vm.currentParams).then(response => {
          if (response.data.data) {
            vm.loadDataCustomer = false;
            exportExcel.exportToExcel(response.data.data, vm.attributeCustomerFull, vm.listUsers, "customer");
          }
        }).catch(() => {});
      }
      vm.hideDropdownListActions()
    },

    getAllAssignerInTeam() {
      let vm = this;
      if (vm.customerChecked.length <= 0) {
        vm.$store.dispatch(CUSTOMER_GET_ALL_ASSIGNER_IN_TEAM).then((response) => {
          let assignerIds = response.data.data;
          if (assignerIds.length > 0) {
            assignerIds.forEach(function (assignerId) {
              vm.listUsers.find(function (user) {
                if (user.id == assignerId) {
                  if (!vm.listAssignerInTeam.includes(user)) {
                    vm.listAssignerInTeam.push(user);
                  }
                }
              });
            });
          } else {
            vm.listAssignerInTeam = [];
          }
        });
      } else {
        vm.listAssignerInTeam = [];
      }
      vm.hideDropdownListActions()
    },

    closeCallspanCustomerContactsTable() {
      if (!this.loadSearch) {
        var elemTable = document.querySelectorAll(".contacts-table.show");
        [].forEach.call(elemTable, function (el) {
          el.classList.remove("show");
        });

        var elemAction = document.querySelectorAll(".customer-group-collapse");
        [].forEach.call(elemAction, function (el) {
          if (!el.classList.contains('collapsed')) {
            el.classList.add("collapsed");
          }

        });
      }
    },

    isChange(isChange) {
      if (isChange) {
        let vm = this;
        vm.loadDataCustomer = true;
        vm.customerChecked = [];
        vm.customerCheckAllPerPage[vm.oldCurrentPage] = false;
        vm.customerCheckedPerPage[vm.oldCurrentPage] = [];
        vm.getAll("pagination");
        $('#table-responsive-data').animate({
          scrollLeft: 0
        }, 200);
      }
    },

    checkHasDealBeforeDelete(customers, is_multiple = false) {
      let vm = this;
      let ids = [];
      let params = {};
      vm.isLoaderDelete = true;

      params.team_id = vm.currentTeamId;
      if (is_multiple) {
        // params.type = customers && customers.length > 0 ? 1 : 2;
        if (customers && customers.length > 0) {
          customers.forEach(function (item) {
            ids.push(item.id);
          });
        }
      } else {
        // params.type = 1;
        ids.push(customers.id);
        vm.customerName = '';
        if (vm.attributeCustomerFull && vm.attributeCustomerFull.length > 0) {
          vm.attributeCustomerFull.forEach(function (attribute) {
            if (customers && customers.attributes.length > 0) {
              customers.attributes.forEach(function (attributeItem) {
                if (attribute.id == attributeItem.attribute_id && attribute.attribute_code == "full_name") {
                  vm.customerName = attributeItem.value;
                }
              })
            }

          });
        }
        vm.hideDropdownListActions()
      }

      params.customer_id = ids;

      vm.isDeletedMultiple = is_multiple;

      vm.$store.dispatch(CUSTOMER_CHECK_HAS_DEAL_BEFORE_DELETE, params).then((response) => {
        let canRemove = response.data.data.can_remove;
        let total = response.data.data.total ? response.data.data.total : 0;
        vm.totalDeleted = total;
        if (canRemove) {
          vm.selectedIds = response.data.data.ids;
          setTimeout(function () {
            $("#trashCustomer").modal('show');
          }, 100)
        } else {
          $("#cancelTrashCustomer").modal('show');
        }
        vm.isLoaderDelete = false;
      });
    },
    reloadTooltip() {
      $("body").tooltip({
        selector: '[data-toggle="m-tooltip"]',
        template: '<div class="m-tooltip tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
      });
    },

    changeTableAttribute(value) {
      let vm = this;
      this.loadDataCustomer = true
      if (value) {
        let params = {
          team_id: vm.currentTeamId,
          user_id: vm.currentUser.id,
          options: value,
          type: 2
        }
        vm.attributes_config_by_user = value
        vm.$store.dispatch(CHANGE_TABLE_ATTRIBUTE_CUSTOMER, params).finally(() => {
          this.loadDataCustomer = false
        })
      }
    },

    async changePerPage() {
      let vm = this;
      vm.loadDataCustomer = true;
      vm.currentParams.per_page = vm.currentPerPage;
      vm.currentParams.page = 1;
      vm.setQueryToUrl();
      await vm.updateUserConfigCustomers();
      await vm.getAll();
    },

    async updateUserConfigCustomers() {
      let vm = this;
      let params = {
        team_id: vm.currentTeamId,
        user_id: vm.currentUser.id,
        options: {
          "per_page": vm.currentParams.per_page
        },
        type: 3
      };
      await vm.$store.dispatch(CHANGE_TABLE_ATTRIBUTE_CUSTOMER, params);
    },

    setQueryToUrl() {
      this.$router.replace({
        name: "customers",
        query: {
          per_page: this.currentParams.per_page,
          page: this.currentParams.page,
          search: this.currentParams.search
        }
      })
    },

    updateSetParams() {
      let params = Object.assign({}, this.currentParams)
      params.customers = this.customerChecked
      params.search = params.search ? params.search : ''
      params.filter = params.filter ? params.filter : ''
      if (params.filter) {
        let filterWithType = {
          assigners: [],
          attributes: []
        }
        params.filter.forEach(function (item) {
          if (item.attribute_id == "charge_person") {
            filterWithType.assigners.push(item)
          }
          if (typeof item.attribute_id !== "undefined" && item.attribute_id !== "charge_person") {
            filterWithType.attributes.push(item)
          } else {
            return;
          }
        })
        params.filter = filterWithType
      }

      return params
    },

    emailSend() {
      let params = this.updateSetParams()
      this.$bus.$emit('emailSendCustomers', params)
      this.hideDropdownListActions()
    },

    updateAttributeCustomers() {
      let params = this.updateSetParams()
      this.$bus.$emit('updateAttributeCustomers', params)
      this.hideDropdownListActions()
    },

    updateAttributeCustomersSuccess(isSuccess) {
      this.resetActionSuccess(isSuccess)
      this.isLoadingS = true
      setTimeout(() => {
        this.isLoadingS = false
      }, 3000)
    },

    resetActionSuccess(isSuccess) {
      if (isSuccess) {
        this.customerChecked = []
        this.customerCheckedPerPage = []
        this.customerCheckAllPerPage = []
      }
    },

    showDropdownListActions() {
      let vm = this;
      setTimeout(() => {
        vm.isToggleDropdownListActions = true
        $('.nt-dropdown-list-actions .m-dropdown__dropoff').remove()
        $('.nt-list-actions-dropdown-backdrop').addClass('show')
      }, 10)
    },

    hideDropdownListActions() {
      this.isToggleDropdownListActions = false
      $('.nt-dropdown-list-actions .m-dropdown__dropoff').remove()
      $('.nt-list-actions-dropdown-backdrop').removeClass('show')
    },
  }
}