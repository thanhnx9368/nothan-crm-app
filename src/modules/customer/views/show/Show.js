/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex"
import VueLadda from "vue-ladda"

import helpers from "@/utils/utils";
import {
  CUSTOMER_SHOW_DETAIL,
  CUSTOMER_RESTORE,
  CHANGE_TABLE_ATTRIBUTE_CUSTOMER,
  GET_CONFIG_CUSTOMER,
  CUSTOMER_UPDATE
} from "@/types/actions.type";
import {
  SET_CUSTOMER_CONFIG
} from '@/types/mutations.type'
import {
  IS_NUMBER,
  PERMISSION_ADD_NOTE_CUSTOMER
} from "@/types/const";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";
import Edit from "../../components/modals/show-edit/ShowEdit.vue";
import InfoTabHead from "../../components/info-tab-head/ShowInfoTabHead.vue";
import MessageTrashCustomer from "../../components/modals/message-trash-customer/MessageTrashCustomer";
import Note from "@/components/note/Note.vue"

export default {
  provide() {
    return {
      $validator: this.$validator,
    };
  },
  components: {
    "info-tab-head": InfoTabHead,
    "edit-popup": Edit,
    "note": Note,
    MessageTrashCustomer: MessageTrashCustomer,
    VueLadda
  },
  computed: {
    ...mapGetters(['userCheckAddOn', 'userCurrentPermissions', 'currentUser', 'currentTeam', 'detailCustomer', 'listUsers']),
    helpers() {
      return helpers;
    },
    isShowTabClass() {
      let data = this.detailCustomer;
      let customerCurrentType = helpers.getAttributeOption(helpers.getAttributeByAttrCode(data, 'customer_type'), helpers.getAttributeByAttrCodeAndVariableName(data, 'customer_type', 'attribute_value'), 'option_value');
      let customerTypes = ["Khách hàng chưa mua và chưa sử dụng", "Khách hàng đã mua", "Khách hàng chỉ mua"];

      if (customerTypes.includes(customerCurrentType)) {
        return false;
      }
      return true;
    },

    isDisabled() {
      if (this.errors.any() || this.isLoadingUpdate || this.isCountAttributeChange <= 0) {
        return true;
      }
      return false;
    },

    customerConfigs() {
      return this.$store.state.customer.customerConfigs
    },

    isCountAttributeChange() {
      let countChange = 0;
      let detailCustomer = this.detailCustomer
      let isChangeAttributes = detailCustomer.attributes.filter((attribute) => attribute.is_change_attribute_value)

      if (isChangeAttributes.length) {
        isChangeAttributes.forEach(attribute => {
          if (attribute.attribute_code != 'level' && attribute.attribute_code != 'avatar') {
            countChange++
          }
        })
      }

      if (detailCustomer.assigners_cache && detailCustomer.assigners_cache.length > 0) {
        if (detailCustomer.assigners && detailCustomer.assigners.length > 0) {
          if (detailCustomer.assigners.join() != detailCustomer.assigners_cache.join()) {
            countChange++;
          }
        } else if (detailCustomer.assigners.length <= 0) {
          countChange++;
        }
      } else {
        if (detailCustomer.assigners && detailCustomer.assigners.length > 0) {
          countChange++;
        }
      }
      return countChange
    },

    countErrorBuyerCustomerInfo() {
      let count = 0
      let errors = {}

      if (this.errors.items.length) {
        this.errors.items.forEach(error => {
          if (!errors[error.field]) {
            errors[error.field] = error.field
          }
        })
      }

      let buyerCustomerInfo = this.$store.state.customer.buyerCustomerInfo

      buyerCustomerInfo.forEach(attribute => {
        if (attribute.attribute_code === errors[attribute.attribute_code] || this.server_errors[attribute.attribute_code]) {
          count++
        }
      })

      return count
    },

    countErrorBasicCustomerInfo() {
      let count = 0
      let errors = {}

      if (this.errors.items.length) {
        this.errors.items.forEach(error => {
          if (!errors[error.field]) {
            errors[error.field] = error.field
          }
        })
      }

      let basicCustomerInfo = this.$store.state.customer.basicCustomerInfo

      basicCustomerInfo.forEach(attribute => {
        if (attribute.attribute_code === errors[attribute.attribute_code] || this.server_errors[attribute.attribute_code]) {
          count++
        }
      })

      return count
    },

    countErrorUsedCustomerInfo() {
      let count = 0
      let errors = {}

      if (this.errors.items.length) {
        this.errors.items.forEach(error => {
          if (!errors[error.field]) {
            errors[error.field] = error.field
          }
        })
      }

      let usedCustomerInfo = this.$store.state.customer.usedCustomerInfo

      usedCustomerInfo.forEach(attribute => {
        if (attribute.attribute_code === errors[attribute.attribute_code] || this.server_errors[attribute.attribute_code]) {
          count++
        }
      })

      return count
    }

  },
  created() {
    this.listenEventBus()
    this.getShowCustomer()
    this.getConfig()
    this.$bus.$on('updateDetailsCustomer', ($event) => {
      if ($event) {
        this.updateCustomerDetails()
      }
    })
  },
  mounted() {
    // vm.getCustomerDetailListDeal(vm.customerId);
    $("#m_header").css("display", "block");
    if (!$("#app-body").hasClass("nt-none-padding-top")) {
      $("#app-body").removeClass("nt-none-padding-top");
    }
    if (!$("#app-body").hasClass("m-body")) {
      $("#app-body").addClass("m-body");
    }
    $("#app-body-subheader").css("display", "block");
    if (this.$route.query.reorder === 'true' || this.$route.query.reorder === true) {
      this.hideActionBar()
      this.isReorder = true
    } else {
      this.isReorder = false
    }
  },

  data() {
    return {
      PERMISSION_ADD_NOTE_CUSTOMER: PERMISSION_ADD_NOTE_CUSTOMER,
      isLoaded: false,
      customerId: parseInt(this.$route.params.customer_id),
      isLoading: false,
      isChangeData: false,
      isReorder: false,
      isLoadingReorder: false,
      isNumber: IS_NUMBER,

      isLoadingUpdate: false,
      server_errors: ''
    }
  },
  methods: {
    listenEventBus() {
      this.$bus.$on('customerRestore', ($event) => {
        if ($event) {
          $("#restoreCustomer").modal('show');
        }
      })
      this.$bus.$on('customerDetailsReorder', () => {
        this.isReorder = true
        this.hideActionBar()
        this.updateQuery()
      })
    },

    hideActionBar() {
      const actions = document.getElementById('nt_header-actions')
      actions.classList.add('d-none')
    },

    showActionBar() {
      const actions = document.getElementById('nt_header-actions')
      actions.classList.remove('d-none')
      $('#nt_header-actions .m-dropdown__dropoff').remove()
    },

    onCancelReorder(isEmit = true) {
      if (isEmit) {
        this.$bus.$emit('customerDetailsCancelReorder')
      }
      this.isReorder = false
      this.showActionBar()
      this.updateQuery()
    },

    updateQuery() {
      const query = Object.assign({}, this.$route.query)
      query.reorder = this.isReorder
      this.$router.push({
        query
      })
    },

    onUpdateConfig() {
      const {
        basicCustomerInfo,
        buyerCustomerInfo,
        usedCustomerInfo,
      } = this.$store.state.customer

      const options = [...basicCustomerInfo, ...buyerCustomerInfo, ...usedCustomerInfo].map((t, i) => ({
        attribute_code: t.attribute_code,
        sort_order: i + 1
      }))
      options.map((t, index) => t.sort_order = index + 1)

      let params = {
        team_id: this.currentTeamId,
        user_id: this.currentUser.id,
        options,
        type: 4
      }
      this.isLoadingReorder = true
      this.$store.dispatch(CHANGE_TABLE_ATTRIBUTE_CUSTOMER, params).then(() => {
        this.$store.commit(SET_CUSTOMER_CONFIG, options)
        this.$forceUpdate()

        this.isLoadingReorder = false
        this.onCancelReorder(false)
        this.$snotify.success('Chỉnh sửa giao diện thành công.', TOAST_SUCCESS);
      })
    },

    restore() {
      let vm = this;
      let params = {};

      params.team_id = vm.currentTeamId;
      params.customer_id = [vm.customerId];
      params.type = 1;

      vm.$store.dispatch(CUSTOMER_RESTORE, params).then((response) => {
        if (response.data.data.success) {
          vm.$store.dispatch(CUSTOMER_SHOW_DETAIL, vm.customerId);
          $('.nt-list-top-actions a').removeClass('nt-button-disabled');
          $('.nt-list-top-actions .nt-button-restored').addClass('nt-button-hide');
          vm.$snotify.success('Phục hồi khách hàng thành công.', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Phục hồi khách hàng thất bại.', TOAST_ERROR);
      });
    },

    async getShowCustomer() {
      let vm = this;
      return await vm.$store.dispatch(CUSTOMER_SHOW_DETAIL, vm.customerId).then(response => {
        let data = response.data.data;
        if (data) {
          if (data.deleted_at) {
            $('.nt-list-top-actions a').addClass('nt-button-disabled');
            $('.nt-list-top-actions .nt-button-restored').removeClass('nt-button-disabled');
            $('.nt-list-top-actions .nt-button-restored').removeClass('nt-button-hide');
          }

          vm.isLoaded = true;
          if (vm.isChangeData) {
            vm.isLoading = true;
          }
        }
      }).catch(error => {
        console.log(error);
      });
    },

    async getConfig() {
      let params = {
        type: 4
      };
      await this.$store.dispatch(GET_CONFIG_CUSTOMER, params)
    },

    reloadData(status) {
      let vm = this;
      if (status) {
        vm.getShowCustomer();
      }
    },

    updateSuccess(status) {
      let vm = this;
      if (status) {
        vm.isChangeData = true;
        vm.getShowCustomer();
        vm.setDetailsCustomerAfterUpdate(vm.detailCustomer)
        vm.$bus.$emit('resetUpdateDetailsCustomer', true)
      }
    },

    afterUpdated(status) {
      let vm = this;
      if (status == "avatar") {
        vm.$snotify.success("Cập nhật ảnh đại diện thành công.", TOAST_SUCCESS);
      } else if (status == "level") {
        vm.$snotify.success("Cập nhật cấp độ thành công.", TOAST_SUCCESS);
      }
      vm.isChangeData = false;
      vm.isLoading = false;
      vm.setDetailsCustomerAfterUpdate(vm.detailCustomer)
      vm.$bus.$emit('resetUpdateDetailsCustomer', true)
    },

    // Update
    cancelUpdateCustomerDetails() {
      this.server_errors = ''
      this.$validator.reset()
      this.setDetailsCustomerAfterCancelUpdate(this.detailCustomer)
      this.$forceUpdate()
      this.$bus.$emit('resetUpdateDetailsCustomer', true)
    },

    setDetailsCustomerAfterCancelUpdate(detailCustomer) {
      detailCustomer.assigners = detailCustomer.assigners_cache
      detailCustomer.attributes.forEach((attribute) => {
        if (attribute.is_change_attribute_value) {
          attribute.is_change_attribute_value = false
          attribute.attribute_value = attribute.attribute_value_cache
        }
      })
    },

    setDetailsCustomerAfterUpdate(detailCustomer) {
      let vm = this;
      detailCustomer.assigners_cache = detailCustomer.assigners
      detailCustomer.attributes.forEach((attribute) => {
        if (attribute.is_change_attribute_value) {
          attribute.is_change_attribute_value = false

          let attribute_value = attribute.attribute_value ? attribute.attribute_value : ''
          if (attribute.data_type.id === vm.isNumber) {
            attribute_value = attribute_value ? String(attribute_value).replace('.', ',') : ''
          }

          attribute.attribute_value = attribute_value
          attribute.attribute_value_cache = attribute_value
        }
      })
    },

    changeUpdateAttributes() {
      if (this.isCountAttributeChange > 0) {
        this.$bus.$emit('checkValidateDetailsCustomer', true)
      }
    },

    updateCustomerDetails(scope) {
      let vm = this;
      vm.$bus.$emit('checkValidateDetailsCustomer', true)
      setTimeout(() => {
        vm.$validator.validateAll(scope).then(() => {
          if (!vm.errors.any() && vm.isCountAttributeChange > 0) {
            vm.isLoadingUpdate = true

            // Get data send mail
            let assignersSendMail = []

            if (vm.detailCustomer.assigners_cache && vm.detailCustomer.assigners_cache.length > 0) {
              vm.detailCustomer.assigners.forEach(function (assigners_id) {
                if (vm.detailCustomer.assigners >= vm.detailCustomer.assigners_cache) {
                  if (vm.detailCustomer.assigners_cache.indexOf(assigners_id) === -1) {
                    assignersSendMail.push(assigners_id)
                  }
                }
              });
            } else {
              vm.detailCustomer.assigners.forEach(function (id) {
                assignersSendMail.push(id)
              });
            }

            let newAssignerSendMail = {}
            newAssignerSendMail['data'] = []

            let customerName = ""

            if (vm.detailCustomer.attributes && vm.detailCustomer.attributes.length > 0) {
              vm.detailCustomer.attributes.forEach(function (item) {
                if (item.attribute_code == "full_name") {
                  customerName = item.attribute_value
                }
              })
            }

            vm.listUsers.forEach(function (item) {
              assignersSendMail.forEach(function (userId) {
                if (item.id === userId) {
                  newAssignerSendMail['data'].push({
                    email: item.email,
                    user_name: item.full_name,
                    customer_name: customerName,
                    team_name: vm.currentTeam.name
                  })
                }
              })
            })

            let detailCustomer = vm.detailCustomer
            detailCustomer.data_mail = newAssignerSendMail['data']
            detailCustomer.type_mail = 6

            vm.$store.dispatch(CUSTOMER_UPDATE, detailCustomer).then(response => {
              if (response.data.status_code && response.data.status_code == 422) {
                vm.server_errors = response.data.errors
                vm.isLoadingUpdate = false

                if (vm.server_errors["attribute_not_exist"] || vm.server_errors["option_not_exist"]) {
                  vm.$snotify.error('Cập nhật khách hàng thất bại.', TOAST_ERROR)
                  $("#messageUpdateError").modal('show')
                  return
                }

                vm.$snotify.error('Cập nhật khách hàng thất bại.', TOAST_ERROR)
                return

              } else {
                vm.isLoadingUpdate = false

                if (response.data.data.sort_delete) {
                  vm.$snotify.error('Cập nhật khách hàng thất bại.', TOAST_ERROR)
                  $("#messageTrashCustomer").modal('show')
                  return
                }

                if (response.data.data && response.data.data.status) {
                  vm.server_errors = ''
                  vm.$snotify.success('Cập nhật khách hàng thành công.', TOAST_SUCCESS)
                  vm.setDetailsCustomerAfterUpdate(vm.detailCustomer)
                  vm.$bus.$emit('resetUpdateDetailsCustomer', true)
                }

              }
            }).catch(() => {
              vm.isLoadingUpdate = false;
            })
          }
        })
      }, 200)
    },

    reloadPage() {
      return window.location.reload()
    },

    reloadTooltip() {
      $(".nt-tabs-item").tooltip({
        selector: '[data-toggle="m-tooltip"]',
        template: '<div class="m-tooltip tooltip nt-tooltip-error" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
      });
    },
  }
}