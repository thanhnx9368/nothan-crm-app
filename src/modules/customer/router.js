import {
  PERMISSION_IMPORT_CUSTOMER,
  PERMISSION_CREATE_CUSTOMER,
  PERMISSION_SHOW_CUSTOMER,
  PERMISSION_CUSTOMER_RESTORED,
  PERMISSION_ADD_DEAL,
  // PERMISSION_UPDATE_CUSTOMER_BASE,
  // PERMISSION_UPDATE_CUSTOMER_GENERAL,
  // PERMISSION_UPDATE_CUSTOMER_BUYER,
  // PERMISSION_UPDATE_CUSTOMER_END_USER
} from '@/types/const'
import utils from '@/utils/utils'
import Customer from './views/customer/Customer.vue'
import ShowInfoBasic from './components/info-basic/ShowInfoBasic.vue'
import Show from './views/show/Show.vue'
import ShowInfoCustomerBuy from './components/info-customer-buy/ShowInfoCustomerBuy.vue'
import ShowInfoCustomerUse from './components/info-customer-use/ShowInfoCustomerUse.vue'
import ShowInfoContacts from './components/info-contact/ShowInfoContacts.vue'
import ShowInfoCustomerDeals from '@/modules/deal/components/show-info-customer/ShowInfoCustomerDeals.vue'
import ListClassOfCustomerDetail from '@/modules/education/components/customer-class-list/ListClassOfCustomerDetail.vue'

export default [{
  name: "customers",
  path: "/customers",
  component: Customer,
  meta: {
    breadcrumb: [{
      name: "Khách hàng",
    }, {
      name: "Danh sách khách hàng",
    }],
    actions: [{
      type: 1,
      class: "nt-button nt-button-top nt-button-primary",
      name: "Import danh sách",
      isIcon: 1,
      iconClass: "la la-upload",
      isModal: 1,
      idModal: "#importCustomer",
      permissionActionName: PERMISSION_IMPORT_CUSTOMER,
      serviceName: "customer"
    }, {
      type: 1,
      class: "nt-button nt-button-top nt-button-warning",
      name: "Thêm khách hàng",
      isIcon: 1,
      iconClass: "icon icon-plus",
      isModal: 1,
      // idModal : "#chooseAddNewCustomer"
      idModal: "#addNewCustomer",
      permissionActionName: PERMISSION_CREATE_CUSTOMER,
      serviceName: "customer"
    }]
  },
}, {
  name: "customerDetails",
  path: "/customers/:customer_id",
  component: Show,
  // redirect: {
  //   name: 'InfoBasic'
  // },
  children: utils.prefixRouter('/team/:team_id/customers', [{
    path: "/:customer_id",
    name: "InfoBasic",
    component: ShowInfoBasic,
    meta: {
      // check redirect 403
      permissionName: PERMISSION_SHOW_CUSTOMER,
      serviceName: "customer",
      breadcrumb: [{
        name: "Khách hàng",
        link: "/team/:team_id/customers"
      }, {
        name: "Chi tiết khách hàng",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "customerRestore",
        position: "before",
        permissionActionName: PERMISSION_CUSTOMER_RESTORED,
        serviceName: "customer"
      }, {
        type: 5,
        class: "nt-button nt-button-top nt-button-secondary mr-0",
        name: "Tác vụ",
        nameLink: "DealAddNew",
        permissionActionName: 'all',
        serviceName: "customer",
        id: 'customerDetailsReorder',
        array: [{
          name: 'Chỉnh sửa giao diện',
          icon: 'icon-layout',
          action: 'customerDetailsReorder'
        }]
      }, {
        type: 3,
        class: "nt-button nt-button-top nt-button-warning",
        name: "Thêm giao dịch",
        isIcon: 1,
        iconClass: "icon icon-plus",
        isModal: 0,
        idModal: "",
        nameLink: "DealAddNew",
        // check show hide action
        permissionActionName: PERMISSION_ADD_DEAL,
        serviceName: "deal"
      }, 
      // {
      //   type: 4,
      //   class: "nt-button nt-button-top nt-button-warning",
      //   name: "Chỉnh sửa",
      //   isIcon: 1,
      //   iconClass: "icon icon-edit",
      //   // isModal : 1,
      //   // idModal : "#editCustomer"
      //   nameFunction: "customerEdit",
      //   position: "after",
      //   permissionActionName: [PERMISSION_UPDATE_CUSTOMER_BASE, PERMISSION_UPDATE_CUSTOMER_GENERAL, PERMISSION_UPDATE_CUSTOMER_BUYER, PERMISSION_UPDATE_CUSTOMER_END_USER],
      //   serviceName: "customer",
      //   isMultipleAction: true
      // }
    ]
    },
  }, {
    path: "/:customer_id/customer-buy",
    name: "InfoCustomerBuy",
    component: ShowInfoCustomerBuy,
    meta: {
      permissionName: PERMISSION_SHOW_CUSTOMER,
      serviceName: "customer",
      breadcrumb: [{
        name: "Khách hàng",
        link: "/team/:team_id/customers"
      }, {
        name: "Chi tiết khách hàng",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "customerRestore",
        position: "before",
        permissionActionName: PERMISSION_CUSTOMER_RESTORED,
        serviceName: "customer"
      }, {
        type: 5,
        class: "nt-button nt-button-top nt-button-secondary mr-0",
        name: "Tác vụ",
        nameLink: "DealAddNew",
        permissionActionName: 'all',
        serviceName: "customer",
        id: 'customerDetailsReorder',
        array: [{
          name: 'Chỉnh sửa giao diện',
          icon: 'icon-layout',
          action: 'customerDetailsReorder'
        }]
      }, {
        type: 3,
        class: "nt-button nt-button-top nt-button-warning",
        name: "Thêm giao dịch",
        isIcon: 1,
        iconClass: "icon icon-plus",
        isModal: 0,
        idModal: "",
        nameLink: "DealAddNew",
        // check show hide action
        permissionActionName: PERMISSION_ADD_DEAL,
        serviceName: "deal"
      },
      //  {
      //   type: 4,
      //   class: "nt-button nt-button-top nt-button-warning",
      //   name: "Chỉnh sửa",
      //   isIcon: 1,
      //   iconClass: "icon icon-edit",
      //   // isModal : 1,
      //   // idModal : "#editCustomer"
      //   nameFunction: "customerEdit",
      //   position: "after",
      //   permissionActionName: [PERMISSION_UPDATE_CUSTOMER_BASE, PERMISSION_UPDATE_CUSTOMER_GENERAL, PERMISSION_UPDATE_CUSTOMER_BUYER, PERMISSION_UPDATE_CUSTOMER_END_USER],
      //   serviceName: "customer",
      //   isMultipleAction: true
      // }
    ],
      tags: {
        title: "Chi tiết khách hàng | Khách hàng",
        content: "",
        description: ""
      }
    },
  }, {
    path: "/:customer_id/customer-use",
    name: "InfoCustomerUse",
    component: ShowInfoCustomerUse,
    meta: {
      permissionName: PERMISSION_SHOW_CUSTOMER,
      serviceName: "customer",
      breadcrumb: [{
        name: "Khách hàng",
        link: "/team/:team_id/customers"
      }, {
        name: "Chi tiết khách hàng",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "customerRestore",
        position: "before",
        permissionActionName: PERMISSION_CUSTOMER_RESTORED,
        serviceName: "customer"
      }, {
        type: 5,
        class: "nt-button nt-button-top nt-button-secondary mr-0",
        name: "Tác vụ",
        nameLink: "DealAddNew",
        permissionActionName: 'all',
        serviceName: "customer",
        id: 'customerDetailsReorder',
        array: [{
          name: 'Chỉnh sửa giao diện',
          icon: 'icon-layout',
          action: 'customerDetailsReorder'
        }]
      }, {
        type: 3,
        class: "nt-button nt-button-top nt-button-warning",
        name: "Thêm giao dịch",
        isIcon: 1,
        iconClass: "icon icon-plus",
        isModal: 0,
        idModal: "",
        nameLink: "DealAddNew",
        // check show hide action
        permissionActionName: PERMISSION_ADD_DEAL,
        serviceName: "deal"
      },
      //  {
      //   type: 4,
      //   class: "nt-button nt-button-top nt-button-warning",
      //   name: "Chỉnh sửa",
      //   isIcon: 1,
      //   iconClass: "icon icon-edit",
      //   // isModal : 1,
      //   // idModal : "#editCustomer"
      //   nameFunction: "customerEdit",
      //   position: "after",
      //   permissionActionName: [PERMISSION_UPDATE_CUSTOMER_BASE, PERMISSION_UPDATE_CUSTOMER_GENERAL, PERMISSION_UPDATE_CUSTOMER_BUYER, PERMISSION_UPDATE_CUSTOMER_END_USER],
      //   serviceName: "customer",
      //   isMultipleAction: true
      // }
    ],
      tags: {
        title: "Chi tiết khách hàng | Khách hàng",
        content: "",
        description: ""
      }
    },
  }, {
    path: "/:customer_id/contacts",
    name: "InfoContacts",
    component: ShowInfoContacts,
    meta: {
      permissionName: PERMISSION_SHOW_CUSTOMER,
      serviceName: "customer",
      breadcrumb: [{
        name: "Khách hàng",
        link: "/team/:team_id/customers"
      }, {
        name: "Chi tiết khách hàng",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "customerRestore",
        position: "before",
        permissionActionName: PERMISSION_CUSTOMER_RESTORED,
        serviceName: "customer"
      }, {
        type: 3,
        class: "nt-button nt-button-top nt-button-warning",
        name: "Thêm giao dịch",
        isIcon: 1,
        iconClass: "icon icon-plus",
        isModal: 0,
        idModal: "",
        nameLink: "DealAddNew",
        // check show hide action
        permissionActionName: PERMISSION_ADD_DEAL,
        serviceName: "deal"
      }, 
      // {
      //   type: 4,
      //   class: "nt-button nt-button-top nt-button-warning",
      //   name: "Chỉnh sửa",
      //   isIcon: 1,
      //   iconClass: "icon icon-edit",
      //   // isModal : 1,
      //   // idModal : "#editCustomer"
      //   nameFunction: "customerEdit",
      //   position: "after",
      //   permissionActionName: [PERMISSION_UPDATE_CUSTOMER_BASE, PERMISSION_UPDATE_CUSTOMER_GENERAL, PERMISSION_UPDATE_CUSTOMER_BUYER, PERMISSION_UPDATE_CUSTOMER_END_USER],
      //   serviceName: "customer",
      //   isMultipleAction: true
      // }
    ],
      tags: {
        title: "Chi tiết khách hàng | Khách hàng",
        content: "",
        description: ""
      }
    },
  }, {
    path: "/:customer_id/deals",
    name: "InfoCustomerDeals",
    component: ShowInfoCustomerDeals,
    meta: {
      permissionName: PERMISSION_SHOW_CUSTOMER,
      serviceName: "customer",
      breadcrumb: [{
        name: "Khách hàng",
        link: "/team/:team_id/customers"
      }, {
        name: "Chi tiết khách hàng",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "customerRestore",
        position: "before",
        permissionActionName: PERMISSION_CUSTOMER_RESTORED,
        serviceName: "customer"
      }, {
        type: 3,
        class: "nt-button nt-button-top nt-button-warning",
        name: "Thêm giao dịch",
        isIcon: 1,
        iconClass: "icon icon-plus",
        isModal: 0,
        idModal: "",
        nameLink: "DealAddNew",
        // check show hide action
        permissionActionName: PERMISSION_ADD_DEAL,
        serviceName: "deal"
      },
      //  {
      //   type: 4,
      //   class: "nt-button nt-button-top nt-button-warning",
      //   name: "Chỉnh sửa",
      //   isIcon: 1,
      //   iconClass: "icon icon-edit",
      //   // isModal : 1,
      //   // idModal : "#editCustomer"
      //   nameFunction: "customerEdit",
      //   position: "after",
      //   permissionActionName: [PERMISSION_UPDATE_CUSTOMER_BASE, PERMISSION_UPDATE_CUSTOMER_GENERAL, PERMISSION_UPDATE_CUSTOMER_BUYER, PERMISSION_UPDATE_CUSTOMER_END_USER],
      //   serviceName: "customer",
      //   isMultipleAction: true
      // }
    ],
      tags: {
        title: "Chi tiết khách hàng | Khách hàng",
        content: "",
        description: ""
      }
    },
  }, {
    path: "/:customer_id/list-class",
    name: "ListClassOfCustomerDetail",
    component: ListClassOfCustomerDetail,
    meta: {
      permissionName: PERMISSION_SHOW_CUSTOMER,
      serviceName: "customer",
      breadcrumb: [{
        name: "Khách hàng",
        link: "/team/:team_id/customers"
      }, {
        name: "Chi tiết khách hàng",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "customerRestore",
        position: "before",
        permissionActionName: PERMISSION_CUSTOMER_RESTORED,
        serviceName: "customer"
      }, {
        type: 3,
        class: "nt-button nt-button-top nt-button-warning",
        name: "Thêm giao dịch",
        isIcon: 1,
        iconClass: "icon icon-plus",
        isModal: 0,
        idModal: "",
        nameLink: "DealAddNew",
        // check show hide action
        permissionActionName: PERMISSION_ADD_DEAL,
        serviceName: "deal"
      },
      //  {
      //   type: 4,
      //   class: "nt-button nt-button-top nt-button-warning",
      //   name: "Chỉnh sửa",
      //   isIcon: 1,
      //   iconClass: "icon icon-edit",
      //   // isModal : 1,
      //   // idModal : "#editCustomer"
      //   nameFunction: "customerEdit",
      //   position: "after",
      //   permissionActionName: [PERMISSION_UPDATE_CUSTOMER_BASE, PERMISSION_UPDATE_CUSTOMER_GENERAL, PERMISSION_UPDATE_CUSTOMER_BUYER, PERMISSION_UPDATE_CUSTOMER_END_USER],
      //   serviceName: "customer",
      //   isMultipleAction: true
      // }
    ],
      roles: {
        "education": []
      },
      tags: {
        title: "Chi tiết khách hàng | Khách hàng",
        content: "",
        description: ""
      }
    },
  }])
}]