/* eslint-disable no-undef */
const TYPE_SELECT = [8, 9, 14];
const TYPE_DATETIME = [2, 3];
const TYPE_SELECT_MULTIPLE = [10, 11, 13, 16];
import {
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER_MULTIPLE
} from "@/types/const"

export default {
  transform(data) {
    let transform = {};
    let assignersCache = [];
    if (data.assigners && data.assigners.length > 0) {
      data.assigners.forEach(function (item) {
        if (item instanceof Object == true) {
          assignersCache.push(item.user_id);
        } else {
          assignersCache.push(item);
        }
      });
    }

    transform = {
      id: data.id,
      team_id: data.team_id,
      user_id: data.user_id,
      assigners: data.assigners,
      assigners_cache: assignersCache,
      contacts: data.contacts,
      is_active: data.is_active,
      customer_type_id: data.customer_type_id,
      attributes: this.attributes(data.attributes),
      attributes_cache: this.attributes(data.attributes),
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
    };

    return transform;
  },
  attributes(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformAttribute(item));
    });

    return new_data;
  },
  transformAttribute(data) {
    let transform = {};

    let attribute_value = "";
    let attribute_value_cache = "";

    if (data.attribute_code != "full_name") {
      attribute_value = data.attribute_value ? data.attribute_value : '';
      attribute_value_cache = attribute_value ? attribute_value : '';
    } else {
      attribute_value = data.attribute_value ? data.attribute_value : '';
      attribute_value_cache = attribute_value;
    }

    let sort_attribute_options = [];
    sort_attribute_options = data.attribute_options;

    if (data.data_type_id == 4) {
      attribute_value_cache = attribute_value_cache ? attribute_value_cache.replace(/(\d{4})(\d{3})(\d{3,5})/, '$1 $2 $3') : '';
      attribute_value = attribute_value ? attribute_value.replace(/(\d{4})(\d{3})(\d{3,5})/, '$1 $2 $3') : '';
    }

    if (sort_attribute_options && sort_attribute_options.length > 0) {
      sort_attribute_options.sort(function (before, after) {
        return before.sort_order - after.sort_order;
      });
    }


    // Set permisson type update
    //  type_update: data.show_position ? data.show_position : 1,
    let type_update = 1
    let tabHeadAttributeInfo = ['full_name', 'phone', 'email', 'address', 'birthday_set_to_date', 'gender', 'level', 'avatar']
    if (tabHeadAttributeInfo.indexOf(data.attribute_code) !== -1) {
      type_update = 1
    } else {
      if (data.show_position) {
        if (data.show_position === 1) {
          type_update = 2
        }
        if (data.show_position === 2) {
          type_update = 3
        }
        if (data.show_position === 3) {
          type_update = 4
        }
      } else {
        type_update = 2
      }
    }

    transform = {
      id: data.id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      data_type_id: data.data_type_id,
      is_hidden: data.is_hidden,
      is_required: data.is_required,
      is_default_attribute: data.is_default_attribute,
      is_unique: data.is_unique,
      is_multiple_value: data.is_multiple_value,
      default_value: data.default_value,
      hint: data.hint,
      note: data.note,
      sort_order: data.sort_order,
      created_at: data.created_at,
      updated_at: data.updated_at,
      data_type: data.data_type,
      attribute_options: sort_attribute_options,
      attribute_value: this.transformValueAttribute(attribute_value, data.data_type_id),
      attribute_value_cache: this.transformValueAttribute(attribute_value_cache, data.data_type_id),
      show_position: data.show_position,
      customer_attribute_id: data.customer_attribute_id,
      type_update: type_update,
      is_change_attribute_value: false
    };

    return transform;
  },

  transformValueAttribute(value, dataTypeId) {
    let newValue = "";
    if ($.isNumeric(value) && dataTypeId != 6) {
      newValue = parseInt(value);
    } else if ($.inArray(dataTypeId, TYPE_SELECT) != -1 && value == "0") {
      newValue = "";
    } else if ($.inArray(dataTypeId, TYPE_DATETIME) != -1 && value == "0000-00-00 00:00:00") {
      newValue = "";
    } else if ($.inArray(dataTypeId, TYPE_SELECT_MULTIPLE) != -1) {
      let newValueArr = [];
      if (value && value.length > 0) {
        value.forEach(function (item) {
          if (item && item != null) {
            newValueArr.push(parseInt(item));
          }
        });
        newValue = newValueArr;
      } else {
        newValue = value;
      }
    } else if (dataTypeId == 6) {
      newValue = value ? value.replace('.', ',') : '';
    } else {
      newValue = value;
    }

    if (dataTypeId === IS_MULTIPLE_SELECT ||
      dataTypeId === IS_CHECKBOX_MULTIPLE ||
      dataTypeId === IS_TAG ||
      dataTypeId === IS_USER_MULTIPLE
    ) {
      newValue = newValue ? newValue : []
    }

    return newValue;
  },
}