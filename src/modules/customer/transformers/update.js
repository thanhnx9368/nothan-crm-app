import {
  IS_DATE,
  IS_DATE_TIME,
  IS_PHONE,
  IS_NUMBER,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
} from "@/types/const";
import helpers from "@/utils/utils";

export default {
  transform(data) {
    let transform = {};

    let typeUpdateObj = {}
    typeUpdateObj[2] = 2
    data.attributes.forEach(function (item) {
      if (item.is_change_attribute_value) {
        if (!typeUpdateObj[item.type_update]) {
          typeUpdateObj[item.type_update] = item.type_update
        }
      }
    })

    transform = {
      id: data.id,
      team_id: data.team_id,
      user_id: data.user_id,
      assigners: data.assigners,
      is_active: data.is_active,
      type_update: Object.values(typeUpdateObj),
      data_mail: data.data_mail,
      type_mail: data.type_mail,
      attributes: this.attributes(data.attributes),
    };

    return transform;
  },

  attributes(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      if (item.is_change_attribute_value) {
        new_data.push(vm.transformAttribute(item));
      }
    });

    return new_data;
  },

  transformAttribute(data) {
    let transform = {};

    data.is_attribute_option = 0;

    if (data.data_type_id == IS_SELECT ||
      data.data_type_id == IS_SELECT_FIND ||
      data.data_type_id == IS_RADIO ||
      data.data_type_id == IS_MULTIPLE_SELECT ||
      data.data_type_id == IS_CHECKBOX_MULTIPLE ||
      data.data_type_id == IS_TAG) {
      data.is_attribute_option = 1
    }

    // Convert data
    if (data.data_type_id == IS_PHONE) {
      data.attribute_value = data.attribute_value ? data.attribute_value.replace(/ /g, "") : '';
    }
    if (data.data_type_id == IS_DATE) {
      data.attribute_value = data.attribute_value ? helpers.formatDateTime(data.attribute_value) : '';
    }
    if (data.data_type_id == IS_DATE_TIME) {
      data.attribute_value = data.attribute_value ? helpers.formatDateTime(data.attribute_value, true) : '';
    }
    if (data.data_type_id == IS_NUMBER) {
      data.attribute_value = data.attribute_value ? this.convertNumberDecimal(data.attribute_value) : '';
    }

    transform = {
      id: data.customer_attribute_id,
      attribute_id: data.id,
      attribute_value: data.attribute_value,
      data_type_id: data.data_type_id,
      is_attribute_option: data.is_attribute_option || data.is_attribute_option != undefined ? data.is_attribute_option : 0
    };

    return transform;
  },

  convertNumberDecimal(value = "") {
    let newValue = String(value);
    if (newValue.length > 0) {
      newValue = parseFloat(newValue.replace(",", "."));
    }
    return newValue;
  },
}