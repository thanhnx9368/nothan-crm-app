import listVM from './list';
import showVM from './show';
import attributeVM from './attributes';
import configAttributeTableVM from './configAttributeTable';
import filterVM from './filter';
import filterAttributeVM from './filterAttributes';
import userAttributeVM from './userAttributes';
import createVM from './create';
import updateVM from './update';
import contactVM from './contacts';
import contactShowVM from './contactShow';
import createFilterVM from './createFilter';
import updateFilterVM from './updateFilter';
import listAttributeDataType from './listAttributeDataType';
import createAttribute from './createAttribute';
import updateAttribute from './updateAttribute';
import prefixVM from './prefix';
import listAttributeOptionsVM from './listAttributeOptions';
import createAttributeOptions from './createAttributeOptions';
import updateAttributeOptions from './updateAttributeOptions';
import listSortAttributeDataType from './listSortAttributeDataType';
import customerTypeVM from './customerTypes';

export const CustomerVM = {
  showVM,
  listVM,
  attributeVM,
  filterVM,
  filterAttributeVM,
  userAttributeVM,
  createVM,
  createFilterVM,
  updateVM,
  contactVM,
  contactShowVM,
  listAttributeDataType,
  createAttribute,
  updateAttribute,
  prefixVM,
  listAttributeOptionsVM,
  createAttributeOptions,
  updateAttributeOptions,
  listSortAttributeDataType,
  customerTypeVM,
  updateFilterVM,
  configAttributeTableVM
}
