export default {
  transform(data) {
    let transform = {};

    transform = {
      id: data.id,
      team_id: data.team_id,
      prefix: data.prefix,
      created_at: data.created_at,
      updated_at: data.updated_at
    };

    return transform;
  }
}
