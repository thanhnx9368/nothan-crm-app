/* eslint-disable no-undef */
export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformFilterAttributes(item));
    });

    return new_data;
  },

  transformFilterAttributes(data) {
    let vm = this;
    let transform = {};
    transform = {
      id: data.id,
      filter_id: data.filter_id,
      attribute_id: data.attribute_id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      data_type: data.data_type,
      attribute_values: data.attribute_values,
      external_attribute: data.external_attribute,
      code_external_attribute: data.code_external_attribute,
      attribute_options: data.attribute_options,
      show_position: data.show_position,
      is_auto_gen: vm.thenIsAutoGenerate(data),
      is_disabled: true,
      created_at: data.created_at,
      updated_at: data.updated_at
    };

    return transform;
  },

  thenIsAutoGenerate(item) {
    if ($.inArray(item.attribute_code, ["sku", "full_name", "level", "customer_type", "user_id", "created_at", "updated_at"]) > -1) {
      return true;
    } else {
      return false;
    }
  }
}
