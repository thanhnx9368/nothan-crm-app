export default {
  transform(data) {
    let transform = {};

    transform = {
      attribute_id: data.attribute_id,
      attribute_options: this.transformOptions(data.attribute_options)
    };

    return transform;
  },

  transformOptions(data) {
    let result = [];
    data.forEach(function (item) {
      let tempData = {
        id: item.id,
        option_value: item.option_value,
        sort_order: item.sort_order,
        note: item.note
      };
      result.push(tempData);
    })
    return result;
  }
}
