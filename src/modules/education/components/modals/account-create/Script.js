/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import { CREATE_ACCOUNT_EDUCATION } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  props: {
    selected_ids: {
      type: Array,
      required: true,
      default: []
    },
    total_created: {
      type: Number,
      required: false,
      default: 0
    },
    is_created_multiple: {
      type: Boolean,
      default: false
    },
    is_create_by_detail_customer: {
      type: Boolean,
      default: false
    },
    student_name: {
      type: String,
      default: ''
    },
  },

  data() {
    return {
      totalSuccess: 0
    }
  },

  computed: {
    ...mapGetters(["currentTeam", "currentUser"]),
  },

  methods: {
    createAccountEducation() {
      let vm = this;
      let params = {};

      params.team_id = vm.currentTeamId;
      params.team_name = vm.currentTeam.name;
      params.team_phone = vm.currentTeam.phone ? vm.currentTeam.phone : null;
      params.team_email = vm.currentTeam.email ? vm.currentTeam.email : null;
      params.team_address = vm.currentTeam.address ? vm.currentTeam.address : null;
      params.list_customer_id = vm.selected_ids;
      params.type = vm.selected_ids && vm.selected_ids.length > 0 ? 1 : 2;
      params.current_user_email = vm.currentUser.email ? vm.currentUser.email : null;

      vm.$store.dispatch(CREATE_ACCOUNT_EDUCATION, params).then((response) => {
        if (response.data.data) {
          vm.$emit('is_change', true);
          vm.totalSuccess = response.data.data.length;
          $("#messageCreateAccountEducation").modal('show');
          vm.$snotify.success('Tạo tài khoản thành công.', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Tạo tài khoản thất bại.', TOAST_ERROR);
      });
    }
  },
}
