import { mapGetters } from "vuex";

import { BLOCK_ACCOUNT_EDUCATION } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  name: "BlockAccount",

  props: {
    selected_id: {
      type: Number,
      default: []
    },
    student_name: {
      type: String,
      default: ''
    },
    student_email: {
      type: String,
      default: ''
    },
  },

  computed: {
    ...mapGetters(["currentTeam"]),
  },

  methods: {
    blockAcountStudent() {
      let vm = this;
      let params = {
        team: vm.currentTeam.name,
        email: vm.student_email,
        team_id: vm.currentTeamId,
        full_name: vm.student_name,
        selected_id: vm.selected_id,
        email_team: vm.currentTeam.email ? vm.currentTeam.email : null,
        hotline_team: vm.currentTeam.phone ? vm.currentTeam.phone : null,
        address_team: vm.currentTeam.address ? vm.currentTeam.address : null
      };
      vm.$store.dispatch(BLOCK_ACCOUNT_EDUCATION, params).then((response => {
        if (response.data.data.success) {
          vm.$snotify.success('Khóa hoạt động tạm thời thành công.', TOAST_SUCCESS);
          vm.$emit('is_change_status_action_with_account_student');
        }

      })).catch(() => {
        vm.$snotify.error('Khóa hoạt động tạm thời thất bại.', TOAST_ERROR);
      })
    }
  },
}
