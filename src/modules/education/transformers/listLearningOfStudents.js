/* eslint-disable no-unused-vars */
export default {
  transform(data) {
    let vm = this;
    let responseData = [];
    let count = 1;
    data.forEach(function (item, index) {
      responseData.push(vm.transformData(item, count));
      count++;
    });

    return responseData;
  },

  transformData(data, stt) {
    let transform = {};

    transform = {
      id: data.id,
      lesson: "Buổi " + stt,
      start_time: data.start_time,
      end_time: data.end_time,
      total_lesson: data.total_lesson,
      date: data.date,
      status: data.status,
      lesson_id: data.lesson_id,
      member_id: data.member_id,
      is_back_soon: data.is_back_soon,
      is_checkin_late: data.is_checkin_late,
      is_checkin: data.is_checkin,
      status_history: data.status_history,
      home_work: data.home_work,
      note_home_work: data.note_home_work,
      note: data.note,
      created_at: data.created_at,
      updated_at: data.updated_at,
      checkin_at: data.checkin_at,
      checkin_by: data.checkin_by,
      back_soon_at: data.back_soon_at,
      back_soon_by: data.back_soon_by,
      checkin_late_at: data.checkin_late_at,
      checkin_late_by: data.checkin_late_by,
      check_home_work_at: data.check_home_work_at,
      check_home_work_by: data.check_home_work_by
    };

    return transform;
  }
}
