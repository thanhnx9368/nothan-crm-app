import {
  EducationVM
} from "../transformers/index";
import educationAPI from "@/services/education";
import productAPI from "@/services/product";

import {
  GET_LEARNING_OF_STUDENTS,
  EXPORT_LEARNING_SITUATION,
  CREATE_ACCOUNT_EDUCATION,
  BLOCK_ACCOUNT_EDUCATION,
  ACTIVE_ACCOUNT_EDUCATION,
  GET_ACCOUNT_EDU_ONLINE_STATUS,
  GET_LIST_CLASS_OF_CUSTOMER_DETAIL
} from "@/types/actions.type";
import {
  SET_LEARNING_OF_STUDENTS,
  SET_ACCOUNT_EDU_ONLINE_STATUS,
  SET_LIST_CLASS_OF_CUSTOMER_DETAIL
} from "@/types/mutations.type";

export const actions = {
  async [GET_LEARNING_OF_STUDENTS]({
    commit
  }, params) {
    try {
      let learning = await educationAPI.getLearningOfStudent(params);
      commit(SET_LEARNING_OF_STUDENTS, EducationVM.listLearningOfStudentsVM.transform(learning.data.data));
      return learning;
    } catch (e) {
      return e;
    }
  },

  async [EXPORT_LEARNING_SITUATION](context, params) {
    try {
      let response = await educationAPI.exportLearningSituation(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [CREATE_ACCOUNT_EDUCATION](context, params) {
    try {
      let response = educationAPI.createAccountEducation(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [BLOCK_ACCOUNT_EDUCATION](context, params) {
    try {
      let response = educationAPI.blockAccountEducation(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [ACTIVE_ACCOUNT_EDUCATION](context, params) {
    try {
      let response = educationAPI.activeAccountEducation(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_LIST_CLASS_OF_CUSTOMER_DETAIL]({
    commit
  }, params) {
    try {
      let response = await educationAPI.getListClassOfCustomerDetail(params);
      let dataResult = response.data.data;

      if (dataResult && dataResult.length > 0) {
        let productIdByKeys = {};
        let productIds = [];

        dataResult.forEach(function (education) {
          if (!productIdByKeys[education.product_id]) {
            productIdByKeys[education.product_id] = education.product_id;
          }
        });

        Object.keys(productIdByKeys).forEach(function (product_id) {
          productIds.push(product_id)
        });

        let params = {
          products: productIds
        };

        let productInEducation = await productAPI.getProductByTeams(params);

        if (productInEducation.data.data && productInEducation.data.data.length > 0) {
          dataResult.forEach(function (item) {
            productInEducation.data.data.forEach(function (product) {
              if (item.product_id == product.id) {
                item["product_name"] = product.product_name;
                item["product_unit"] = product.product_unit;
              }
            })
          })
        }
      }

      commit(SET_LIST_CLASS_OF_CUSTOMER_DETAIL, EducationVM.listClassOfCustomerDetailVM.transform(dataResult));
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_ACCOUNT_EDU_ONLINE_STATUS]({
    commit
  }, params) {
    try {
      let response = await educationAPI.getAccountEduOnlineStatus(params);
      commit(SET_ACCOUNT_EDU_ONLINE_STATUS, response.data.data);
      return response;
    } catch (e) {
      return e;
    }
  },
}