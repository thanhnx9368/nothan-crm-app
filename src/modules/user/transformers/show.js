export default {
  transform(data) {
    let transform = {};

    let teams = {};
    if (data.teams && data.teams.length > 0) {
      data.teams.forEach(function (item) {
        if (!teams[item.team]) {
          teams[item.team] = item.role;
        }
      });
    }

    transform = {
      id: data.id,
      email: data.email,
      full_name: data.full_name,
      phone_number: data.phone_number,
      role_id: data.role_id,
      avatar_url: data.avatar_url,
      birthday: data.birthday,
      address: data.address,
      team_ids: data.team_ids ? data.team_ids.split(",") : '',
      // teams : data.teams,
      created_at: data.created_at,
      updated_at: data.updated_at,
      teams: teams
    };
    return transform;
  }
}
