/* eslint-disable no-undef */
export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      let tempData = {
        id: item.id,
        name: item.name,
        description: item.description,
        team_id: item.team_id,
        parent_id: item.parent_id ? item.parent_id : 0,
        sort_order: item.sort_order,
        status: item.status,
        hidden: false,
        rules: 1,
        created_at: item.created_at,
        updated_at: item.updated_at,
        has_children: item.children && item.children.length > 0 ? true : false,
        users: item.users
      };
      new_data.push(tempData);
      let childrenData = vm.recursiveData(item);
      if (typeof childrenData != "undefined") {
        $.merge(new_data, childrenData);
      }
    });

    return new_data;
  },

  recursiveData(data) {
    if (!data.children || data.children && data.children.length <= 0) {
      return;
    }
    let vm = this;
    let newData = [];
    data.children.forEach(function (item) {
      let tempData = {
        id: item.id,
        name: item.name,
        description: item.description,
        parent_id: item.parent_id ? item.parent_id : 0,
        sort_order: item.sort_order,
        hidden: false,
        rules: 1,
        has_children: item.children && item.children.length > 0 ? true : false,
        users: item.users
      };
      newData.push(tempData);
      if (item.children && item.children.length > 0) {
        let childData = vm.recursiveData(item);
        $.merge(newData, childData);
      }
    })
    return newData;
  }
}
