export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformItem(item));
    });

    return new_data;
  },

  transformItem(data) {
    let transform = {};

    transform = {
      id: data.id,
      name: data.full_name,
      permissions: data.email,
    };

    return transform;
  }
}
