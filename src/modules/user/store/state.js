export const state = {
  list: [],
  currentUser: null,
  currentTeam: null,
  isLoaded: false,
  userCheckAddOn: [],
  listPermission: [],
  listStructures: [],
  userCurrentPermissions: {},
  listPermissionByStructure: [],
  appInfo: null
}
