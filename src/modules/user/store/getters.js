export const getters = {
  currentUser(state) {
    return state.currentUser;
  },
  listUsers(state) {
    return state.list;
  },
  userCheckAddOn(state) {
    return state.userCheckAddOn;
  },
  listPermission(state) {
    let listPermissionCode = [];

    if (state.listPermission && state.listPermission.length > 0) {
      state.listPermission.forEach(function (permission) {
        if (permission.code) {
          listPermissionCode.push(permission.code);
        }
      });
    }

    return state.listPermission;
  },
  listStructures(state) {
    return state.listStructures;
  },
  userCurrentPermissions(state) {
    return state.userCurrentPermissions;
  },
  listPermissionByStructure(state) {
    return state.listPermissionByStructure;
  },
  appInfo(state) {
    const infos = state.appInfo
    if (!infos) {
      return
    }
    infos.forEach(info => {
      switch (info.app_slug) {
        case 'crm':
          info.class = 'crm'
          break
        case 'hrm':
          info.class = 'hrm'
          break
        case 'cem':
          info.class = 'cem'
          break
        case 'education':
          info.class = 'em'
          break
      }
    })
    return infos
  },
  appInfoObj(state) {
    let appInfo = [];
    if (state.appInfo && state.appInfo.length > 0) {
      state.appInfo.forEach(app => {
        appInfo[app.app_slug] = app;
      })
    }
    return appInfo;
  }
}