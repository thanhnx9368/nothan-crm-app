export default {
  transform(data) {
    let transform = {};

    transform = {
      id: data.id,
      attributes: this.attributes(data.attributes),
      attributes_cache: this.attributes(data.attributes),
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
      user_id: data.user_id
    };

    return transform;
  },
  attributes(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformAttribute(item));
    });

    return new_data;
  },
  transformAttribute(data) {
    let transform = {};
    transform = {
      id: data.id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      data_type_id: data.data_type_id,
      is_hidden: data.is_hidden,
      is_required: data.is_required,
      is_unique: data.is_unique,
      is_multiple_value: data.is_multiple_value,
      attribute_options: data.attribute_options ? data.attribute_options : [],
      attribute_value: data.attribute_value ? data.attribute_value : "",
      attribute_value_cache: data.attribute_value ? data.attribute_value : ""
    };

    return transform;
  },
}
