export default {
  transform(data, meta) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformProducts(item, meta));
    });

    return new_data;
  },

  transformProducts(data, meta) {
    let transform = {};

    transform = {
      id: data.id,
      attributes: data.attributes,
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
      in_page: meta.current_page,
    };

    return transform;
  },
}
