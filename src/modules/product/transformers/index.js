import listVM from './list';
import showVM from './show';
import filterVM from './filter';
import createVM from './create';
import updateVM from './update';
import attributeVM from "./attributes";
import userAttributeVM from "./userAttributes";

export const ProductVM = {
  showVM,
  listVM,
  filterVM,
  createVM,
  updateVM,
  attributeVM,
  userAttributeVM
}
