export default {
  transform(data) {
    let transform = {};
    transform = {
      id: data.id,
      attributes: this.attributes(data.attributes),
      team_id: data.team_id
    };
    return transform;
  },

  attributes(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformAttribute(item));
    });

    return new_data;
  },

  transformAttribute(data) {
    let transform = {}

    transform = {
      id: data.id,
      attribute_value: data.attribute_value ? data.attribute_value : '',
      attribute_code: data.attribute_code ? data.attribute_code : '',
    };

    return transform;
  }

}