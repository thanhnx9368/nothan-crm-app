export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformFilters(item));
    });

    return new_data;
  },

  transformFilters(data) {
    let transform = {};

    transform = {
      id: data.id,
      name: data.name,
      team_id: data.teamId,
      filter_set_id: data.filterSetId,
      status: data.status,
      updated_at: data.updatedAt,
      deleted_at: data.deletedAt,
    };

    return transform;
  }
}
