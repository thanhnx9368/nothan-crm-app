import { mapGetters } from "vuex";

import helpers from '@/utils/utils';

export default {
  name: "ShowInfoDescription",
  computed: {
    helpers() {
      return helpers;
    },
    isLoader() {
      let vm = this;
      if (vm.dataReady === true && vm.attributeReady === true) {
        return true;
      } else {
        return false;
      }
    },
    ...mapGetters(['detailProduct'])
  },
  data() {
    return {
      dataReady: false,
      attributeReady: false,
    }
  },
}
