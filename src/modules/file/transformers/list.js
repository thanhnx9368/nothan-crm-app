export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformFiles(item));
    });

    return new_data;
  },

  transformFiles(data) {
    let transform = {};

    transform = {
      id: data.id,
      file_type_id: data.file_type_id,
      team_id: data.team_id,
      user_id: data.user_id,
      customer_id: data.customer_id,
      file_folder_id: data.file_folder_id,
      original_name: data.original_name,
      size: data.size,
      type: data.type,
      is_image: data.is_image,
      name: data.name,
      path_on_server: data.path_on_server,
      title: data.title,
      description: data.description,
      file_type: data.file_type,
      customer: data.customer,
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
    };

    return transform;
  }
}
