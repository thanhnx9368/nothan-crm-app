export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformTypes(item));
    });

    return new_data;
  },

  transformTypes(data) {
    let transform = {};

    transform = {
      id: data.id,
      mime_type: data.mime_type,
      extension: data.extension,
      file_type_group_id: data.file_type_group_id,
      is_allowed_to_upload: data.is_allowed_to_upload,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
    };

    return transform;
  }
}
