export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformData(item));
    });

    return new_data;
  },

  transformData(data) {
    let transform = {};

    transform = {
      id: data.id,
      team_id: data.team_id,
      user_id: data.user_id,
      content: data.content,
      object_id: data.object_id,
      type: data.type,
      created_at: data.created_at,
      update_at: data.update_at,
      files: this.transformFileData(data.files)
    };

    return transform;
  },

  transformFileData(files) {
    files.forEach(function (file) {
      if (file.type == "jpg" || file.type == "jpeg" || file.type == "png" || file.type == "gif") {
        file.url = "picture.png";
      } else if (file.type == "doc" || file.type == "docx") {
        file.url = "word.png";
      } else if (file.type == "xls" || file.type == "xlsx") {
        file.url = "excel.png";
      } else if (file.type == "txt") {
        file.url = "txt.png";
      } else if (file.type == "pdf") {
        file.url = "pdf.png";
      }
    });

    return files;
  },
}
