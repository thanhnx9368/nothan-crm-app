import {
  SET_NOTE_BY_OBJECT_ID
} from "@/types/mutations.type";

export const mutations = {
  [SET_NOTE_BY_OBJECT_ID](state, notes) {
    state.listByObjectId = notes;
  },
}
