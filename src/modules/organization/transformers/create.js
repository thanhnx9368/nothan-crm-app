export default {
  transform(data) {
    let transform = {};

    transform = {
      name: data.name,
      business_field_id: data.business_field_id
    };

    return transform;
  }
};
