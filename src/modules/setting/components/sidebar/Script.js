/* eslint-disable no-undef */
import { mapState } from "vuex";

export default {
  mounted() {
    $(".nt-settings-sidebar-menu-item-toggle").on("click", function () {
      let parent = $(this).parent(".nt-settings-sidebar-menu-item");
      let subMenu = parent.children(".nt-settings-sidebar-submenu").children(".nt-settings-sidebar-menu-subnav");
      if (!$(parent).hasClass("nt-sidebar-menu-open")) {
        $(parent).addClass("nt-sidebar-menu-open");
        $(subMenu).slideDown(300);
      } else {
        $(parent).removeClass("nt-sidebar-menu-open");
        $(subMenu).slideUp(200);
      }
    })
  },
  computed: {
    ...mapState({
      currentTeam: state => state.organization.currentTeam,
      currentUser: state => state.user.currentUser,
    }),
    currentRoleId() {
      return this.currentTeam.role_id;
    }
  },
  methods: {
    getClassParent(key) {
      let vm = this;
      let fullPath = vm.$route.fullPath;
      if (fullPath.indexOf(key) > -1) {
        return "nt-sidebar-menu-open";
      }
    },

    getDisplaySubMenu(key) {
      let vm = this;
      let fullPath = vm.$route.fullPath;
      if (fullPath.indexOf(key) > -1) {
        return "block";
      } else {
        return "none";
      }
    },

    getClassChildren(key) {
      let vm = this;
      let currentName = vm.$route.name;
      if (currentName == key) {
        return "nt-sidebar-menu-active";
      }
    }
  }
}
