/* eslint-disable no-undef */
const notifications = {
  update_prefix: {
    message: {
      title: "Thành công.",
      content: "Cập nhật tiền tố thành công"
    },
    type: "success",
    isShow: false
  }
}

const messagePrefix = {
  custom: {
    prefix_code: {
      required: "Không được để trống trường này.",
      max: "Giới hạn 2 kí tự."
    }
  }
};

export default {
  props: {
    title: {
      type: String,
      required: false,
    },
    type_page: {
      type: Number,
      required: true
    },
    after_confirm: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    after_confirm: {
      handler() {
        let vm = this;
        if (vm.after_confirm) {
          vm.prefix_data = JSON.parse(vm.cache_data);
          vm.isChange = false;
          vm.$emit('is_change_data', false);
          vm.$emit('confirm_data_success', true);
        }
      },
      deep: true
    }
  },
  mounted() {
    let vm = this;
    vm.getPrefixElement();
    vm.$validator.localize("en", messagePrefix);
  },
  computed: {
    autoUpperCasePrefix: function () {
      return this.prefix_data.toUpperCase();
    }
  },
  data() {
    return {
      prefix_data: '',
      cache_data: '',
      isChange: false
    }
  },
  methods: {
    getPrefixElement() {
      let vm = this;
      let url;
      if (vm.type_page == 1) {
        url = 'customer/getCustomerPrefix';
      }
      vm.$store.dispatch(url, 1).then(response => {
        if (response) {
          vm.prefix_data = response.data.data.prefix;
          vm.cache_data = JSON.stringify(response.data.data.prefix);
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getNamePageByKey(key) {
      switch (key) {
        case 2:
        case 3:
        case 4:
          break;

        case 1:
        default:
          return "Mã " + this.title;
      }
    },
    getValueCodeKey(key) {
      switch (key) {
        case 2:
        case 3:
        case 4:
          break;

        case 1:
        default:
          return this.title;
      }
    },
    savePrefix() {
      let vm = this;
      let url;
      if (vm.type_page == 1) {
        url = 'customer/updateCustomerPrefix';
      }
      vm.$store.dispatch(url, { team_id: 1, prefix: vm.prefix_data.toUpperCase() }).then(response => {
        if (response) {
          vm.cache_data = JSON.stringify(response.data.data.prefix);
          vm.isChange = false;
          $('#m-portlet--head-sm-prefix-' + vm.type_page).parent('.m-portlet--head-sm').removeClass('m-portlet--collapse');
          window.vueNotifications.notify = notifications.update_prefix;
          window.vueNotifications.notify.isShow = true;
        }
      }).catch(error => {
        console.log(error);
      });
    },
    resetPrefix() {
      let vm = this;
      vm.prefix_data = JSON.parse(vm.cache_data);
      vm.isChange = false;
      $('#m-portlet--head-sm-prefix-' + vm.type_page).parent('.m-portlet--head-sm').removeClass('m-portlet--collapse');
    },
    getCurrentFormatTime() {
      let newDateTime = new Date();
      let getMonth = parseInt(newDateTime.getMonth() + 1) > 9 ? parseInt(newDateTime.getMonth() + 1) : "0" + parseInt(newDateTime.getMonth() + 1);
      let getFullYear = newDateTime.getFullYear();
      return getFullYear.toString().substring(2) + getMonth.toString();
    },
    checkKeyUp() {
      let vm = this;
      vm.isChange = true;
      /*if (vm.prefix_data != JSON.parse(vm.cache_data)) {
          vm.isChange = true;
          $(window).on('beforeunload', function (e) {
              if (vm.isChange) {
                  e.preventDefault();
                  return false;
              }
          })
          $('#m-portlet--head-sm-prefix-' + vm.type_page).parent('.m-portlet--head-sm').addClass('m-portlet--collapse');
          $('#m-portlet--head-sm-prefix-' + vm.type_page).on('click', function (e) {
              if (vm.isChange) {
                  e.preventDefault();
                  $(this).parent('.m-portlet--head-sm').addClass('m-portlet--collapse');
                  $('#table__confirm--when-change-prefix').modal('show');
              }
          })
          vm.$emit('is_change_data', true);
      }*/
    },
    confirmChange() {
      let vm = this;
      vm.prefix_data = JSON.parse(vm.cache_data);
      vm.isChange = false;
      $('#m-portlet--body-sm-prefix-' + vm.type_page).slideUp(200);
      vm.$emit('is_change_data', false);
    }
  },
}
