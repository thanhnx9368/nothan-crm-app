import {
  DEAL_GET_ATTRIBUTES
} from "@/types/actions.type";
import ListAttributeCustom from "../../components/deal-attribute-list/ListAttributesDeal.vue";

export default {
  mounted() {
    let vm = this;
    vm.getAttributes();
  },
  data() {
    return {
      loadDataSuccess: false
    }
  },
  components: {
    "list-attribute-advanced": ListAttributeCustom
  },
  methods: {
    getAttributes() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(response => {
        let data = response.data.data;
        if (data && data.length > 0) {
          vm.loadDataSuccess = true;
        }
      }).catch(error => {
        console.log(error);
      });
    }
  }
}
