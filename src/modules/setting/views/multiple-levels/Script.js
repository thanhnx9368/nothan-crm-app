/* eslint-disable no-undef */
import Vue from "vue";
import VueNestable from 'vue-nestable';
import { mapGetters } from "vuex";

import {
  CUSTOMER_GET_ATTRIBUTE_OPTION,
  CUSTOMER_SORT_ATTRIBUTE_OPTION,
  CUSTOMER_CREATE_ATTRIBUTE_OPTION,
  CUSTOMER_UPDATE_ATTRIBUTE_OPTION,
  CUSTOMER_DELETE_ATTRIBUTE_OPTION
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

Vue.use(VueNestable);

export default {
  props: {
    attribute_id: {
      default: '',
      type: [Number, String],
      required: true
    },
    label: {
      required: true,
      type: String
    },
    prefix: {
      required: true,
      type: String
    },
    prefix_full: {
      required: true,
      type: String
    },
    multi_level_id: {
      required: true,
      type: String
    },
    stt: {
      default: '',
      type: String
    },
  },
  watch: {
    attribute_id: {
      handler(after) {
        if (Number.isInteger(after)) {
          this.getAttributeOptions(after);
        }
      },
      deep: true
    },

    data_list: {
      handler(after) {
        let vm = this;
        let count = 0;
        // Kiểm tra validate
        after.forEach(function (item) {
          if (item.require || item.unique) {
            count++;
          }

          if (item.option_value && item.option_value.length > 250) {
            count++;
          }
        });

        if (count) {
          vm.isDisabled = true;
        } else {
          vm.isDisabled = false;
        }
      },
      deep: true
    },
  },
  computed: {
    ...mapGetters(["getCustomerAttributeOptions"])
  },
  data() {
    return {
      isDisabled: false,
      convert_data_list: [],
      new_data_list: [],
      selectedItem: '',
      data_list: [],
      isChange: false,
      server_errors: "",
      isLoader: false,
      isLoadData: true,
      clickCollapse: 0,
      selectedId: 0,
      isDeleleAttribute: false
    }
  },
  methods: {
    showCollapse(event, stt) {
      let vm = this;
      let currentTarget = event.currentTarget;
      if (!$(currentTarget).hasClass("m-portlet--collapse")) {
        vm.clickCollapse = stt;
      }
    },

    sortAttributeOptions(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_SORT_ATTRIBUTE_OPTION, params).then(() => {
      }).catch(error => {
        console.log(error);
      });
    },

    getAttributeOptions(attribute_id) {
      let vm = this;
      if (Number.isInteger(attribute_id)) {
        vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTE_OPTION, attribute_id).then(response => {
          let data = response.data.data;
          if (data) {
            let new_data_sort = data.sort(function (before, after) {
              return before.sort_order - after.sort_order;
            });
            data.forEach(function (item) {
              item.option_value_cache = item.option_value;
            });
            vm.data_list = new_data_sort;
            vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
            vm.mapActionDataToMultiLevel();
          }
        }).catch(error => {
          console.log(error);
        });
      }
    },

    convertNestedDataToArray(data, array) {
      let vm = this;

      if (typeof data == 'undefined' || (typeof data != 'undefined' && data.length <= 0)) {
        return array;
      }

      for (let i = 0; i < data.length; i++) {
        array.push(data[i]);

        vm.convertNestedDataToArray(data[i].childrens, array);
      }

      return array;
    },

    convertNestedData(data) {
      let result = [];
      data.forEach(function updateParentId(parent_id) {
        return function ({ id, option_value, childrens, is_edit, add_new, require, unique, count, level, option_value_cache, sort_order }) {

          if (!parent_id && childrens.length) {
            result.push({ id, option_value, parent_id: parent_id || 0, is_edit, add_new, require, unique, count, level, option_value_cache, childrens, sort_order });
          }

          if (parent_id || !childrens.length) {
            result.push({ id, option_value, parent_id: parent_id || 0, is_edit, add_new, require, unique, count, level, option_value_cache, childrens, sort_order });
          }

          childrens.forEach(updateParentId(id));
        };
      }(''));

      return result;
    },

    mapActionDataToMultiLevel() {
      let vm = this;
      let tmpDataObject = {};
      let newData = [];

      // Tạo 1 object để dễ dàng truy xuất theo id mà không cần find in array
      vm.convert_data_list.forEach(function (data) {
        tmpDataObject[data.id] = data;
      });

      // add level
      for (let i = 0; i < vm.convert_data_list.length; i++) {

        vm.convert_data_list[i].level = 0;
        vm.convert_data_list[i].childrens = [];

        if (vm.convert_data_list[i].parent_id > 0 && tmpDataObject[vm.convert_data_list[i].parent_id]) {
          vm.convert_data_list[i].level = tmpDataObject[vm.convert_data_list[i].parent_id].level + 1;
        }
      }

      //Sắp xếp lại mảng theo thứ tự từ con nhỏ nhất -> cha
      vm.convert_data_list.sort(function (before, after) {
        return after.level - before.level;
      });

      //Lặp mảng, trả con về với cha tương ứng và xoá con
      vm.convert_data_list.forEach(function (data) {
        if (data.parent_id > 0) {
          if (tmpDataObject[data.parent_id]) {
            tmpDataObject[data.parent_id].childrens.push(data);
          }

          delete tmpDataObject[data.id];
        }
      });

      //Convert từ oject về mảng
      vm.convert_data_list.forEach(function (data) {
        if (tmpDataObject[data.id]) {
          newData.push(tmpDataObject[data.id]);
        }
      });

      //Gán lại biến global
      vm.new_data_list = newData;
      vm.isLoadData = false;
    },

    addNewItem(parentId) {
      let vm = this;
      let id = Math.floor((Math.random() * 10000000));

      let item = {
        id: id,
        parent_id: parentId,
        option_value: "",
        require: false,
        unique: false,
        childrens: [],
        is_edit: true,
        add_new: true,
        count: 0,
        sort_order: vm.data_list.length + 1
      };

      vm.data_list = vm.convertNestedData(vm.new_data_list);

      vm.data_list.push(item);

      vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
      vm.mapActionDataToMultiLevel();

      setTimeout(function () {
        $('#' + vm.multi_level_id + '_item_body_' + id).show();

        $('#' + vm.multi_level_id + '_item_body_' + id + ' .s--table-multiple-level_item-input[data-id="' + id + '"]').focus();
      }, 100);
    },

    checkValidateItem(data, event) {
      let vm = this;
      let value = (event.target.value).trim();
      let listMultiLevelExist = [];

      vm.data_list = vm.convertNestedData(vm.new_data_list);

      vm.data_list.find(function (item) {
        item.require = false;
        item.unique = false;

        // Check Require
        if (data.id == item.id) {
          item.option_value = value;

          if (value == "" || value == null || value == 'null') {
            item.require = true;
          }
        }

        // Check unique
        listMultiLevelExist = Object.values(vm.data_list.reduce((data, item) => {
          if ((item.option_value) != "" || (item.option_value) != null || (item.option_value) != 'null') {
            let key = (item.option_value).trim().toLowerCase();
            data[key] = data[key] || [];
            data[key].push(item);
            return data;
          }
        }, {})).reduce((data, item) => item.length > 1 ? data.concat(item) : data, []);

        listMultiLevelExist.forEach(function (item_exist) {
          if (data.id == item_exist.id && item.id == item_exist.id) {
            item.unique = true;
          }
        });

      });

      vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
      vm.mapActionDataToMultiLevel();
    },

    showEditItem(data) {
      let vm = this;

      vm.getAttributeOptions(vm.attribute_id);

      $('#' + vm.multi_level_id + '_item_body_' + data.id).slideToggle(300);

      setTimeout(function () {
        $('#' + vm.multi_level_id + '_item_body_' + data.id + ' .s--table-multiple-level_item-input[data-id="' + data.id + '"]').focus();
      }, 100);
    },

    saveItem(data) {
      let vm = this;
      vm.mapActionDataToMultiLevel();
      vm.selectedId = data.id;
      if (data.add_new) {
        vm.add(data);
      } else {
        vm.update(data);
      }
    },

    add(data) {
      let vm = this;
      let params = {
        attribute_id: vm.attribute_id,
        attribute_options: {
          option_value: data.option_value,
          sort_order: data.sort_order
        }
      }
      vm.isLoader = true;
      vm.$store.dispatch(CUSTOMER_CREATE_ATTRIBUTE_OPTION, params).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.setErrorsInServer(response.data.errors);
          vm.$snotify.error("Thêm mới " + vm.label + " thất bại.", TOAST_ERROR);
        } else {
          vm.getAttributeOptions(vm.attribute_id);
          $('#' + vm.multi_level_id + '_item_body_' + data.id).slideUp(300);
          vm.$snotify.success("Thêm mới " + vm.label + " thành công.", TOAST_SUCCESS);
        }
        vm.isLoader = false;
      }).catch(() => {
        vm.$snotify.error("Thêm mới " + vm.label + " thất bại.", TOAST_ERROR);
      });
    },

    update(data) {
      let vm = this;
      vm.isLoader = true;
      let params = {
        id: data.id,
        attribute_id: vm.attribute_id,
        option_value: data.option_value,
        parent_id: data.parent_id,
        count: data.count,
        sort_order: data.sort_order,
      }

      vm.$store.dispatch(CUSTOMER_UPDATE_ATTRIBUTE_OPTION, params).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.setErrorsInServer(response.data.errors);
          vm.$snotify.error("Cập nhật " + vm.label + " thất bại.", TOAST_ERROR);
        } else {
          vm.getAttributeOptions(vm.attribute_id);
          $('#' + vm.multi_level_id + '_item_body_' + data.id).slideUp(300);
          vm.$snotify.success("Cập nhật " + vm.label + " thành công.", TOAST_SUCCESS);
        }
        vm.isLoader = false;
      }).catch(() => {
        vm.$snotify.error("Cập nhật " + vm.label + " thất bại.", TOAST_ERROR);
      });
    },

    cancelSaveItem(data) {
      let vm = this;
      vm.server_errors = "";
      vm.getAttributeOptions(vm.attribute_id);
      vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
      vm.mapActionDataToMultiLevel();

      $('#' + vm.multi_level_id + '_item_body_' + data.id).slideUp(300);
    },

    modalDeleteItem(data) {
      let vm = this;
      vm.data_list.forEach(function (item) {
        if (item.id == data.id) {
          vm.selectedItem = item.id;
        }
      });

      vm.data_list = vm.convertNestedData(vm.new_data_list);

      setTimeout(function () {
        $('#deleteItem').modal('show');
      }, 10);
    },

    deleteItem(id) {
      let vm = this;
      let data = '';
      let dataIndex = '';
      vm.data_list.forEach(function (item, index) {
        if (item.id == id) {
          data = item;
          dataIndex = index;
        }
      });
      vm.server_errors = "";
      $('.modal-backdrop').remove();
      vm.data_list = vm.convertNestedData(vm.new_data_list);
      if (data && data.add_new) {
        vm.data_list.splice(dataIndex, 1);
        vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
        vm.mapActionDataToMultiLevel();
      } else {
        vm.clickCollapse = vm.stt;
        vm.isDeleleAttribute = true;
        vm.$store.dispatch(CUSTOMER_DELETE_ATTRIBUTE_OPTION, id).then(response => {
          if (response.data && response.data.errors && response.data.errors.exist) {
            $('#deleteItemConfirmInfo').modal('show');
          } else {
            vm.data_list.splice(dataIndex, 1);
            vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
            vm.mapActionDataToMultiLevel();
            vm.getAttributeOptions(vm.attribute_id);
            vm.$snotify.success("Xóa " + vm.label + " thành công.", TOAST_SUCCESS);
          }
          vm.isDeleleAttribute = false;
        }).catch(() => {
          $('#deleteItemConfirmInfo').modal('show');
        });
        vm.getAttributeOptions(vm.attribute_id);
      }
      vm.selectedItem = '';
    },

    checkDisabledAction(data) {
      if (data.option_value && data.option_value.trim().toLowerCase() == data.option_value_cache.trim().toLowerCase()) {
        return true;
      }
      return false;
    },

    setErrorsInServer(errors) {
      let vm = this;
      if (errors) {
        let keys = Object.keys(errors);
        if (keys.length > 0) {
          keys.forEach(function (key) {
            let message = "";
            if (errors[key][0] == "required") {
              message = "Không được để trống trường này";
            }
            vm.server_errors = message;
          })
        }
      }
    },

    sortNestedData() {
      let vm = this;
      let new_data = vm.convertNestedData(vm.new_data_list);
      if (new_data.length > 0) {
        new_data.forEach(function (item, index) {
          item.new_sort_order = index + 1;
        });
      }
      let new_data_sort = {
        attribute_id: vm.attribute_id,
        attribute_options: new_data
      }

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.sortAttributeOptions(new_data_sort);
      }, 1000);
    },
  }
}
