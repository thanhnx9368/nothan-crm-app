/* eslint-disable no-undef */
import helpers from "@/utils/utils";
export const getters = {
  deals(state) {
    return state.list;
  },
  listCustomerDeals(state) {
    return state.list;
  },
  customerDetailListDeal(state) {
    return state.customer_detail_list_deal;
  },
  detailDeal(state) {
    return state.deal ? state.deal : null;
  },
  allAttributes(state) {
    let attributeInfoGeneralNotAttribute = [
      {
        id: "customer_address",
        attribute_code: "customer_address",
        attribute_id: "customer_address",
        attribute_name: "Địa chỉ khách hàng",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: false,
        code_external_attribute: "customer_address",
        data_type: {
          id: 1
        }
      },
      {
        id: "product_code",
        attribute_code: "product_code",
        attribute_id: "product_code",
        attribute_name: "Mã Sản Phẩm",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "product_code",
        data_type: {
          id: 1
        }
      },
      {
        id: "product_name",
        attribute_code: "product_name",
        attribute_id: "product_name",
        attribute_name: "Tên Sản Phẩm",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "product_name",
        data_type: {
          id: 1
        }
      },
      {
        id: "product_quantity",
        attribute_code: "product_quantity",
        attribute_id: "product_quantity",
        attribute_name: "Số lượng",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "product_quantity",
        data_type: {
          id: 7
        }
      },
      {
        id: "product_price",
        attribute_code: "product_price",
        attribute_id: "product_price",
        attribute_name: "Đơn giá niêm yết",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "product_price",
        data_type: {
          id: 7
        }
      },
      {
        id: "product_discount",
        attribute_code: "product_discount",
        attribute_id: "product_discount",
        attribute_name: "Chiết khấu",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "product_discount",
        data_type: {
          id: 7
        }
      },
      {
        id: "order_discount",
        attribute_code: "order_discount",
        attribute_id: "order_discount",
        attribute_name: "Chiết khấu thêm",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "order_discount",
        data_type: {
          id: 7
        }
      },
      {
        id: "single_sale_price",
        attribute_code: "single_sale_price",
        attribute_id: "single_sale_price",
        attribute_name: "Đơn giá bán",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "single_sale_price",
        data_type: {
          id: 7
        }
      },
      {
        id: "price_after",
        attribute_code: "price_after",
        attribute_id: "price_after",
        attribute_name: "Thành tiền",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "price_after",
        data_type: {
          id: 7
        }
      },
      {
        id: "last_user_used",
        attribute_code: "last_user_used",
        attribute_id: "last_user_used",
        attribute_name: "Người sử dụng cuối",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "last_user_used",
        data_type: {
          id: 1
        }
      },
      {
        id: "shipping_status",
        attribute_code: "shipping_status",
        attribute_id: "shipping_status",
        attribute_name: "Trạng thái giao hàng",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "shipping_status",
        data_type: {
          id: 8
        }
      },
      {
        id: "user_id",
        attribute_code: "user_id",
        attribute_id: "user_id",
        attribute_name: "Người tạo",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "user_id",
        data_type: {
          id: 8
        }
      },
      {
        id: "created_at",
        attribute_code: "created_at",
        attribute_id: "created_at",
        attribute_name: "Ngày tạo",
        is_hidden: 0,
        is_show: 0,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "created_at",
        data_type: {
          id: 2
        }
      }
    ];
    let fullAttributes = JSON.parse(JSON.stringify(state.attributes));
    $.merge(fullAttributes, attributeInfoGeneralNotAttribute);
    let newAttributes = [];
    fullAttributes.forEach(function (attribute) {
      if (attribute.attribute_code !== "vat_type" && attribute.attribute_code !== "order_payment") {
        if (attribute.attribute_code === 'customer_email') {
          attribute.attribute_name = "Email khách hàng";
        }
        if (attribute.attribute_code === 'customer_phone') {
          attribute.attribute_name = "Số điện thoại khách hàng";
        }
        if ($.inArray(attribute.attribute_code, ["shipping_status", "product_price", "order_date", "order_code", "single_sale_price", "price_after", "product_code", "product_name", "product_quantity", "product_discount", "order_discount", "amount_must_be_paid", "transaction_status", "reason_for_cancellation", "base_total_transaction_amount", "vat_amount", "vat_type", "total_transaction_amount", "total_paid_amount", "order_payment_status", "customer_code", "customer_name", "user_id", "created_at", "updated_at"]) > -1) {
          attribute.is_auto_gen = true;
        } else {
          attribute.is_auto_gen = false;
        }
        newAttributes.push(attribute);
      }
    })
    newAttributes.sort(function (a, b) {
      if (helpers.convertLanguages(a.attribute_name) < helpers.convertLanguages(b.attribute_name)) { return -1; }
      if (helpers.convertLanguages(a.attribute_name) > helpers.convertLanguages(b.attribute_name)) { return 1; }
      return 0;
    });
    return newAttributes;
  },

  listAttributesForDeals(state) {
    let listFirstShow = [];
    let middleShow = [];
    let afterList = [];
    let fullAttributes = JSON.parse(JSON.stringify(state.attributes));
    fullAttributes.forEach(function (attribute) {
      if (attribute.attribute_code === 'order_code' || attribute.attribute_code === 'customer_code'
        || attribute.attribute_code === 'order_date' || attribute.attribute_code === 'contact_user'
        || attribute.attribute_code === 'charge_person' || attribute.attribute_code === 'order_name'
        || attribute.attribute_code === 'total_transaction_amount' || attribute.attribute_code === 'transaction_status'
      ) {
        listFirstShow.push(attribute);
      } else if (attribute.is_default_attribute) {
        if (attribute.attribute_code !== "vat_type" && attribute.attribute_code !== "order_payment") {
          if (attribute.attribute_code === "customer_phone") {
            attribute.attribute_name = "Số điện thoại khách hàng";
          } else if (attribute.attribute_code === "customer_email") {
            attribute.attribute_name = "Email khách hàng";
          }
          middleShow.push(attribute);
        } else {
          return;
        }
      } else {
        afterList.push(attribute);
      }
    });
    let sortFirstAttributes = [
      'order_code', 'customer_code', 'order_date', 'contact_user', 'charge_person', 'order_name', 'total_transaction_amount', 'transaction_status'
    ];
    let fistAttributesAfterSort = [];
    sortFirstAttributes.forEach(function (code) {
      listFirstShow.forEach(function (attribute) {
        if (attribute.attribute_code === code) {
          fistAttributesAfterSort.push(attribute);
        }
      })
    })
    middleShow.sort(function (before, after) {
      return before.sort_order - after.sort_order;
    });
    let externalAttributes = [];
    if (middleShow && middleShow.length > 0) {
      externalAttributes = [
        {
          id: "user_id",
          attribute_code: "user_id",
          attribute_id: "user_id",
          attribute_name: "Người tạo",
          is_hidden: 0,
          is_show: 0,
          external_attribute: 1,
          is_auto_gen: true,
          code_external_attribute: "user_id",
          data_type: {
            id: 8
          },
          sort_order: 0
        },
        {
          id: "created_at",
          attribute_code: "created_at",
          attribute_id: "created_at",
          attribute_name: "Ngày tạo",
          is_hidden: 0,
          is_show: 0,
          external_attribute: 1,
          is_auto_gen: true,
          code_external_attribute: "created_at",
          data_type: {
            id: 2
          },
          sort_order: 0
        }
      ];
    }
    afterList.sort(function (before, after) {
      return before.sort_order - after.sort_order;
    });
    $.merge(fistAttributesAfterSort, middleShow);
    $.merge(fistAttributesAfterSort, externalAttributes);
    $.merge(fistAttributesAfterSort, afterList);
    fistAttributesAfterSort.forEach(function (item, index) {
      item.sort_order = index + 1;
    })
    return fistAttributesAfterSort;
  },

  attributeDealCustomerFull(state) {
    let customerAttributes = ["order_code", "order_date", "transaction_status", "order_payment_status", "total_transaction_amount"];
    let newAttributes = [];
    customerAttributes.forEach(function (item) {
      state.attributes.forEach(function (attribute) {
        if (attribute.attribute_code == item) {
          newAttributes.push(attribute);
        }
      })
    });
    return newAttributes;
  },
  attributeAddDeal(state) {
    let attributes = [];
    state.attributes.forEach(function (item) {
      if (item.attribute_code !== 'order_payment') {
        attributes.push(item);
      }
    })
    return attributes;
  },
  attributeEditDeal(state) {
    let listFirstShow = [];
    let middleShow = [];
    let afterList = [];
    let fullAttributes = JSON.parse(JSON.stringify(state.attributes));
    fullAttributes.forEach(function (attribute) {
      if (attribute.attribute_code !== "customer_code" && attribute.attribute_code !== "customer_name") {
        if (attribute.attribute_code === 'order_code' || attribute.attribute_code === 'customer_code'
          || attribute.attribute_code === 'order_date' || attribute.attribute_code === 'contact_user'
          || attribute.attribute_code === 'charge_person' || attribute.attribute_code === 'order_name'
          || attribute.attribute_code === 'total_transaction_amount' || attribute.attribute_code === 'transaction_status'
        ) {
          listFirstShow.push(attribute);
        } else if (attribute.is_default_attribute) {
          middleShow.push(attribute);
        } else {
          if (attribute.attribute_code !== "vat_type" && attribute.attribute_code !== "vat_amount") {
            afterList.push(attribute);
          } else {
            return;
          }
        }
      }
    });
    let sortFirstAttributes = [
      'order_code', 'customer_code', 'order_date', 'contact_user', 'charge_person', 'order_name', 'total_transaction_amount', 'transaction_status'
    ];
    let fistAttributesAfterSort = [];
    sortFirstAttributes.forEach(function (code) {
      listFirstShow.forEach(function (attribute) {
        if (attribute.attribute_code === code) {
          fistAttributesAfterSort.push(attribute);
        }
      })
    })
    middleShow.sort(function (before, after) {
      return before.sort_order - after.sort_order;
    });
    let externalAttributes = [];
    if (middleShow && middleShow.length > 0) {
      externalAttributes = [
        {
          id: "user_id",
          attribute_code: "user_id",
          attribute_id: "user_id",
          attribute_name: "Người tạo",
          is_hidden: 0,
          is_show: 0,
          external_attribute: 1,
          is_auto_gen: true,
          code_external_attribute: "user_id",
          data_type: {
            id: 8
          },
          sort_order: 0
        },
        {
          id: "created_at",
          attribute_code: "created_at",
          attribute_id: "created_at",
          attribute_name: "Ngày tạo",
          is_hidden: 0,
          is_show: 0,
          external_attribute: 1,
          is_auto_gen: true,
          code_external_attribute: "created_at",
          data_type: {
            id: 2
          },
          sort_order: 0
        }
      ];
    }
    afterList.sort(function (before, after) {
      return before.sort_order - after.sort_order;
    });
    $.merge(fistAttributesAfterSort, middleShow);
    $.merge(fistAttributesAfterSort, externalAttributes);
    $.merge(fistAttributesAfterSort, afterList);
    fistAttributesAfterSort.forEach(function (item, index) {
      item.sort_order = index + 1;
    })
    return fistAttributesAfterSort;
  },

  attributeDealUsingReports(state) {
    let attributeIds = [8, 9, 10, 11, 13, 14, 15, 16];
    let newAttributes = [];
    let attributesConvert = state.attributes;
    let chargePersonAttr = {};

    attributesConvert.forEach(function (attribute) {
      attributeIds.forEach(function (id) {
        if (attribute.data_type_id == id && attribute.attribute_code != 'contact_user' && attribute.is_default_attribute === 1) {
          newAttributes.push(attribute);
        }
      });
    });

    chargePersonAttr = {
      id: "charge_person",
      attribute_code: "charge_person",
      attribute_id: "charge_person",
      attribute_name: "Người phụ trách",
      is_show: 1,
      is_hidden: 0,
      external_attribute: 1,
      code_external_attribute: "charge_person",
      sort_order: 1,
      user_id: 0,
      data_type: {
        id: "charge_person"
      },
      is_auto_gen: false
    };
    newAttributes.push(chargePersonAttr);

    return newAttributes;
  },

  getStatusAddNew(state) {
    return state.add_new;
  },

  getDealFilters(state) {
    return state.filters;
  },

  filterAttributesDeal(state) {
    return state.filter_attributes ? state.filter_attributes : [];
  },

  getAllDealDataType(state) {
    return state.data_types;
  },

  attributeDealCustom(state) {
    let attributes = state.attributes;
    let customAttributes = attributes.filter(function (attribute) {
      if (!attribute.is_default_attribute && !attribute.external_attribute) {
        return attribute;
      }
    })
    customAttributes.sort(function (before, after) {
      return before.sort_order - after.sort_order;
    });
    return customAttributes;
  }
}
