import dealAPI from "@/services/deal";
import productAPI from "@/services/product";
import customerAPI from "@/services/customer";
import educationAPI from "@/services/education";
import { DealVM } from "../transformers/index";
import {
  DEAL_CREATE,
  DEAL_CREATE_ADD_STATUS,
  DEAL_GET_ALL,
  DEAL_GET_ATTRIBUTES,
  DEAL_GET_DETAIL,
  DEAL_GET_TOTAL_PRICE,
  DEAL_GET_USER_ATTRIBUTES,
  DEAL_PRINT_TO_EXCEL,
  DEAL_UPDATE,
  DEAL_GET_ALL_PAYMENT_HISTORY,
  DEAL_CREATE_PAYMENT,
  CUSTOMER_DETAIL_GET_ALL_DEAL,
  DEAL_GET_ALL_PRODUCT,
  DEAL_UPDATE_DELIVERY_STATUS,
  GET_LIST_CUSTOMER_BY_PRODUCT_USED,
  GET_MEMBER_IN_CLASS_OF_DEAL,
  GET_LIST_DEAL_FILTERS,
  GET_DEAL_FILTER_ATTRIBUTES,
  DEAL_FILTER_CREATE,
  DEAL_FILTER_UPDATE,
  DEAL_FILTER_DELETE,
  DEAL_GET_TOP_HARD_REPORT,
  DEAL_GET_INFO_HARD_CHART,
  GET_DEAL_FLEXIBLE_REPORT,
  GET_DEAL_CHARGE_PERSON_REPORT,
  EXPORT_DEAL_FLEXIBLE_REPORT,
  EXPORT_DEAL_CHARGE_PERSON_REPORT,
  SAVE_DEAL_CONFIG_REPORT_FOR_USER,
  GET_DEAL_CONFIG_REPORT_FOR_USER,
  EXPORT_LIST_DEAL,
  DEAL_GET_ATTRIBUTE_DATA_TYPE,
  DEAL_CREATE_ATTRIBUTE,
  DEAL_ADD_ATTRIBUTE_CHECK_EXIST,
  DEAL_DELETE_ATTRIBUTE,
  DEAL_UPDATE_ATTRIBUTE,
  DEAL_SORT_ATTRIBUTES,
  DEAL_CHECK_HAS_PRODUCT_BEFORE_DELETE,
  DEAL_CHECK_PAYMENT_STATUS_SUCCESS,
  DEAL_IMPORT_CHECK_FILE_EXCEL,
  DEAL_GET_CUSTOMER_ATTRIBUTE,
  DEAL_GET_PRODUCT_ATTRIBUTE,
  DEAL_GET_DEAL_ATTRIBUTE,
  DEAL_EXPORT_ATTRIBUTE,
  DEAL_EXPORT_DEFAULT_ATTRIBUTE,
  DEAL_DEFAULT_ATTRIBUTE,
  DEAL_VALIDATE_IMPORT_DEAL,
  DEAL_IMPORT_DEAL,
  DEAL_GET_RESULT_FILE
} from "@/types/actions.type";
import {
  SET_DEAL_ATTRIBUTES,
  SET_DEAL_DETAIL,
  SET_DEAL_USER_ATTRIBUTES,
  SET_DEALS,
  SET_STATUS_ADD_NEW,
  SET_ALL_PAYMENT_HISTORY,
  SET_CUSTOMER_DETAIL_DEAL,
  SET_DEAL_FILTERS,
  SET_DEAL_FILTER_ATTRIBUTES,
  SET_DEAL_FILTERS_CREATE,
  SET_DEAL_FILTERS_UPDATE,
  SET_DEAL_FILTERS_DELETE,
  SET_DEAL_ATTRIBUTE_DATA_TYPE,
} from "@/types/mutations.type";

export const actions = {
  async [DEAL_GET_RESULT_FILE](context, params) {
    try {
      return await dealAPI.getResultDeal(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_IMPORT_DEAL](context, params) {
    try {
      return await dealAPI.importDeal(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_VALIDATE_IMPORT_DEAL](context, params) {
    try {
      return await dealAPI.validateImportDeal(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_DEFAULT_ATTRIBUTE](context, params) {
    try {
      return await dealAPI.getDefaultAttributes(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_EXPORT_DEFAULT_ATTRIBUTE](context, params) {
    try {
      return await dealAPI.exportExcelDefaultAttribute(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_EXPORT_ATTRIBUTE](context, params) {
    try {
      return await dealAPI.exportExcelAttribute(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_CUSTOMER_ATTRIBUTE](context, params) {
    try {
      return await customerAPI.getCustomerAttribute(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_PRODUCT_ATTRIBUTE](context, params) {
    try {
      return await productAPI.getProductAttribute(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_DEAL_ATTRIBUTE](context, params) {
    try {
      return await dealAPI.getDealAttribute(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_IMPORT_CHECK_FILE_EXCEL](context, params) {
    try {
      return await dealAPI.importCheckFileExcel(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_ALL]({ commit }, params) {
    try {
      let listData = await dealAPI.getAll(params);
      commit(SET_DEALS, DealVM.listVM.transform(listData.data.data, listData.data.meta));
      return listData;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_DETAIL_GET_ALL_DEAL]({ commit }, params) {
    try {
      let listData = await dealAPI.getAllDealByCustomer(params);
      commit(SET_CUSTOMER_DETAIL_DEAL, DealVM.listDealCustomerVM.transform(listData.data.data));
      return listData;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_ALL_PRODUCT](context, params) {
    try {
      let listData = await dealAPI.getAllProductByDealId(params);
      return listData;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_DETAIL]({ commit }, id) {
    try {
      let dealDetail = await dealAPI.show(id);
      let dataDealDetail = dealDetail.data.data;

      // let customer = await customerAPI.show(dataDealDetail.customer_id);

      // dataDealDetail.customer = customer.data.data;

      let attributes = [];
      let attributeTransactionStatus = "";

      if (dataDealDetail.attributes) {
        Object.keys(dataDealDetail.attributes).forEach(function (index) {
          let attribute = dataDealDetail.attributes[index];
          if (attribute.attribute_code == "transaction_status") {
            attributeTransactionStatus = attribute
          }
          attributes.push(attribute);
        });
      }

      // convert transaction status
      let transactionStatusCurrent = "";
      let transactionStatus = attributeTransactionStatus.attribute_options && attributeTransactionStatus.attribute_options.length > 0 ? attributeTransactionStatus.attribute_options : [];
      let transactionStatusCurrentValue = attributeTransactionStatus.attribute_value ? attributeTransactionStatus.attribute_value : "";

      transactionStatus.forEach(function (item, index) {
        item.level = index + 1;
      });

      dataDealDetail.transactionStatus = transactionStatus;
      dataDealDetail.transactionStatusCurrentValue = transactionStatusCurrentValue;

      if (dataDealDetail.transactionStatus && dataDealDetail.transactionStatus.length > 0) {
        dataDealDetail.transactionStatus.forEach(function (item) {
          if (item.id == dataDealDetail.transactionStatusCurrentValue) {
            transactionStatusCurrent = item;
          }
        });
      }

      dataDealDetail.transactionStatusCurrent = transactionStatusCurrent;

      dataDealDetail.attributes = attributes;

      let productIds = [];

      if (dataDealDetail.products && dataDealDetail.products.length > 0) {
        dataDealDetail.products.forEach(function (product) {
          productIds.push(product.id);
        });
      }

      let productDisabledIds = [];

      if (productIds && productIds.length > 0) {
        let productDisabledData = await educationAPI.getMemberInClassOfDeal({ ids: productIds });
        productDisabledIds = productDisabledData.data.data;
      }

      if (productDisabledIds && productDisabledIds.length > 0) {
        dataDealDetail.products.forEach(function (product) {
          productDisabledIds.forEach(function (id) {
            if (product.id == id) {
              product.is_disabled = 1;
            }
          });
        });
      }

      dataDealDetail.productDisabledIds = productDisabledIds;

      commit(SET_DEAL_DETAIL, DealVM.showVM.transform(dataDealDetail));
      return dealDetail;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_ATTRIBUTES]({ commit }) {
    try {
      let attributes = await dealAPI.getAttributes();
      commit(SET_DEAL_ATTRIBUTES, DealVM.attributeVM.transform(attributes.data.data));
      return attributes;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_USER_ATTRIBUTES]({ commit }) {
    try {
      let attributes = await dealAPI.getAttributeByUser();
      commit(SET_DEAL_USER_ATTRIBUTES, DealVM.userAttributeVM.transform(attributes.data.data));
      return attributes;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_TOTAL_PRICE]() {
    try {
      let totalPrice = await dealAPI.totalPriceOfAllDeal();
      return totalPrice;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_CREATE](context, params) {
    try {
      let parseParams = DealVM.insertVM.transform(params);
      let deal = await dealAPI.create(parseParams);
      return deal;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_PRINT_TO_EXCEL](context, params) {
    try {
      let data = await dealAPI.printToExcel(params);
      return data;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_CREATE_ADD_STATUS]({ commit }, status) {
    commit(SET_STATUS_ADD_NEW, status);
  },

  async [DEAL_UPDATE](context, params) {
    try {
      let parseParams = DealVM.updateVM.transform(params.params);
      let deal = await dealAPI.update(params.id, parseParams);
      return deal;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_ALL_PAYMENT_HISTORY]({ commit }, params) {
    try {
      let paymentHistory = await dealAPI.getAllPaymentHistory(params);
      commit(SET_ALL_PAYMENT_HISTORY, paymentHistory.data.data);
      return paymentHistory;

    } catch (e) {
      return e;
    }
  },

  async [DEAL_CREATE_PAYMENT](context, params) {
    try {
      return dealAPI.createPayment(params);
    } catch (e) {
      return e;
    }
  },

  async [DEAL_UPDATE_DELIVERY_STATUS](context, params) {
    try {
      let product = await dealAPI.dealUpdateDeliveryStatus(params);
      return product;
    } catch (e) {
      return e;
    }
  },

  async [GET_LIST_CUSTOMER_BY_PRODUCT_USED](context, params) {
    try {
      let responseData = await dealAPI.getListCustomerByProductUsed(params);
      return responseData;
    } catch (e) {
      return e;
    }
  },

  async [GET_MEMBER_IN_CLASS_OF_DEAL](context, params) {
    try {
      let responseData = await educationAPI.getMemberInClassOfDeal({ ids: params });
      return responseData;
    } catch (e) {
      return e;
    }
  },

  async [GET_LIST_DEAL_FILTERS]({ commit }, params) {
    try {
      let filters = await dealAPI.getDealFilters(params);
      if (filters.data.data) {
        commit(SET_DEAL_FILTERS, DealVM.filterVM.transform(filters.data.data));
      }
      return filters;
    } catch (e) {
      return e;
    }
  },

  async [GET_DEAL_FILTER_ATTRIBUTES]({ commit }, filterId) {
    try {
      let filterAttributes = await dealAPI.getDealFilterAttributes(filterId);
      if (filterAttributes.data.data) {
        commit(SET_DEAL_FILTER_ATTRIBUTES, DealVM.filterAttributesVM.transform(filterAttributes.data.data));
      }
      return filterAttributes;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_FILTER_CREATE]({ commit }, params) {
    try {
      let createFilter = await dealAPI.dealFilterCreate(params);
      if (createFilter.data.data) {
        commit(SET_DEAL_FILTERS_CREATE, DealVM.createFilterVM.transform(createFilter.data.data));
      }
      return createFilter;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_FILTER_UPDATE]({ commit }, params) {
    try {
      let filterId = params.filter_id;
      delete params.filter_id;
      let updateFilter = await dealAPI.dealFilterUpdate(filterId, params);
      if (updateFilter.data.data) {
        commit(SET_DEAL_FILTERS_UPDATE, DealVM.createFilterVM.transform(updateFilter.data.data));
      }
      return updateFilter;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_FILTER_DELETE]({ commit }, params) {
    try {
      let filterId = params.filter_id;
      delete params.filter_id;
      let deleteFilter = await dealAPI.dealFilterDelete(filterId, params);
      if (deleteFilter.data.data.success) {
        commit(SET_DEAL_FILTERS_DELETE, filterId);
      }
      return deleteFilter;
    } catch (error) {
      console.log(error)
    }
  },

  /***
***** REPORTS
***/
  async [DEAL_GET_TOP_HARD_REPORT](context, params) {
    try {
      let response = await dealAPI.getInfoTopHardChart(params);
      // commit(SET_CUSTOMER_TOP_HARD_REPORT, response.data.data);
      return response;
    } catch (e) {
      console.log(e)
      return e;
    }
  },

  async [DEAL_GET_INFO_HARD_CHART](context, params) {
    try {
      let response = await dealAPI.getInfoHardChart(params);
      return response;
    } catch (e) {
      console.log(e)
      return e;
    }
  },

  async [GET_DEAL_FLEXIBLE_REPORT](context, params) {
    try {
      let response = await dealAPI.getDealFlexibleReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_DEAL_CHARGE_PERSON_REPORT](context, params) {
    try {
      let response = await dealAPI.getDealChargePersonReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EXPORT_DEAL_FLEXIBLE_REPORT](context, params) {
    try {
      let response = await dealAPI.exportDealFlexibleReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EXPORT_DEAL_CHARGE_PERSON_REPORT](context, params) {
    try {
      let response = await dealAPI.exportDealChargePersonReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [SAVE_DEAL_CONFIG_REPORT_FOR_USER](context, params) {
    try {
      let response = await dealAPI.saveConfigDealAttributeReportForUser(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_CHECK_PAYMENT_STATUS_SUCCESS](context, params) {
    try {
      let response = await dealAPI.checkPaymentStatusSuccessOfDeal(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_DEAL_CONFIG_REPORT_FOR_USER](context, params) {
    try {
      let response = await dealAPI.getConfigDealAttributeReportForUser(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EXPORT_LIST_DEAL](context, params) {
    try {
      let response = await dealAPI.exportListDeal(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_GET_ATTRIBUTE_DATA_TYPE]({ commit }) {
    try {
      let response = await dealAPI.getAllAttributeDataType();
      if (response.data) {
        commit(SET_DEAL_ATTRIBUTE_DATA_TYPE, DealVM.listAttributeDataTypeVM.transform(response.data));
      }
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_CREATE_ATTRIBUTE](context, params) {
    try {
      let response = await dealAPI.createAttribute(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_ADD_ATTRIBUTE_CHECK_EXIST](context, params) {
    try {
      let response = await dealAPI.checkExistsCreateAttribute(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_UPDATE_ATTRIBUTE](context, params) {
    try {
      let response = await dealAPI.updateAttribute(params.id, params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_DELETE_ATTRIBUTE](context, params) {
    try {
      let response = await dealAPI.deleteAttributeById(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_SORT_ATTRIBUTES](context, params) {
    try {
      let response = await dealAPI.sortAttributes(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [DEAL_CHECK_HAS_PRODUCT_BEFORE_DELETE](context, params) {
    try {
      let response = await dealAPI.checkProductHasDealBeforeDelete(params);
      return response;
    } catch (e) {
      return e;
    }
  }
}
