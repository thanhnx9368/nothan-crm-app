import { actions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';
import { state } from './state';

export const dealStore = {
  actions: actions,
  getters: getters,
  mutations: mutations,
  state: state
}
