import {
  PERMISSION_ADD_DEAL,
  PERMISSION_UPDATE_DEAL,
  PERMISSION_UPDATE_PRODUCT_DEAL,
  PERMISSION_UPDATE_DEAL_EXTRA,
  PERMISSION_SHOW_DEAL,
  PERMISSION_EXPORT_DEAL,
  PERMISSION_CREATE_PAYMENT
} from '@/types/const'
import utils from '@/utils/utils'
import Deal from './views/deal/Deal.vue'
import DealCustomAttribute from './components/deal-custom/DealCustomAttribute.vue'
import Show from './views/show/Show.vue'
import ShowInfoProduct from './components/show-info-product/ShowInfoProduct.vue'
import ShowInfoAdvanced from './components/show-info-advanced/ShowInfoAdvanced.vue'
import ShowPaymentHistory from './components/show-payment-history/ShowPaymentHistory.vue'
import AddNewDeal from './components/deal-new/AddNewDeal.vue'
import EditDetailDeal from './components/deal-edit/EditDetailDeal.vue'
import convertFile from "./components/convert-file/convertFile";

export default [
  {
  name: "deals",
  path: "/deals",
  component: Deal,
  meta: {
    breadcrumb: [{
      name: "Giao dịch",
    }, {
      name: "Danh sách giao dịch",
    }],
    actions: [{
      type: 1,
      class: "nt-button nt-button-top nt-button-secondary",
      name: "Import danh sách",
      isIcon: 1,
      iconClass: "la la-upload",
      isModal: 1,
      idModal: "#importDeal",
      permissionActionName: "all",
      // permissionActionName: PERMISSION_IMPORT_DEAL,
      serviceName: "deal"
    }]
  }
},
  {
    path: "/deals/convert-file",
    name: "convertFile",
    component: convertFile
  },
  {
  path: "/deals/custom",
  name: "DealCustomAttribute",
  component: DealCustomAttribute,
  },
  {
  path: "/add-new-deal/:customer_id",
  name: "DealAddNew",
  component: AddNewDeal,
  meta: {
    breadcrumb: [{
      name: "Giao dịch"
    }, {
      name: "Thêm giao dịch",
    }],
    permissionName: PERMISSION_ADD_DEAL,
    serviceName: "deal"
  }
}, {
  path: "/edit-deal/:deal_id",
  name: "EditDetailDeal",
  component: EditDetailDeal,
  meta: {
    breadcrumb: [{
      name: "Giao dịch"
    }, {
      name: "Sửa giao dịch",
    }],
    isPermissionMultiple: true,
    permissionName: [PERMISSION_UPDATE_DEAL, PERMISSION_UPDATE_PRODUCT_DEAL, PERMISSION_UPDATE_DEAL_EXTRA],
    serviceName: "deal"
  }
}, {
  name: "dealDetail",
  path: "/deals/:deal_id",
  component: Show,
  redirect: {
    name: 'DealProductInfo'
  },
  children: utils.prefixRouter('/team/:team_id/deals/:deal_id', [{
    path: "",
    name: "DealProductInfo",
    component: ShowInfoProduct,
    meta: {
      permissionName: PERMISSION_SHOW_DEAL,
      serviceName: "deal",
      breadcrumb: [{
        name: "Giao dịch",
        link: "/team/:team_id/deals"
      }, {
        name: "Chi tiết giao dịch",
      }, ],
      actions: [{
        type: 3,
        class: "nt-button nt-button-top nt-button-warning btn-deal-edit nt-button-top-disabled",
        name: "Chỉnh sửa",
        isIcon: 1,
        iconClass: "icon icon-edit",
        isModal: 1,
        idModal: "",
        nameLink: "EditDetailDeal",
        permissionActionName: [PERMISSION_UPDATE_DEAL, PERMISSION_UPDATE_PRODUCT_DEAL, PERMISSION_UPDATE_DEAL_EXTRA],
        serviceName: "deal",
        isMultipleAction: true
      }, {
        type: 4,
        class: "nt-button nt-button-top nt-button-secondary",
        name: "In giao dịch",
        isIcon: 1,
        iconClass: "icon icon-printer",
        nameFunction: "dealPrintToExcel",
        position: "before",
        permissionActionName: PERMISSION_EXPORT_DEAL,
        serviceName: "deal"
      }]
    },
  }, {
    path: "/info-advanced",
    name: "DealInfoAdvanced",
    component: ShowInfoAdvanced,
    meta: {
      permissionName: PERMISSION_SHOW_DEAL,
      serviceName: "deal",
      breadcrumb: [{
        name: "Giao dịch",
        link: "/team/:team_id/deals"
      }, {
        name: "Chi tiết giao dịch",
      }, ],
      actions: [{
        type: 3,
        class: "nt-button nt-button-top nt-button-warning btn-deal-edit nt-button-top-disabled",
        name: "Chỉnh sửa",
        isIcon: 1,
        iconClass: "icon icon-edit",
        isModal: 1,
        idModal: "",
        nameLink: "EditDetailDeal",
        permissionActionName: [PERMISSION_UPDATE_DEAL, PERMISSION_UPDATE_PRODUCT_DEAL, PERMISSION_UPDATE_DEAL_EXTRA],
        serviceName: "deal",
        isMultipleAction: true
      }, {
        type: 4,
        class: "nt-button nt-button-top nt-button-secondary",
        name: "In giao dịch",
        isIcon: 1,
        iconClass: "icon icon-printer",
        nameFunction: "dealPrintToExcel",
        position: "before",
        permissionActionName: PERMISSION_EXPORT_DEAL,
        serviceName: "deal"
      }]
    },
  }, {
    path: "/payment-history",
    name: "DealPaymentHistory",
    component: ShowPaymentHistory,
    meta: {
      permissionName: PERMISSION_SHOW_DEAL,
      serviceName: "deal",
      breadcrumb: [{
        name: "Giao dịch",
        link: "/team/:team_id/deals"
      }, {
        name: "Chi tiết giao dịch",
      }],
      actions: [{
        type: 3,
        class: "nt-button nt-button-top nt-button-warning btn-deal-edit nt-button-top-disabled",
        name: "Chỉnh sửa",
        isIcon: 1,
        iconClass: "icon icon-edit",
        isModal: 1,
        idModal: "",
        nameLink: "EditDetailDeal",
        permissionActionName: [PERMISSION_UPDATE_DEAL, PERMISSION_UPDATE_PRODUCT_DEAL, PERMISSION_UPDATE_DEAL_EXTRA],
        serviceName: "deal",
        isMultipleAction: true
      }, {
        type: 4,
        class: "nt-button nt-button-top nt-button-secondary",
        name: "In giao dịch",
        isIcon: 1,
        iconClass: "icon icon-printer",
        nameFunction: "dealPrintToExcel",
        position: "before",
        permissionActionName: PERMISSION_EXPORT_DEAL,
        serviceName: "deal"
      }, {
        type: 4,
        class: "nt-button nt-button-top nt-button-primary  btn-deal-edit nt-button-top-disabled",
        name: "Thêm thanh toán",
        isIcon: 1,
        iconClass: "icon icon-plus",
        nameFunction: "addNewPayment",
        permissionActionName: PERMISSION_CREATE_PAYMENT,
        serviceName: "deal",
        position: "after",
      }]
    },
  }])
}]
