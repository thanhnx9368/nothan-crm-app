/* eslint-disable no-undef */
export default {
  transform(data) {
    let transform = {};
    let vm = this;
    transform = {
      id: data.id,
      attributes: vm.attributes(data.attributes),
      attributes_cache: vm.attributes(data.attributes),
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
      user_id: data.user_id,
      productDisabledIds: data.productDisabledIds ? data.productDisabledIds : [],
      products: data.products ? data.products : [],
      products_cache: data.products ? data.products : [],
      customer_id: data.customer_id,
      customer: data.customer ? data.customer : "",
      assigners_deal: data.assigners_deal ? data.assigners_deal : "",
      assigners_deal_cache: data.assigners_deal ? data.assigners_deal : "",
      deal_type: data.deal_type ? data.deal_type : "",
      deal_type_cache: data.deal_type ? data.deal_type : "",
      deal_discount: data.deal_discount ? data.deal_discount : "",
      deal_discount_cache: data.deal_discount ? data.deal_discount : "",
      transactionStatus: data.transactionStatus ? data.transactionStatus : [],
      transactionStatusCurrent: data.transactionStatusCurrent ? data.transactionStatusCurrent : "",
      transactionStatusCurrentValue: data.transactionStatusCurrentValue ? data.transactionStatusCurrentValue : ""
    };

    return transform;
  },
  attributes(data) {
    let vm = this;
    let new_data = [];
    // let attributes = vm.transformAttributes(data);
    data.forEach(function (item) {
      new_data.push(vm.transformAttribute(item));
    });

    return new_data;
  },

  transformAttributes(data) {
    let attributes = [];
    let issetAttributes = [];
    if (data && data.length > 0) {
      data.forEach(function (item) {

        if (item.data_type_id === 10 || item.data_type_id === 11 || item.data_type_id === 13 || item.data_type_id === 16) {
          if ($.inArray(item.id, issetAttributes) > -1) {
            let newValue = [];
            attributes.forEach(function (attribute) {
              if (item.id === attribute.id) {
                if (Array.isArray(attribute.attribute_value)) {
                  newValue.push(item.attribute_value);
                  $.merge(attribute.attribute_value, newValue);
                } else {
                  newValue.push(item.attribute_value);
                  newValue.push(attribute.attribute_value);
                  attribute.attribute_value = newValue;
                }
              }
            })
          } else {
            let newValue = [];
            newValue.push(item.attribute_value);
            item.attribute_value = newValue;
            attributes.push(item);
            issetAttributes.push(item.id);
          }
        } else if (item.data_type_id === 6) {
          item.attribute_value = item.attribute_value ? item.attribute_value.replace('.', ',') : '';
          attributes.push(item);
        } else {
          attributes.push(item);
        }
      })
    }
    return attributes;
  },

  transformAttribute(data) {
    let transform = {};
    let attributeValue = "";
    if (!data.attribute_value && data.attribute_code == "vat_amount") {
      attributeValue = 0;
    } else if (data.attribute_value) {
      attributeValue = data.attribute_value;
      if (data.data_type_id === 6) {
        attributeValue = data.attribute_value ? data.attribute_value.replace('.', ',') : "";
      }
    }
    transform = {
      id: data.id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      data_type_id: data.data_type_id,
      is_hidden: data.is_hidden,
      is_required: data.is_required,
      is_unique: data.is_unique,
      is_multiple_value: data.is_multiple_value,
      is_default_attribute: data.is_default_attribute ? data.is_default_attribute : 0,
      attribute_options: data.attribute_options ? data.attribute_options : [],
      attribute_value: attributeValue,
      attribute_value_cache: attributeValue,
      data_type: data.data_type,
      default_value: data.default_value,
      hint: data.hint,
      sort_order: data.sort_order,
      note: data.note
    };

    return transform;
  },
}
