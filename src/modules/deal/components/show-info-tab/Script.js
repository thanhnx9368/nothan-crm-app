/* eslint-disable no-undef */
import { mapGetters, mapState } from "vuex";

import helpers from "@/utils/utils";
import {
  DEAL_UPDATE,
  DEAL_CHECK_PAYMENT_STATUS_SUCCESS
} from "@/types/actions.type";
import ShowCustomerDropdown from "../show-customer/ShowCustomerDropdown.vue";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import { PERMISSION_UPDATE_DEAL } from "@/types/const";

const customerCreateTransactionStatusCancel = {
  custom: {
    'cancel_reason': {
      required: 'Không được để trống trường này.',
      max: 'Giới hạn 1025 ký tự.',
    },
  }
};

export default {
  components: {
    ShowCustomerDropdown: ShowCustomerDropdown
  },
  mounted() {
    let vm = this;
    vm.$validator.localize('en', customerCreateTransactionStatusCancel);
    vm.getInfoAssignersDetailDeal();
    vm.checkTransactionStatusSuccess(vm.detailDeal);
    vm.dealCheckPaymentStatusSuccess();
  },
  data() {
    return {
      PERMISSION_UPDATE_DEAL: PERMISSION_UPDATE_DEAL,

      selectItemIndex: null,
      cancelReason: '',
      dealAssigners: [],

      isDelivered: false,
      isTotalPaidAmount: false,
      isLoaderDealCheckPaymentStatusSuccess: false
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapState({
      totalPriceHasPaid: state => state.deal.totalPriceHasPaid,
      totalPriceDeal: state => state.deal.totalPriceDeal,
    }),
    ...mapGetters(["detailDeal", "currentTeam", "listUsers", "userCheckAddOn", "userCurrentPermissions"]),
    currentTotalPricePaid() {
      return helpers.formatPrice(this.totalPriceHasPaid);
    },
    currentTotalPriceRemain() {
      let priceRemain = this.totalPriceDeal - this.totalPriceHasPaid;
      return helpers.formatPrice(priceRemain);
    },
  },
  created() {
    let vm = this;
    vm.$bus.$on('updateIsDelivered', ($event) => {
      if ($event) {
        vm.dealCheckPaymentStatusSuccessWithoutLoader();
      }
    });
  },
  watch: {
    detailDeal: {
      handler(after) {
        let vm = this;
        vm.checkTransactionStatusSuccess(after);
      },
      deep: true
    }
  },
  methods: {
    dealCheckPaymentStatusSuccess() {
      let vm = this;
      let teamId = vm.$route.params.team_id;
      let dealId = vm.$route.params.deal_id;
      vm.isLoaderDealCheckPaymentStatusSuccess = true;
      if (teamId && dealId) {
        let params = {
          team_id: teamId,
          deal_id: dealId
        }
        vm.$store.dispatch(DEAL_CHECK_PAYMENT_STATUS_SUCCESS, params).then(response => {
          if (response.data.data.status) {
            vm.isDelivered = true;
          } else {
            vm.isDelivered = false;
          }
          vm.isLoaderDealCheckPaymentStatusSuccess = false;
        });
      }
    },

    dealCheckPaymentStatusSuccessWithoutLoader() {
      let vm = this;
      let teamId = vm.$route.params.team_id;
      let dealId = vm.$route.params.deal_id;
      if (teamId && dealId) {
        let params = {
          team_id: teamId,
          deal_id: dealId
        }
        vm.$store.dispatch(DEAL_CHECK_PAYMENT_STATUS_SUCCESS, params).then(response => {
          vm.isDelivered = response.data.data.status;
          // if (response.data.data.status) {
          //   vm.isDelivered = true;
          // } else {
          //   vm.isDelivered = false;
          // }
        });
      }
    },

    checkTransactionStatusSuccess(data) {
      let vm = this;

      let totalTransactionAmount = 0;
      let totalPaidAmount = 0;

      // vm.isDelivered = false;
      // let totalDelivered = 0;
      vm.isTotalPaidAmount = false;

      // if(data && data.products){
      //     if( data.products.length > 0){
      //         data.products.forEach(function(product){
      //             if(product.is_delivered == 2){
      //             totalDelivered++;
      //             }
      //         });
      //     }
      //
      //
      //     if(totalDelivered == data.products.length || data.products.length <= 0){
      //         vm.isDelivered = true;
      //     }
      // }

      if (data && data.attributes && data.attributes.length > 0) {
        data.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == 'total_transaction_amount') {
            if (attribute.attribute_value) {
              totalTransactionAmount = attribute.attribute_value;
            }
          }
          if (attribute.attribute_code == 'total_paid_amount') {
            if (attribute.attribute_value) {
              totalPaidAmount = attribute.attribute_value;
            }
          }
        });

        if (totalTransactionAmount == totalPaidAmount) {
          vm.isTotalPaidAmount = true;
        }
      }
    },

    getInfoAssignersDetailDeal() {
      let vm = this;
      vm.dealAssigners = [];
      if (vm.detailDeal && vm.detailDeal.assigners_deal && vm.detailDeal.assigners_deal.length > 0) {
        vm.listUsers.forEach(function (item) {
          vm.detailDeal.assigners_deal.forEach(function (userId) {
            if (item.id === userId) {
              vm.dealAssigners.push(item);
            }
          });
        });
      }
    },

    showModalTransactionStatus(data, index) {
      let vm = this;
      vm.selectItemIndex = index;
      setTimeout(function () { $('#change_transaction_status' + index).modal('show'); }, 100);
    },

    changeTransactionStatus(data) {
      let vm = this;
      let detailDeal = vm.detailDeal;
      let detailDealConvert = {};
      let attributes = [];

      detailDealConvert.id = detailDeal.id;

      if (vm.detailDeal && vm.detailDeal.attributes && vm.detailDeal.attributes.length > 0) {
        vm.detailDeal.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == "transaction_status") {
            attribute.attribute_value = data.id;
            attributes.push(attribute);
          }
        });
      }
      detailDealConvert.attributes = attributes;
      detailDealConvert.type_update = [1];

      let teamId = vm.$route.params.team_id;
      let dealId = vm.$route.params.deal_id;
      if (teamId && dealId) {
        let params = {
          team_id: teamId,
          deal_id: dealId
        }

        vm.$store.dispatch(DEAL_CHECK_PAYMENT_STATUS_SUCCESS, params).then(response => {
          if (response.data.data.status) {
            vm.$store.dispatch(DEAL_UPDATE, { id: detailDealConvert.id, params: detailDealConvert }).then(response => {
              if (response.data.status_code && response.data.status_code == 422) {
                let server_errors = response.data.errors;
                if (server_errors["not_permission_update"]) {
                  $("#updateTransactionStatusConfirmInfo").modal('show');
                } else {
                  vm.$snotify.error('Cập nhật trạng thái giao dịch thất bại.', TOAST_ERROR);
                }
              } else {
                if (response.data.data && response.data.data.status) {
                  vm.detailDeal.transactionStatusCurrent = data;
                  vm.detailDeal.transactionStatusCurrentValue = data.id;

                  vm.$snotify.success('Cập nhật trạng thái giao dịch thành công.', TOAST_SUCCESS);

                  vm.$emit("update_success", true);
                }
              }
            }).catch(() => {
              vm.$snotify.error('Cập nhật trạng thái giao dịch thất bại.', TOAST_ERROR);
            });
          } else {
            $("#updateTransactionStatusConfirmInfo").modal('show');
          }
        });
      }
    },

    showModalCancelReason(data, index) {
      let vm = this;
      vm.selectItemIndex = index;
      setTimeout(function () { $('#cancel_reason' + index).modal('show'); }, 100);
    },

    addCancelReason(scope, data) {
      let vm = this;

      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          let detailDeal = vm.detailDeal;
          let detailDealConvert = {};
          let attributes = [];

          detailDealConvert.id = detailDeal.id;

          if (vm.detailDeal && vm.detailDeal.attributes && vm.detailDeal.attributes.length > 0) {
            vm.detailDeal.attributes.forEach(function (attribute) {
              if (attribute.attribute_code == "transaction_status") {
                attribute.attribute_value = data.id;
                attributes.push(attribute);
              } else if (attribute.attribute_code == "reason_for_cancellation") {
                attribute.attribute_value = vm.cancelReason;
                attributes.push(attribute);
              }
            });
          }

          detailDealConvert.attributes = attributes;
          detailDealConvert.type_update = [1];

          vm.$store.dispatch(DEAL_UPDATE, { id: detailDealConvert.id, params: detailDealConvert }).then(response => {

            if (response.data.status_code && response.data.status_code == 422) {
              let server_errors = response.data.errors;
              if (server_errors["not_permission_update"]) {
                $("#updateTransactionStatusConfirmInfo").modal('show');
                $('#cancel_reason' + vm.selectItemIndex).modal('hide');
              } else {
                vm.$snotify.error('Cập nhật trạng thái giao dịch thất bại.', TOAST_ERROR);
              }
            } else {
              if (response.data.data && response.data.data.status) {
                vm.detailDeal.transactionStatusCurrent = data;
                vm.detailDeal.transactionStatusCurrentValue = data.id;

                vm.$snotify.success('Cập nhật trạng thái giao dịch thành công.', TOAST_SUCCESS);

                vm.$emit("update_success", true);
                $('#cancel_reason' + vm.selectItemIndex).modal('hide');
                vm.resetCancelReason();
              }
            }
          }).catch(() => {
            $('#cancel_reason' + vm.selectItemIndex).modal('hide');
            vm.$snotify.error('Cập nhật trạng thái giao dịch thất bại.', TOAST_ERROR);
          });

        }
      })
    },

    resetCancelReason() {
      let vm = this;
      vm.cancelReason = '';
      vm.$validator.reset();
    },

    reloadPage() {
      return window.location.reload();
    }
  }
}
