export default {
  props: {
    id: '',
  },
  data() {
    return {
      reason: '',
      error: '',
    }
  },
  watch: {
    reason: {
      handler() {
        this.showError()
      }
    }
  },
  methods: {
    showError() {
      if (!this.reason.trim()) {
        this.error = 'Không được để trống trường này.'
      } else if (this.reason.length >= 1025) {
        this.error = 'Giới hạn 1025 ký tự.'
      } else {
        this.error = ''
      }
    },

    reset() {
      this.reason = ''
      setTimeout(() => {
        this.error = ''
      }, 300);
    },

    cancel() {
      this.$emit('cancel')
      this.reset()
    },

    submit(e) {
      if (!this.reason.trim() || this.reason.length >= 1025) {
        this.showError()
        e.stopPropagation()
        return
      }
      this.$emit('confirmed', this.reason)
      this.reset()
    }
  },
}