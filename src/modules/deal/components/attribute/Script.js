import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import { PERMISSION_SHOW_CUSTOMER } from "@/types/const";
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE
} from "@/types/const";

export default {
  inject: {
    $validator: '$validator'
  },
  props: {
    attributes: {
      type: Array,
      default: []
    },
    server_errors: {
      type: [Array, Object, String],
      default: []
    },
    scope: {
      type: String,
      default: 'scope'
    },
    extend: {
      type: Boolean,
      default: false
    },
    deal: {
      type: Object
    },
    is_edit: {
      type: Boolean
    }
  },
  components: {},
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["listUsers", "detailCustomer", "attributeAddDeal", "customerContacts", "detailDeal", "userCheckAddOn", "userCurrentPermissions"]),
  },
  mounted() {
    let vm = this;

    if (vm.customerContacts && vm.customerContacts.length > 0) {
      let contacts = [];
      vm.customerContacts.forEach(function (contact) {
        if (!contact.deleted_at) {
          contacts.push(contact);
        }
      });
      vm.customerContactSortDelete = contacts;
      vm.getListContact(contacts);
    }

    if (!vm.is_edit) {
      vm.updateContactInfoAddDeal();
    }
  },
  watch: {
    customerContacts: {
      handler(after) {
        let vm = this;

        if (after && after.length > 0) {
          let contacts = [];
          after.forEach(function (contact) {
            if (!contact.deleted_at) {
              contacts.push(contact);
            }
          });
          vm.customerContactSortDelete = contacts;
          vm.getListContact(contacts);
        }

        // if(!vm.is_edit) {
        //     vm.updateContactInfoAddDeal();
        // }
      },
      deep: true
    }
  },
  data() {
    return {
      PERMISSION_SHOW_CUSTOMER: PERMISSION_SHOW_CUSTOMER,
      contacts: {},
      main_contact: '',
      contact_select: '',
      customerContactSortDelete: [],
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
    }
  },
  methods: {
    getListContact(contacts) {
      let vm = this;
      if (contacts.length) {
        contacts.forEach((item) => {
          vm.contacts[item.id] = item;
          if (item.is_main_contact == 1) {
            vm.main_contact = item.id;
          }
        })
      }

      vm.getContactSelect(vm.contacts);
    },

    changeDataInputType(attribute_code) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute_code]) {
          vm.server_errors[attribute_code] = '';
        }
      }, 500);
    },

    getUsersByValue() {
      let vm = this;
      let user = vm.listUsers.find(function (item) {
        return item.id == vm.detailDeal.user_id;
      });
      return user.full_name;
    },

    changeContact(value) {
      let vm = this;
      //console.log(vm.contacts, vm.main_contact);
      let contact_name = vm.contacts[value].full_name;
      let contact_phone = vm.contacts[value].phone;
      let contact_address = vm.contacts[value].address;
      let customer_id = vm.contacts[value].customer_id;
      //console.log(contact_name, contact_phone, contact_address);
      let contact = {
        contact_id: value,
        contact_phone: contact_phone,
        contact_address: contact_address,
        contact_name: contact_name,
        customer_id: customer_id,
      };
      let attributes = vm.is_edit ? vm.detailDeal.attributes : vm.attributeAddDeal;
      vm.updateContactInfo(attributes, contact);
    },

    getContactSelect(contacts) {
      let contact_id = '';
      let vm = this;

      if (!vm.is_edit) {
        contact_id = vm.main_contact ? vm.main_contact : '';
      } else {
        if (vm.detailDeal.attributes && vm.detailDeal.attributes.length > 0) {
          vm.detailDeal.attributes.forEach(function (attribute) {
            if (attribute.attribute_code == "contact_user") {
              contact_id = attribute.attribute_value;
            }
          });

          if (contact_id && !contacts[contact_id]) {
            contact_id = '';
          }
        }
      }
      vm.contact_select = contact_id;
    },

    updateContactInfoAddDeal() {
      let vm = this;
      let attributes = vm.attributeAddDeal;
      let attributeCustomer = vm.detailCustomer.attributes;

      let contact = {
        contact_id: '',
        contact_phone: '',
        contact_address: '',
        contact_name: '',
        customer_id: ''
      };
      if (vm.main_contact) {
        let contact_name = vm.contacts[vm.main_contact].full_name;
        let contact_phone = vm.contacts[vm.main_contact].phone;
        let contact_address = vm.contacts[vm.main_contact].address;
        let customer_id = vm.contacts[vm.main_contact].customer_id;
        //console.log(contact_name, contact_phone, contact_address, vm.contacts, vm.main_contact);
        contact.contact_id = vm.main_contact;
        contact.contact_phone = contact_phone;
        contact.contact_address = contact_address;
        contact.contact_name = contact_name;
        contact.customer_id = customer_id;
      }
      //console.log(contact);
      vm.updateContactInfo(attributes, contact);
      if (vm.accessible(vm.userCheckAddOn, 'customer', vm.PERMISSION_SHOW_CUSTOMER, vm.userCurrentPermissions)) {
        vm.updatePhoneAddressContactAddDeal(attributeCustomer, attributes);
      }
    },

    updateContactInfo(attributes, contact) {
      if (attributes && attributes.length > 0) {
        attributes.forEach(function (attribute) {
          if (attribute.attribute_code == "contact_user") {
            attribute.attribute_value = contact.contact_id;
            attribute.customer_id = contact.customer_id;
          }
          // if(attribute.attribute_code == "contact_user_phone"){
          //     attribute.attribute_value = contact.contact_phone;
          // }
          // if(attribute.attribute_code == "delivery_address"){
          //     attribute.attribute_value = contact.contact_address;
          // }
          // if(attribute.attribute_code == "receiver"){
          //     attribute.attribute_value = contact.contact_name;
          // }
        })
      }
    },

    updatePhoneAddressContactAddDeal(attributeCustomer, attributes) {
      let customer_phone = '';
      let customer_address = '';
      if (attributeCustomer && attributeCustomer.length > 0) {
        attributeCustomer.forEach(function (attribute) {
          if (attribute.attribute_code == "phone") {
            customer_phone = attribute.attribute_value;
          }
          if (attribute.attribute_code == "address") {
            customer_address = attribute.attribute_value;
          }
        })
      }
      //console.log(customer_phone);
      if (attributes && attributes.length > 0) {
        attributes.forEach(function (attribute) {
          if (attribute.attribute_code == "contact_user_phone") {
            attribute.attribute_value = customer_phone;
          }
          if (attribute.attribute_code == "delivery_address") {
            attribute.attribute_value = customer_address;
          }
        })
      }
    }
  }
};
