import {
  CUSTOMER_SHOW,
  CUSTOMER_GET_CONTACT
} from "@/types/actions.type";
import helpers from "@/utils/utils";

import {
  mapGetters
} from "vuex";

export default {
  props: {
    customer_id: {
      type: Number,
      required: true
    },

  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["currentTeam", "detailDeal", "customerContacts"]),
  },
  mounted() {
    let vm = this;
    let params = {
      customer_id: vm.customer_id,
    };
    vm.getAllContactByCustomer(params);
  },
  watch: {
    customer_id: {
      handler() {
        let vm = this;
        vm.getCustomerById();
      },
      deep: true
    }
  },
  created() {
    this.getCustomerById();
  },
 
  data() {
    return {
      isLoader: false,
      customer: ""
    }
  },
  methods: {
    getCustomerById() {
      let vm = this;
      vm.isLoader = true;
      let params = {
        customer_id: vm.customer_id,
        is_permission: false
      };
      vm.$store.dispatch(CUSTOMER_SHOW, params).then(response => {
        if (response.data.data) {
          vm.customer = response.data.data;
          vm.$bus.$emit('getCustomerDetailDeal', vm.customer);
          vm.isLoader = false;
        }
      });
    },
    getAllContactByCustomer(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_CONTACT, params).then((response) => {
        if (response.data.data) {
          //console.log(response.data.data, '123');
        }
      }).catch((error) => {
        console.log(error)
      });
    },
  }
};