import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import UserObjectLoading from "@/components/loading/UserObject";
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE
} from "@/types/const";

export default {
  components: {
    "user-object-loading": UserObjectLoading,
  },
  mounted() {
    let vm = this;
    vm.getCreatedBy(vm.detailDeal.user_id);
    if (vm.detailDeal.attributes && vm.detailDeal.attributes.length > 0) {
      let attributes = vm.detailDeal.attributes.filter(function (item) {
        if (!item.is_default_attribute) {
          return item;
        }
      })
      vm.attributesNotDefault = attributes;
    }
  },
  data() {
    return {
      createdBy: "",
      attributesNotDefault: [],
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["detailDeal", "listUsers"]),
  },
  methods: {
    getCreatedBy(userId) {
      let vm = this;
      let createdBy = vm.listUsers.find(function (user) {
        return user.id == userId;
      });

      vm.createdBy = createdBy;
    },

    convertNumber(value) {
      let stringValue = String(value);
      if (stringValue.length > 0) {
        stringValue = stringValue.replace(".", ",");
      }
      return stringValue;
    },

    getColorByOption(deal) {
      let value = "";
      if (deal && deal.attributes && deal.attributes.length > 0) {
        deal.attributes.forEach(function (item) {
          if (item.attribute_code == "order_payment_status") {
            let option = "";
            option = item.attribute_options.find(function (op) {
              return op.id == item.attribute_value;
            });
            if (option) {
              value = option.option_value;
            }
          }
        });
      }

      if (value == "Chưa thanh toán") {
        return "rgb(184, 46, 69)";
      } else if (value == "Đã thanh toán hết") {
        return "#69BE31";
      } else if (value == "Thanh toán 1 phần") {
        return "#FF8A00";
      } else {
        return "";
      }
    }
  }
}
