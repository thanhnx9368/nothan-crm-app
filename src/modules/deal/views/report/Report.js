/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex"

import TopStats from '@/modules/report/components/stats/TopStats.vue'
import HardChart from '@/modules/report/components/chart-hard/HardChart.vue'
import DealReports from '@/modules/report/views/deal/DealReports.vue'
import helpers from "@/utils/utils"
import {
  DEAL_GET_TOP_HARD_REPORT,
  DEAL_GET_INFO_HARD_CHART,
  DEAL_GET_ATTRIBUTES,
  SAVE_DEAL_CONFIG_REPORT_FOR_USER,
  GET_DEAL_CONFIG_REPORT_FOR_USER,
  USER_CURRENT
} from '@/types/actions.type'
import Loading from '@/components/loading/LoadingTable.vue'
import moment from "moment"

const tabs = {
  report: 'report',
  statistic: 'statistic'
}

const statOptions = {
  reportWithCancelStatus: 'Có giao dịch đã huỷ',
  reportWithoutCancelStatus: 'Không có giao dịch đã huỷ'
}

const rangeOptions = {
  day: 'Hôm nay',
  week: 'Tuần này',
  month: 'Tháng này',
  quarter: 'Quý này',
  year: 'Năm nay',
  custom: 'Tuỳ chỉnh'
}

export default {
  name: "report",
  components: {
    TopStats,
    HardChart,
    DealReports,
    Loading
  },
  data() {
    return {
      utils: helpers,
      rangeOptions: rangeOptions,
      selectedRangeOption: rangeOptions.month,
      tabs: tabs,
      selectedTab: tabs.report,
      statOptions: statOptions,
      range: {
        from: '',
        to: ''
      },
      lang: {
        days: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        months: [
          "Tháng 1",
          "Tháng 2",
          "Tháng 3",
          "Tháng 4",
          "Tháng 5",
          "Tháng 6",
          "Tháng 7",
          "Tháng 8",
          "Tháng 9",
          "Tháng 10",
          "Tháng 11",
          "Tháng 12"
        ],
      },
      selectedStatOptions: statOptions.reportWithoutCancelStatus,
      isLoader: false,
      loadingDataChart: false,
      team_id: parseInt(this.$route.params.team_id),
      hardChart: {
        start_time: [],
      },
      chart: null,
      chartId: 'report_deal_main_chart',
      chartType: 'line',
      chartName: 'Báo cáo giao dịch',
      chartOptions: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: false
        },
        legend: {
          display: false,
          position: 'top'
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          titleFontColor: "#838CAB",
          titleFontStyle: "normal",
          titleMarginBottom: 7,
          xPadding: 25,
          yPadding: 15,
          bodyFontColor: "#fff",
          bodyFontStyle: "bold",
          footerFontStyle: 'normal',
          caretSize: 6,
          footerFontColor: '#69BE31',
          callbacks: {
            label: function (tooltipItem) {
              if (tooltipItem.yLabel !== 0) {
                return tooltipItem.yLabel;
              } else {
                return '0'
              }
            },
            title: function (tooltipItems, data) {

              let tooltipIndex = tooltipItems[0].index;
              let newData = data.labelsX[tooltipIndex];

              return newData;
            },

          },
          custom: function (tooltip) {
            if (!tooltip) return;
            // disable displaying the color box;
            tooltip.displayColors = false;
          },
          labelTextColor: function () {
            return '#f00';
          }
        },
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Tăng trưởng giao dịch theo thời gian'
            },
            ticks: {
              beginAtZero: true
            }
          }]
        }
      },
      chartData: {
        labelsX: [],
        labels: [],
        datasets: [{
          label: "T3 - 19/11/2019",
          data: [],
          fill: false,
          backgroundColor: "#2A94DB",
          borderColor: "#2A94DB",
          lineTension: 0
        }]
      },
      topReports: [{
          current_value: '',
          value_description: "Tổng số giao dịch",
          value_type: "number",
          styles: {
            icon: '<i class="icon-shopping-cart-1"></i>',
            color: "#69BE31",
            background: "#EAF6E2",
            fontSize: "25px"
          }
        },
        {
          current_value: '',
          value_description: "Giao dịch mới mở",
          value_type: "number",
          styles: {
            icon: '<i class="icon-shopping-cart-1"></i>',
            color: "#B82E45",
            background: "#F5E1E4",
            fontSize: "25px"
          }
        },
        {
          current_value: '',
          value_description: "Giao dịch hoàn thành",
          value_type: "number",
          styles: {
            icon: '<i class="icon-dollar-2"></i>',
            color: "#9F36BE",
            background: "#F1E2F6",
            fontSize: "25px"

          }
        },
        {
          current_value: '',
          value_description: "Giao dịch hủy bỏ",
          value_type: "number",
          styles: {
            icon: '<i class="icon-dollar-2"></i>',
            color: "#FF8A00",
            background: "#FFE0BC",
            fontSize: "25px"

          }
        },
      ],
      attributeReady: false,
      attributesReportCheckedIds: [],
      flexibleReportData: [],
    }
  },
  mounted() {
    this.getAttributeConfigForUser();

    let oneDay = 24 * 60 * 60 * 1000;
    let dateCreateOrganization = this.currentTeam && this.currentTeam.created_at ? new Date(this.currentTeam.created_at) : '';
    let currentDate = new Date();
    let diffDays = dateCreateOrganization ? Math.round(Math.abs((currentDate.getTime() - dateCreateOrganization.getTime()) / (oneDay))) : '';

    if (diffDays < 14 && dateCreateOrganization) {
      this.hardChart.start_time[0] = dateCreateOrganization;
      this.hardChart.start_time[1] = currentDate;
    }

    setTimeout(() => {
      this.onChangeRange()
    }, 1000)
  },
  watch: {
    currentTeam: {
      handler(after) {
        let vm = this;
        if (after.created_at) {
          let oneDay = 24 * 60 * 60 * 1000;
          let dateCreateOrganization = new Date(after.created_at);
          let currentDate = new Date();
          let diffDays = Math.round(Math.abs((currentDate.getTime() - dateCreateOrganization.getTime()) / (oneDay)));

          if (diffDays < 14) {
            vm.hardChart.start_time[0] = dateCreateOrganization;
            vm.hardChart.start_time[1] = currentDate;
          }
        }
      }
    }
  },
  computed: {
    ...mapGetters(["attributeDealUsingReports", "currentTeam", "currentUser"])
  },
  methods: {
    isRemoveBorder(index) {
      const selectedIndex = helpers.objectValues(rangeOptions).indexOf(this.selectedRangeOption)
      return index - 1 === selectedIndex
    },
    onChangeRange() {
      switch (this.selectedRangeOption) {
        case rangeOptions.week:
          this.range = {
            from: moment().startOf('week').add(1, 'day').format('YYYY-MM-DD'),
            to: moment().endOf('week').add(1, 'day').format('YYYY-MM-DD')
          }
          break
        case rangeOptions.month:
          this.range = {
            from: moment().startOf('month').format('YYYY-MM-DD'),
            to: moment().endOf('month').format('YYYY-MM-DD')
          }
          break
        case rangeOptions.quarter:
          this.range = {
            from: moment().startOf('quarter').format('YYYY-MM-DD'),
            to: moment().endOf('quarter').format('YYYY-MM-DD')
          }
          break
        case rangeOptions.year:
          this.range = {
            from: moment().startOf('year').format('YYYY-MM-DD'),
            to: moment().endOf('year').format('YYYY-MM-DD')
          }
          break
        case rangeOptions.custom:
          document.getElementById('app-body').click()
          document.getElementsByClassName('mx-input')[0].click()
          return
        default:
          this.range = {
            from: moment().format('YYYY-MM-DD'),
            to: moment().format('YYYY-MM-DD')
          }
          break
      }
      this.onRefresh()
    },
    onChange(event) {
      this.range = {
        from: moment(event[0]).format('YYYY-MM-DD'),
        to: moment(event[1]).format('YYYY-MM-DD')
      }
      this.onRefresh()
    },
    onRefresh() {
      this.loadDataByDateRange()
      this.getTopHardReports()
    },
    ChangeDateTimeSelect(data) {
      this.hardChart = data
      this.loadDataByDateRange()
    },
    loadDataByDateRange() {
      let vm = this;
      vm.loadingDataChart = true;
      const params = {
        team_id: vm.team_id,
        is_contain_deal_cancel: this.selectedStatOptions === statOptions.reportWithCancelStatus,
        ...this.range
      };

      vm.$store.dispatch(DEAL_GET_INFO_HARD_CHART, params).then((response) => {
        vm.loadingDataChart = false;
        if (response.data.data) {
          let data = response.data.data;
          vm.chartData.labelsX = data.date_date_full;
          vm.chartData.labels = data.data_date;
          vm.chartData.datasets[0].data = data.date_value;
          vm.loadChart();

        }
      }).catch((error) => {
        console.log(error)
      });
    },
    loadChart() {
      let vm = this;
      vm.isLoaderChart = false;

      if (vm.chart) {
        vm.chart.destroy();
      }
      if (document.getElementById(vm.chartId)) {
        vm.chart = new Chart(document.getElementById(vm.chartId), {
          type: vm.chartType,
          data: vm.chartData,
          options: vm.chartOptions
        });
      }
    },
    getTopHardReports() {
      const params = {
        is_contain_deal_cancel: this.selectedStatOptions === statOptions.reportWithCancelStatus,
        ...this.range
      }
      this.$store.dispatch(DEAL_GET_TOP_HARD_REPORT, params).then((response) => {
        if (response.data.data) {
          this.isLoader = false
          let infoHard = response.data.data;
          this.topReports[0]['current_value'] = infoHard.total;
          this.topReports[1]['current_value'] = infoHard.total_deal_new;
          this.topReports[2]['current_value'] = infoHard.total_deal_done;
          this.topReports[3]['current_value'] = infoHard.total_deal_cancel;
        }
      }).catch((error) => {
        console.log(error);
      })
    },
    async getAttributes() {
      let vm = this;
      await vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(() => {
        vm.attributeReady = true;
      }).catch(error => {
        console.log(error)
      });
    },
    checkedAttributeShowHide() {
      let vm = this;
      vm.isLoader = true;

      vm.saveAttributeConfigForUser();
    },
    saveAttributeConfigForUser() {
      let vm = this;

      let params = {
        team_id: vm.currentTeamId,
        user_id: vm.currentUser.id,
        ids: vm.attributesReportCheckedIds
      };

      vm.$store.dispatch(SAVE_DEAL_CONFIG_REPORT_FOR_USER, params).then((response) => {
        if (response.data.data) {
          vm.isLoader = false;
        }
      });
    },
    async getAttributeConfigForUser() {
      let vm = this;

      await vm.getAttributes()

      let params = {
        team_id: vm.currentTeam && vm.currentTeam.team_id ? vm.currentTeam.team_id : vm.team_id
      }

      if (!vm.currentUser) {
        await vm.$store.dispatch(USER_CURRENT)
      }

      params.user_id = vm.currentUser.id

      await vm.$store.dispatch(GET_DEAL_CONFIG_REPORT_FOR_USER, params).then((response) => {
        if (response.data.data) {
          let attributeIds = response.data.data;
          vm.attributesReportCheckedIds = [];

          if (attributeIds) {
            for (var attributeKey in attributeIds) {
              if (!attributeIds.hasOwnProperty(attributeKey)) {
                continue;
              } else {
                vm.attributesReportCheckedIds.push(attributeIds[attributeKey]);
              }
            }
          }
        }
      });
    }
  }
}