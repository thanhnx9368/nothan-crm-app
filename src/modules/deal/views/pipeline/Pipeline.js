/* eslint-disable no-undef */
import draggable from 'vuedraggable'
import InfiniteLoading from 'vue-infinite-loading'
import {
  mapGetters,
} from "vuex"

import DealApi from '@/services/deal'
import Confirmation from "@/components/confirmation-popup/Confirmation.vue"
import utils from '@/utils/utils'
import CancelModal from '../../components/modals/cancel-reason/CancelModal.vue'
import Loading from "@/components/loading/LoadingTable.vue"

const colNames = {
  open: 'Mới mở',
  completed: 'Hoàn thành',
  cancel: 'Hủy bỏ'
}

const paymentStatuses = {
  unpaid: 'Chưa thanh toán',
  partialPaid: 'Thanh toán 1 phần',
  paid: 'Thanh toán hết'
}

const sortOptions = {
  'created_date, desc': 'Ngày tạo (mới <i class="fas fa-arrow-right"></i> cũ)',
  'created_date, asc': 'Ngày tạo (cũ <i class="fas fa-arrow-right"></i> mới)',
  'order_date, desc': 'Ngày đặt hàng  (mới <i class="fas fa-arrow-right"></i> cũ)',
  'order_date, asc': 'Ngày đặt hàng  (cũ <i class="fas fa-arrow-right"></i> mới)',
  'base_total_transaction_amount, desc': 'Tổng tiền  (lớn <i class="fas fa-arrow-right"></i> bé)',
  'base_total_transaction_amount, asc': 'Tổng tiền  (bé <i class="fas fa-arrow-right"></i> lớn)',
}

export default {
  components: {
    draggable,
    InfiniteLoading,
    Confirmation,
    CancelModal,
    Loading
  },
  props: {
    filter: {
      type: Array,
      default: () => []
    }
  },
  data() {
    return {
      list: [],
      sortOptions: sortOptions,
      paymentStatus: [],
      transactions: [],
      selectedIndex: -1,
      uid: `nt_confirmation${utils.uid()}`,
      tempData: {},
      dealIds: [],
      userConfig: [],
      confirmation: {
        showButton: {
          confirm: false,
          cancel: true
        },
        confirmText: '',
        cancelText: '',
        content: '',
        type: ''
      },
    }
  },
  watch: {
    filter: {
      handler() {
        this.onRefresh()
      },
      deep: true
    }
  },
  computed: {
    ...mapGetters(["listUsers", "currentUser", "currentTeam"]),
    totalTransactions() {
      if (!this.transactions.length) {
        return
      }
      const totalDeals = this.transactions.map(t => t.numberOfTransaction)
      return totalDeals.reduce((a, b) => a + b, 0)
    }
  },
  created() {
    DealApi.getUserConfig(this.currentUser.id).then(res => {
      const data = res.data && res.data.data
      this.userConfig = data
      this.getAttributes()
    })
  },
  methods: {
    getUserConfig() {
      DealApi.getUserConfig
    },

    getAttributes() {
      DealApi.getAttributes().then(res => {
        const data = res.data && res.data.data
        if (!data) {
          return
        }
        this.getPaymentStatus(data)
        this.getTransaction(data)
      })
    },

    getPaymentStatus(data) {
      const status = data.filter(t => t.attribute_code === 'order_payment_status')[0]
      const options = status.attribute_options
      if (!options) {
        return
      }
      options.forEach((option) => {
        switch (option.option_value) {
          case paymentStatuses.unpaid:
            this.paymentStatus.push({
              id: option.id,
              name: paymentStatuses.unpaid
            })
            break
          case paymentStatuses.partialPaid:
            this.paymentStatus.push({
              id: option.id,
              class: 'status_partial-paid',
              name: paymentStatuses.partialPaid
            })
            break
          case paymentStatuses.paid:
            this.paymentStatus.push({
              id: option.id,
              class: 'status_paid',
              name: paymentStatuses.paid
            })
            break
        }
      })
    },

    getTransaction(data) {
      const transactionStatus = data.filter(t => t.attribute_code === 'transaction_status')[0]
      const transactionAmount = data.filter(t => t.attribute_code === 'base_total_transaction_amount')[0]
      const cancelReason = data.filter(t => t.attribute_code === 'reason_for_cancellation')[0]
      const options = transactionStatus.attribute_options
      if (!options) {
        return
      }
      options.forEach((option, index) => {
        const config = this.userConfig.filter(t => t.id === option.id)[0]
        this.transactions.push({
          numberOfTransaction: 0,
          totalMoneys: 0,
          page: 0,
          colName: option.option_value,
          borderTopColor: this.getBorderTopColor(option.option_value),
          color: this.getColor(option.option_value),
          sortBy: config ? config.sort_by : 'created_date',
          sortDirection: config ? config.sort_direction : 'desc',
          transactionId: option.attribute_id,
          transactionValue: option.id,
          baseTotalTransactionAmountId: transactionAmount.id,
          cancelReasonId: cancelReason.id,
          canShowNoResult: false,
          isLoading: false,
          deals: []
        })
        if (this.filter.length) {
          this.getDealIds(this.transactions[index], index)
        } else {
          this.fetch(option.id, transactionAmount.id, index)
        }
      })
    },

    fetch(attribute_option_id, deal_attribute_id, index) {
      const transaction = this.transactions[index]
      const params = {
        is_first: !this.filter.length,
        attribute_option_id,
        deal_attribute_id,
        deal_ids: this.dealIds
      }
      DealApi.getDealStatistics(params).then(res => {
        const data = res.data && res.data.data
        if (!data) {
          return
        }
        transaction.numberOfTransaction = data.total_deal
        transaction.totalMoneys = Math.trunc(data.total_transaction_amount)
      })
    },

    getBorderTopColor(colName) {
      switch (colName) {
        case colNames.open:
          return '#FF8A00'
        case colNames.completed:
          return '#69BE31'
        case colNames.cancel:
          return '#BB1130'
        default:
          return '#fff'
      }
    },

    getColor(colName) {
      switch (colName) {
        case colNames.open:
          return '#FF8A00'
        case colNames.completed:
          return '#69BE31'
        case colNames.cancel:
          return '#BB1130'
        default:
          return '#FF8A00'
      }
    },

    onRefresh() {
      this.transactions.forEach((transaction, index) => {
        transaction.page = 0
        transaction.isLoading = true
        this.fetchDeals(transaction)
        this.getDealIds(transaction, index)
      })
    },

    getDealIds(transaction, index) {
      DealApi.getDealIdsByFilter({
        filter: this.getFilter(transaction)
      }).then(res => {
        const data = res.data && res.data.data
        this.dealIds = data
        this.fetch(transaction.transactionValue, transaction.baseTotalTransactionAmountId, index)
      })
    },

    getFilter(transaction) {
      let filter = [{
        attribute_id: transaction.transactionId,
        data_type: 1,
        attribute_code: 'transaction_status',
        attribute_value: [transaction.transactionValue],
        condition: 'IN'
      }]
      this.filter.forEach(v => {
        if (!v.attribute_value.length) {
          return
        }
        if (v.attribute_code === 'transaction_status') {
          const codes = filter.map(t => t.attribute_code)
          const index = codes.indexOf(v.attribute_code)
          filter[index].attribute_value = v.attribute_value.indexOf(transaction.transactionValue) !== -1 ? [transaction.transactionValue] : [-1]
        } else {
          filter.unshift(v)
        }
      })
      return filter
    },

    fetchDeals(transaction, $state) {
      transaction.page++
      const params = {
        page: transaction.page,
        filter: this.getFilter(transaction),
        sort_by: transaction.sortBy,
        sort_direction: transaction.sortDirection,
      }
      DealApi.getAll(params).then(res => {
        transaction.isLoading = false
        transaction.canShowNoResult = true
        const data = res.data && res.data.data
        if ($state && !data.length) {
          $state.complete()
          return
        }
        transaction.deals = transaction.page === 1 ? this.transform(data) : [...transaction.deals, ...this.transform(data)]
        if ($state) {
          $state.loaded()
        }
      })
    },

    getAssigner(id) {
      const user = this.listUsers.filter(a => a.id === id)[0] || {}
      return user.full_name
    },

    transform(data) {
      const results = []
      data.forEach(t => {
        const result = {}
        result.isLoading = false
        result.assigner = t.assigners ? t.assigners[0] : -1
        result.createdDate = new Date(t.created_at)
        result.id = t.id
        t.attributes.forEach(att => {
          switch (att.attribute_code) {
            case 'order_name':
              result.name = att.value
              break
            case 'order_payment_status':
              result.status = att.value
              break
            case 'customer_name':
              result.customer = att.value
              break
            case 'order_code':
              result.code = att.value
              break
            case 'base_total_transaction_amount':
              result.money = att.value
              break
            case 'order_date':
              result.orderDate = new Date(att.value)
              result.orderDateId = att.id
              break
          }
        })
        results.push(result)
      })
      return results
    },

    getRates(number) {
      return this.totalTransactions ? number / this.totalTransactions : 0
    },

    getPaymentStatusClass(status) {
      const result = this.paymentStatus.filter(t => t.id === status)[0]
      return result ? result.class : ''
    },

    dropAreas() {
      return [{
        class: 'open',
        colName: colNames.open,
      }, {
        class: 'completed',
        colName: colNames.completed,
      }, {
        class: 'cancel',
        colName: colNames.cancel,
      }]
    },

    onChoose(index, colName) {
      if (colName === colNames.completed || colName === colNames.cancel) {
        return
      }
      this.selectedIndex = index
      this.$refs[`drag${this.uid}`].style.visibility = 'visible'
    },

    onUnchoose() {
      $('[data-toggle="m-tooltip"]').tooltip('hide')
      setTimeout(() => {
        this.$refs[`drag${this.uid}`].style.visibility = 'hidden'
      }, 100);
    },

    onAdd(colName, event) {
      const dealId = parseInt(event.clone.id)
      this.tempData = {
        colName,
        dealId
      }
      const deals = this.transactions.filter(t => t.colName === colName)[0].deals
      if (this.list[0]) {
        deals.unshift(this.list[0])
      }
      const deal = deals.filter(t => t.id === dealId)[0]
      deal.isLoading = true
      switch (colName) {
        case colNames.completed:
          this.checkCompleteStatus()
          break
        case colNames.cancel:
          this.openConfirmation('Đơn hàng sau khi được chuyển sang trạng thái "<span style="color: #B82E45">Huỷ bỏ</span>" sẽ không được chỉnh sửa nữa. Bạn muốn tiếp tục chứ?', {
            confirm: true,
            cancel: true
          }, 'Tiếp tục', undefined, colNames.cancel)
          break
        default:
          deal.isLoading = false
          break
      }
      this.list = []
    },

    checkCompleteStatus() {
      const {
        colName,
        dealId
      } = this.tempData
      const selectedCol = this.transactions[this.selectedIndex]
      const addedCol = this.transactions.filter(t => t.colName === colName)[0]
      const body = {
        deal_id: dealId,
        team_id: this.$route.params.team_id
      }
      DealApi.checkPaymentStatusSuccessOfDeal(body).then(res => {
        const data = res.data && res.data.data
        if (!data) {
          return
        }
        const deals = addedCol.deals
        const deal = deals.filter(t => t.id === dealId)[0]
        if (data.status) {
          DealApi.checkPaymentOfDeal(body).then(r => {
            const status = r.data && r.data.data && r.data.data.status
            switch (status) {
              case 2:
                deal.status = this.paymentStatus.filter(t => t.name === paymentStatuses.partialPaid)[0].id
                this.reverseCompleted(selectedCol, deals)
                break
              case 3:
                deal.status = this.paymentStatus.filter(t => t.name === paymentStatuses.paid)[0].id
                this.openConfirmation('Đơn hàng sau khi được chuyển sang trạng thái "<span style="color: #69BE31">Hoàn thành</span>" sẽ không được chỉnh sửa nữa. Bạn muốn tiếp tục chứ?', {
                  confirm: true,
                  cancel: true
                }, 'Tiếp tục', undefined, colNames.completed)
                break
              default:
                this.reverseCompleted(selectedCol, deals)
                break
            }
          })
        } else {
          this.reverseCompleted(selectedCol, deals)
        }
      }).catch(() => {
        this.reverse(selectedCol, deals)
      })
    },

    reverseCompleted(selectedCol, deals) {
      this.openConfirmation('Giao dịch chỉ được chuyển sang trạng thái <b>Hoàn thành</b> khi tất cả sản phẩm thuộc giao dịch <b>đã được giao hàng</b> và <b>thanh toán hết</b>.', {
        confirm: false,
        cancel: true
      }, undefined, 'Đóng')
      this.reverse(selectedCol, deals)
    },

    onUpdateStatus(reason) {
      const {
        colName,
        dealId
      } = this.tempData
      const addedCol = this.transactions.filter(t => t.colName === colName)[0]
      const deals = addedCol.deals
      const deal = deals.filter(t => t.id === dealId)[0]
      const body = {
        attributes: [{
          attribute_code: "transaction_status",
          attribute_value: addedCol.transactionValue,
          id: addedCol.transactionId
        }],
        type_update: [1]
      }
      if (reason) {
        body.attributes.push({
          attribute_code: "reason_for_cancellation",
          attribute_value: reason,
          id: addedCol.cancelReasonId
        })
        body.type_update.push(3)
      }
      DealApi.update(dealId, body).then(res => {
        deal.isLoading = false
        const data = res.data && res.data.data
        if (!data) {
          this.openConfirmation('Không thể chuyển đổi trạng thái giao dịch.<br>Vui lòng thử lại.', {
            confirm: false,
            cancel: true
          }, undefined, 'Đóng')
          this.onRefresh()
          return
        }
        const selectedCol = this.transactions[this.selectedIndex]
        if (data.status) {
          selectedCol.numberOfTransaction -= 1
          selectedCol.totalMoneys = Math.trunc(selectedCol.totalMoneys) - Math.trunc(deal.money)
          addedCol.numberOfTransaction += 1
          addedCol.totalMoneys = Math.trunc(addedCol.totalMoneys) + Math.trunc(deal.money)
        } else {
          this.reverse(selectedCol, deals)
        }
      }).catch(() => {
        this.reverse(selectedCol, deals)
      })
    },

    onDismiss() {
      const selectedCol = this.transactions[this.selectedIndex]
      const addedCol = this.transactions.filter(t => t.colName === this.tempData.colName)[0]
      const deals = addedCol.deals
      this.reverse(selectedCol, deals)
    },

    reverse(selectedCol, deals) {
      const dealId = this.tempData.dealId
      const deal = deals.filter(t => t.id === dealId)[0]
      deal.isLoading = false
      selectedCol.deals.unshift(deal)
      this.onSort(selectedCol.sortOption, this.selectedIndex)
      const index = deals.map(t => t.id).indexOf(dealId)
      if (index !== -1) {
        deals.splice(index, 1)
      }
    },

    statusTooltip(status) {
      if (!status) {
        return paymentStatuses.unpaid
      }
      return this.paymentStatus.filter(t => t.id === status)[0].name
    },

    canPull(colName) {
      if (colName === colNames.completed || colName === colNames.cancel) {
        return false
      }
      return true
    },

    sortValues() {
      return Object.values(sortOptions)
    },

    selectedSortOption(transaction) {
      return sortOptions[`${transaction.sortBy}, ${transaction.sortDirection}`]
    },

    onSort(index, transaction) {
      if (this.sortValues()[index] === this.selectedSortOption(transaction)) {
        return
      }
      const sortKeys = Object.keys(sortOptions)
      const key = sortKeys[index].split(', ')
      const transactionIds = this.userConfig.map(t => t.id)
      const indexT = transactionIds.indexOf(transaction.transactionValue)
      const config = {
        sort_by: key[0],
        sort_direction: key[1],
        id: transaction.transactionValue
      }
      if (indexT !== -1) {
        this.userConfig[indexT] = config
      } else {
        this.userConfig.push(config)
      }
      transaction.sortBy = key[0]
      transaction.sortDirection = key[1]
      transaction.page = 0
      transaction.isLoading = true
      const body = {
        team_id: this.currentTeamId,
        user_id: this.currentUser.id,
        options: this.userConfig,
        action_type: 'pipeline_sort'
      }
      DealApi.updateUserConfig(body)
      this.fetchDeals(transaction)
    },

    openConfirmation(content, showButton = {
      confirm: true,
      cancel: true
    }, confirmText = 'Đồng ý', cancelText = 'Huỷ Bỏ', type = '') {
      this.confirmation = {
        confirmText,
        cancelText,
        content,
        showButton,
        type
      }
      this.$refs[`span${this.uid}`].click()
    },

    onConfirmed() {
      switch (this.confirmation.type) {
        case colNames.completed:
          this.onUpdateStatus()
          break
        case colNames.cancel:
          this.$refs[`modal${this.uid}`].click()
          break
        default:
          break
      }
    },

    onCanceled() {
      switch (this.confirmation.type) {
        case colNames.completed:
        case colNames.cancel:
          this.onDismiss()
          break
        default:
          break
      }
    },
  },
}