import Vue from 'vue'
import Vuex from 'vuex'

import {
  authStore
} from "@/store/auth/index";
import {
  customerStore
} from "@/modules/customer/store/index";
import {
  customerCareStore
} from "@/modules/customer-care/store/index";
import {
  dealStore
} from "@/modules/deal/store/index";
import {
  educationStore
} from "@/modules/education/store/index";
import {
  fileStore
} from "@/modules/file/store/index";
import {
  noteStore
} from "@/modules/note/store/index";
import {
  organizationStore
} from "@/modules/organization/store/index";
import {
  productStore
} from "@/modules/product/store/index";
import {
  userStore
} from '@/modules/user/store/index';
import {
  myJobStore
} from '@/modules/my-job/store/index';


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: authStore,
    customer: customerStore,
    deal: dealStore,
    education: educationStore,
    file: fileStore,
    note: noteStore,
    organization: organizationStore,
    product: productStore,
    user: userStore,
    customerCare: customerCareStore,
    myJob: myJobStore
  },
})